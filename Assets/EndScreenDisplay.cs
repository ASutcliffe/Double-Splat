using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EndScreenDisplay : MonoBehaviour
{
    public TextMeshProUGUI playerScore;
    public UIController.Player player;
	public Image BackgroundImg, InlayImg;
    public RectTransform background, inlay, medal,text;
    private void Awake()
    {
        background.sizeDelta = new Vector2(Screen.width * 0.2734375f, Screen.height * 0.345f);
		GetComponent<RectTransform>().anchoredPosition = new Vector2(player == UIController.Player.Player1 ? 0 - Screen.width / 4 : Screen.width / 4, GetComponent<RectTransform>().anchoredPosition.y);
        inlay.sizeDelta = new Vector2(background.sizeDelta.x, background.sizeDelta.y);
        medal.sizeDelta = new Vector2(inlay.sizeDelta.y, inlay.sizeDelta.y);
        medal.anchoredPosition = new Vector2(0-(medal.sizeDelta.x/2),0);
        text.sizeDelta = new Vector2(inlay.sizeDelta.y, inlay.sizeDelta.x* 0.2121f);
        playerScore.fontSize = inlay.sizeDelta.x * 0.2424f;

		

	}
	private void OnEnable()
	{
		background.sizeDelta = new Vector2(Screen.width * 0.2734375f, Screen.height * 0.345f);
		GetComponent<RectTransform>().anchoredPosition = new Vector2(player == UIController.Player.Player1 ? 0 - Screen.width / 4 : Screen.width / 4, GetComponent<RectTransform>().anchoredPosition.y);

		inlay.sizeDelta = new Vector2(background.sizeDelta.x, background.sizeDelta.y);
		medal.sizeDelta = new Vector2(inlay.sizeDelta.y * 0.8164598301763553f, inlay.sizeDelta.y * 0.8164598301763553f);
		medal.anchoredPosition = new Vector2(0 - (medal.sizeDelta.x / 2), 0);
		text.sizeDelta = new Vector2(inlay.sizeDelta.y, inlay.sizeDelta.x * 0.2121f);
		playerScore.fontSize = inlay.sizeDelta.x * 0.2424f;

	}
	public void SetStyle()
	{
		BackgroundImg.sprite = ParameterManager.Instance.EndScreenBorder.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.EndScreenBorder.Dimensions.x, ParameterManager.Instance.EndScreenBorder.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			128f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		InlayImg.sprite = ParameterManager.Instance.EndScreenInlay.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.EndScreenInlay.Dimensions.x, ParameterManager.Instance.EndScreenInlay.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			128f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		BackgroundImg.type = Image.Type.Tiled;
		InlayImg.type = Image.Type.Tiled;


	}
}
