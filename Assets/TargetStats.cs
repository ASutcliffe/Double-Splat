#define DEBUG
///When building product should always #undef DEBUG
#undef DEBUG

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetStats
{
    #region Basic Variables
    public float speed { get; private set; }
    public string Debug()
    {
        string _tmpString;
        _tmpString = string.Format("ID {0}, Color {1}, Position X {2} Y {3}, Rotation {4}, Scale {5}, Speed {6}"
            , ID
            , color
            , positionRotationScale.x
            , positionRotationScale.y
            , positionRotationScale.z
            , positionRotationScale.w
            , speed);
        return _tmpString;
    }
    #endregion
    #region Complex Variables
    public Color color { get; private set; }
    public OmiTexture texture { get; private set; }
    public OmiTexture splatTex { get; private set; }
    public UIController.Player player { get; private set; }
    public Vector2Int ID { get; private set; }
    public Vector4 positionRotationScale { get; private set; }
    #endregion

    #region Constructor
    /// <summary>
    /// TargetStats Constructor
    /// </summary>
    /// <param name="_ID">Vector 2 Int X Determines the texture of the target and Y Determines the Color</param>
    /// <param name="_position">The XY Position of the Target</param>
    /// <param name="_rotation">The rotation in the Z axis</param>
    /// <param name="_scale">The Uniform Scale of the Target</param>
    /// <param name="_speed">The Speed the Target moves at</param>
    /// <param name="_player">The player the target is created for</param>
    public TargetStats(Vector2Int _ID, Vector2 _position, float _rotation, float _scale, float _speed, UIController.Player _player)
    {
        ID = _ID;
        texture = ParameterManager.Instance.ObjectImages[ID.x];
        if (ID.x> ParameterManager.Instance.ObjectsTappedState.Count-1)
        {
            splatTex = ParameterManager.Instance.ObjectsTappedState[0];
        }
        else
        {
            splatTex = ParameterManager.Instance.ObjectsTappedState[ID.x];
        }
        color = ParameterManager.Instance.ObjectColors[ID.y];
        positionRotationScale = new Vector4(_position.x, _position.y, _rotation, _scale);
        speed = _speed;
        player = _player;
    }
    #endregion

    public void SetSpeed(float _newSpeed)
    {
        speed = _newSpeed;
    }

    public void SetScale(float _newScale)
    {
        positionRotationScale = new Vector4(positionRotationScale.x, positionRotationScale.y, positionRotationScale.z, _newScale);
    }

}
