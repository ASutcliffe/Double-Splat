#define DEBUG
///When building product should always #undef DEBUG
#undef DEBUG

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class Target : MonoBehaviour
{

	#region Basic Variables
	#region Public
	public bool
		Initialised = false,
		xMax,
		yMax,
		xMin,
		yMin;
	#endregion
	#region Private
	private bool
		Completed = false,
		Spawned = false,
		destroyed = false;
	private float
		destroyTime = 0f,
		createTime = 0f,
		fadeOutTime = 1f;
	#endregion
	#endregion
	#region Complex Variables
	#region Public
	public PolygonCollider2D
		poly2Dcol;
	public Sprite
		defautSprite,
		splattedSprite;
	public SpriteRenderer
		spriteRenderer;
	public TargetStats
		stats;
	#endregion
	#region Private
	private GameObject
		objectToDestroy;
	public Transform
		targetTransform;
	public RotationConstraint RotCon;
	public GameObject RotationSmoother;
	#endregion
	#endregion

	#region Awake
	// Start is called before the first frame update
	private void Awake()
	{
#if DEBUG
        LogWriter.Log("Target Is Awake");
#endif
		spriteRenderer = GetComponent<SpriteRenderer>();
		targetTransform = gameObject.transform.parent.transform;
		RotationSmoother = new GameObject();
		RotationSmoother.name = "RotationSmoother";
		RotCon = GetComponent<RotationConstraint>();
		ConstraintSource constraintSource = new ConstraintSource();
		constraintSource.weight = 1f;
		constraintSource.sourceTransform = RotationSmoother.transform;
		RotCon.AddSource(constraintSource);
		RotCon.constraintActive = true;
	}
	#endregion

	#region Update
	// Update is called once per frame
	int UICollisionCoolDown = 0;
	bool CollisionState = false;

	void Update()
	{

		RotationSmoother.transform.rotation = Quaternion.Lerp(RotationSmoother.transform.rotation, targetTransform.rotation, Time.deltaTime*2f);
		if (Initialised)
		{
			if (!Completed)
			{
				SetGraphics();
				SetTransforms();
				Completed = true;
			}
			if (targetTransform.localScale.x < stats.positionRotationScale.w)
			{
				targetTransform.localScale = Vector3.Lerp(new Vector3(0, 0, 0), new Vector3(stats.positionRotationScale.w, stats.positionRotationScale.w, stats.positionRotationScale.w), createTime / ParameterManager.Instance.ObjectFadeInTime);
				createTime += Time.deltaTime;
			}
			if (targetTransform.localScale.x > stats.positionRotationScale.w)
			{
				targetTransform.localScale = Vector3.Lerp(new Vector3(0, 0, 0), new Vector3(stats.positionRotationScale.w, stats.positionRotationScale.w, stats.positionRotationScale.w), createTime / ParameterManager.Instance.ObjectFadeInTime);
				createTime += Time.deltaTime;
			}
			if (targetTransform.localScale.x == stats.positionRotationScale.w)
			{
				if (!Spawned)
				{
					SetCollider();
					Spawned = true;
				}
			}
			switch (stats.player)
			{
				case UIController.Player.Player1:

					if (!BoundsCheck(GameController.Instance.areaMinP1, GameController.Instance.areaMaxP1, targetTransform))
					{
						OutOfBounds(GameController.Instance.areaMinP1, GameController.Instance.areaMaxP1);
					}
					if (BoundsCheck(UIController.Instance.ReloadP1.GetComponent<ReloadButton>().Min.transform.position, UIController.Instance.ReloadP1.GetComponent<ReloadButton>().Max.transform.position, targetTransform))
					{
						if (!CollisionState)
						{
							targetTransform.up = -targetTransform.up;
							CollisionState = true;
						}

					}
					break;
				case UIController.Player.Player2:

					if (!BoundsCheck(GameController.Instance.areaMinP2, GameController.Instance.areaMaxP2, targetTransform))
					{
						OutOfBounds(GameController.Instance.areaMinP2, GameController.Instance.areaMaxP2);
					}
					if (BoundsCheck(UIController.Instance.ReloadP2.GetComponent<ReloadButton>().Min.transform.position, UIController.Instance.ReloadP2.GetComponent<ReloadButton>().Max.transform.position, targetTransform))
					{
						if (!CollisionState)
						{
							targetTransform.up = -targetTransform.up;
							CollisionState = true;

						}

					}
					break;
				default:
					break;
			}
			if (CollisionState)
			{
				if (!BoundsCheck(stats.player == UIController.Player.Player1 ? UIController.Instance.ReloadP1.GetComponent<ReloadButton>().Min.transform.position : UIController.Instance.ReloadP2.GetComponent<ReloadButton>().Min.transform.position, stats.player == UIController.Player.Player1 ? UIController.Instance.ReloadP1.GetComponent<ReloadButton>().Max.transform.position : UIController.Instance.ReloadP2.GetComponent<ReloadButton>().Max.transform.position, targetTransform))
				{
					CollisionState = false;
				}
			}
			targetTransform.position += (targetTransform.up * stats.speed) * Time.deltaTime;
		}

		if (destroyed)
		{
			spriteRenderer.sprite = splattedSprite;
			objectToDestroy.transform.localScale = Vector3.Lerp(objectToDestroy.transform.localScale, new Vector3(0, 0, 0), destroyTime / ParameterManager.Instance.ObjectFadeOutTime);
			destroyTime += Time.deltaTime;
			if (objectToDestroy.transform.localScale.x < 0.1f)
			{
				objectToDestroy.SetActive(false);
				Destroy(RotationSmoother);
				Destroy(objectToDestroy);
			}
		}
	}
	#region Set Grapics
	void SetGraphics()
	{
#if DEBUG
        LogWriter.Log("Setting Target Graphics");
#endif
		switch (stats.texture.Type)
		{
			case TextureType.GIF:
				stats.texture.CreateSprite(gameObject);
				spriteRenderer.color = stats.color;
				break;
			case TextureType.OBJ:
				break;
			case TextureType.IMG:
				defautSprite = stats.texture.CreateSprite();
				spriteRenderer.sprite = defautSprite;
				spriteRenderer.color = stats.color;
				break;
			case TextureType.MOV:
				break;
			default:
				break;
		}
		switch (stats.splatTex.Type)
		{
			case TextureType.GIF:
				splattedSprite = stats.splatTex.CreateSprite();
				break;
			case TextureType.OBJ:
				break;
			case TextureType.IMG:
				splattedSprite = stats.splatTex.CreateSprite();
				break;
			case TextureType.MOV:
				break;
			default:
				break;
		}

	}
	#endregion
	#region Set Collider
	void SetCollider()
	{
#if DEBUG
        LogWriter.Log("Setting Target Collider");
#endif
		poly2Dcol = gameObject.AddComponent<PolygonCollider2D>();
		if (poly2Dcol.pathCount > 1)
		{
			for (int i = 1; i < poly2Dcol.pathCount; i++)
			{
				poly2Dcol.SetPath(i, new List<Vector2>());
			}
		}
	}
	#endregion
	#region Set Transforms
	void SetTransforms()
	{
#if DEBUG
        LogWriter.Log("Setting Target Transforms");
#endif
		targetTransform.position = new Vector3(stats.positionRotationScale.x, stats.positionRotationScale.y, targetTransform.position.z);
		targetTransform.localScale = new Vector3(0, 0, 0);
		targetTransform.rotation = Quaternion.Euler(0, 0, stats.positionRotationScale.z);
	}
	#endregion
	#region Boundscheck
	public bool BoundsCheck(Vector2 _min, Vector2 _max, Transform _transform)
	{
		Rect rect = Rect.MinMaxRect(_min.x, _min.y, _max.x, _max.y);
		return rect.Contains(_transform.position, true);
	}
	public bool BoundsCheck(Rect playArea, Rect reloadArea, Transform _transform)
	{
		bool _output = playArea.Contains(_transform.position, true) && !reloadArea.Contains(_transform.position, true);

		return _output;
	}
	#endregion
	#region Out of Bounds
	public void OutOfBounds(Vector2 _boundsMin, Vector2 _boundsMax)
	{
		Vector2 _randPosition = MoreRandom.RangeVector2(_boundsMin, _boundsMax);
		Vector3 direction = (_randPosition - (Vector2)targetTransform.position).normalized;
		targetTransform.up = direction;
	}
	#endregion
	#endregion
	#region Accessed Externally
	#region Clear Target
	public void ClearTarget(GameObject _object)
	{
#if DEBUG
        LogWriter.Log("Clearing Target");
#endif
		objectToDestroy = _object;
		destroyed = true;
	}
	#endregion
	#endregion
}
