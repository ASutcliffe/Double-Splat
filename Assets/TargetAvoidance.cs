using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetAvoidance : MonoBehaviour
{
	public Target target;
	public CircleCollider2D circleCollider;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
			target.targetTransform.Rotate(new Vector3(target.targetTransform.eulerAngles.x, target.targetTransform.eulerAngles.y, 180 - Vector2.Angle(collision.gameObject.transform.position, gameObject.transform.position)));
			//Debug.Log(gameObject.name+" Has Collided With "+ collision.gameObject.name + " Angle is " + Vector2.Angle(collision.gameObject.transform.position, gameObject.transform.position));
	}
}
