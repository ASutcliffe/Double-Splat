using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerTargetDisplay : MonoBehaviour
{
    public Image TargetImage,BackgroundImg,InlayImg;
    public UIController.Player player;
    public RectTransform background,inlay,target;
    private void Awake()
    {
        background.sizeDelta = new Vector2(Screen.width * 0.0703125f, Screen.height * 0.125f);
        inlay.sizeDelta = new Vector2(background.sizeDelta.x,background.sizeDelta.y);
        target.sizeDelta = new Vector2(inlay.sizeDelta.x-10, inlay.sizeDelta.x -10);
    }
	public void SetStyle()
	{
		BackgroundImg.sprite = ParameterManager.Instance.TargetBorder.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.TargetBorder.Dimensions.x, ParameterManager.Instance.TargetBorder.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			128f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		BackgroundImg.type = Image.Type.Tiled;
		InlayImg.sprite = ParameterManager.Instance.TargetInlay.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.TargetInlay.Dimensions.x, ParameterManager.Instance.TargetInlay.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			128f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		InlayImg.type = Image.Type.Tiled;
	}
}
