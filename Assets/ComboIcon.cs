using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ComboIcon : MonoBehaviour
{
	public UIController.Player player;
	public Image ComboImage;
	public RectTransform background, inlay, combo;
	private void Awake()
	{
		background.sizeDelta = new Vector2(Screen.width * 0.3359375f, Screen.height * 0.15625f);
		inlay.sizeDelta = new Vector2(background.sizeDelta.x-10, background.sizeDelta.y-10);
		combo.sizeDelta = new Vector2 (inlay.sizeDelta.x-5, inlay.sizeDelta.y-5);
	}
}
