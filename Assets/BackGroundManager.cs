using System.Collections.Generic;
using UnityEngine;

public class BackGroundManager : MonoBehaviour
{

	private void OnEnable()
	{
		BackgroundTypeCheck();
		EffectController.Instance.BackGroundLoadStatus = true;
	}

	enum BackgroundType { Split, Shared, Reflex }
	private bool
		reflexCameraFound = false;
	BackgroundType currentType;
	BackgroundType BackgroundMode
	{
		get{return currentType;}
		set
		{
			currentType = value;
			switch (value)
			{
				case BackgroundType.Split:
					backGroundPlayerOne.SetActive(true);
					backGroundPlayerTwo.SetActive(true);
					backGroundShared.SetActive(false);
					reflexBackground.SetActive(false);
					break;
				case BackgroundType.Shared:
					backGroundPlayerOne.SetActive(false);
					backGroundPlayerTwo.SetActive(false);
					backGroundShared.SetActive(true);
					reflexBackground.SetActive(false);
					break;
				case BackgroundType.Reflex:
					backGroundPlayerOne.SetActive(false);
					backGroundPlayerTwo.SetActive(false);
					backGroundShared.SetActive(false);
					reflexBackground.SetActive(true);
					break;
				default:
					break;
			}
		}
	}

	public GameObject
		backGroundPlayerOne,
		backGroundPlayerTwo,
		backGroundShared,
		reflexBackground;
	private List<Color>
		backGroundColors = new List<Color>();


	void BackgroundTypeCheck()
	{

		foreach (var tex in ParameterManager.Instance.Background)
		{
			if (tex.name.ToLower().Contains("reflex"))
			{
				setReflexBackground();
			}
			else
			{

				break;
			}
		}
		setBackground();
	}
	void setReflexBackground()
	{
		BackgroundMode = BackgroundType.Reflex;
		WebCamDevice[] devices = WebCamTexture.devices;
		WebCamTexture webCamTexture = new WebCamTexture();
		string _cameraFeed = " ";
		_cameraFeed = ParameterManager.Instance.CameraFeed;
		_cameraFeed = _cameraFeed.Substring(_cameraFeed.IndexOf(' ') + 1);
		Debug.Log(_cameraFeed);
		foreach (var cam in devices)
		{
			if (cam.name == _cameraFeed)
			{
				Renderer sr_reflexBackground = reflexBackground.GetComponent<Renderer>();
				webCamTexture.deviceName = cam.name;
				webCamTexture.Play();
				sr_reflexBackground.material.mainTexture = webCamTexture;
				reflexBackground.transform.position = new Vector3(0.0f, 0.0f, 10f);
				reflexBackground.transform.localScale = new Vector3(-((UIController.Instance.cameraWorldSpaceDimensions.x * 2) / sr_reflexBackground.bounds.size.x), 0f, (UIController.Instance.cameraWorldSpaceDimensions.y * 2) / sr_reflexBackground.bounds.size.y);


				reflexCameraFound = true;
			}
		}
		if (!reflexCameraFound)
		{
			Debug.LogError("Reflex Camera Not Found");
		}
	}

	void setBackground()
	{
		reflexBackground.SetActive(false);

		if (ParameterManager.Instance.BackgroundSplitMode)
		{
			#region Split Background
			BackgroundMode = BackgroundType.Split;
			OmiTexture _backgroundTexP1, _backgroundTexP2;
			Color _backGroundColorP1, _backGroundColorP2;
			SpriteRenderer sr_backGroundPlayerOne = backGroundPlayerOne.GetComponent<SpriteRenderer>();
			SpriteRenderer sr_backGroundPlayerTwo = backGroundPlayerTwo.GetComponent<SpriteRenderer>();
			#region Background Count Check
			if (ParameterManager.Instance.Background.Count > 1)
			{
				_backgroundTexP1 = ParameterManager.Instance.Background[0];
				_backgroundTexP2 = ParameterManager.Instance.Background[1];
			}
			else
			{
				_backgroundTexP1 = ParameterManager.Instance.Background[0];
				_backgroundTexP2 = ParameterManager.Instance.Background[0];
			}
			if (backGroundColors.Count > 1)
			{
				_backGroundColorP1 = ParameterManager.Instance.BackgroundColors[0];
				_backGroundColorP2 = ParameterManager.Instance.BackgroundColors[1];
			}
			else
			{
				_backGroundColorP1 = ParameterManager.Instance.BackgroundColors[0];
				_backGroundColorP2 = ParameterManager.Instance.BackgroundColors[0];
			}
			#endregion
			#region Type Check
			switch (_backgroundTexP1.Type)
			{
				case TextureType.GIF:
					_backgroundTexP1.CreateSprite(backGroundPlayerOne);
					break;
				case TextureType.OBJ:
					break;
				case TextureType.IMG:
					sr_backGroundPlayerOne.sprite = _backgroundTexP1.CreateSprite();
					break;
				case TextureType.MOV:
					OmiMovie omiMovie = (OmiMovie)_backgroundTexP1;
					omiMovie.SetUpMovie(backGroundPlayerOne, true, false);
					omiMovie.Play();
					break;
				default:
					break;
			}
			switch (_backgroundTexP2.Type)
			{
				case TextureType.GIF:
					_backgroundTexP2.CreateSprite(backGroundPlayerTwo);
					break;
				case TextureType.OBJ:
					break;
				case TextureType.IMG:
					sr_backGroundPlayerTwo.sprite = _backgroundTexP2.CreateSprite();
					break;
				case TextureType.MOV:
					OmiMovie omiMovie = (OmiMovie)_backgroundTexP2;
					omiMovie.SetUpMovie(backGroundPlayerTwo.gameObject, true, false);
					omiMovie.Play();
					break;
				default:
					break;
			}
			#endregion
			#region Set Colour
			sr_backGroundPlayerOne.color = _backGroundColorP1;
			sr_backGroundPlayerTwo.color = _backGroundColorP2;
			#endregion
			#region Scale BackGround To Camera
			backGroundPlayerOne.transform.position = new Vector3(-UIController.Instance.cameraWorldSpaceDimensions.x / 2, 0f, 10f);
			backGroundPlayerTwo.transform.position = new Vector3(UIController.Instance.cameraWorldSpaceDimensions.x / 2, 0f, 10f);
			backGroundPlayerOne.transform.localScale = new Vector2((UIController.Instance.cameraWorldSpaceDimensions.y * 2) / backGroundPlayerOne.GetComponent<SpriteRenderer>().bounds.size.y, UIController.Instance.cameraWorldSpaceDimensions.x / backGroundPlayerOne.GetComponent<SpriteRenderer>().bounds.size.x);
			backGroundPlayerTwo.transform.localScale = new Vector2((UIController.Instance.cameraWorldSpaceDimensions.y * 2) / backGroundPlayerTwo.GetComponent<SpriteRenderer>().bounds.size.y, UIController.Instance.cameraWorldSpaceDimensions.x / backGroundPlayerTwo.GetComponent<SpriteRenderer>().bounds.size.x);
			#endregion
			#endregion
		}
		else
		{
			#region Shared Background
			BackgroundMode = BackgroundType.Shared;
			OmiTexture _backGroundSharedTex;
			Color _backGroundSharedCol;
			SpriteRenderer sr_backGroundShared = backGroundShared.GetComponent<SpriteRenderer>();
			#region Get Backround Texture and Colour from List
			_backGroundSharedTex = ParameterManager.Instance.Background[0];
			_backGroundSharedCol = ParameterManager.Instance.BackgroundColors[0];
			#endregion
			#region Type Check
			switch (_backGroundSharedTex.Type)
			{
				case TextureType.GIF:
					_backGroundSharedTex.CreateSprite(backGroundShared);
					break;
				case TextureType.OBJ:
					break;
				case TextureType.IMG:
					sr_backGroundShared.sprite = _backGroundSharedTex.CreateSprite();
					break;
				case TextureType.MOV:
					OmiMovie omiMovie = (OmiMovie)_backGroundSharedTex;
					omiMovie.SetUpMovie(backGroundShared, true, false);
					omiMovie.Play();
					break;
				default:
					break;
			}

			#endregion
			#region Set Colour
			sr_backGroundShared.color = _backGroundSharedCol;
			#endregion
			#region Scale Background To Camera
			backGroundShared.transform.position = new Vector3(0.0f, 0.0f, 10f);

			backGroundShared.transform.localScale = new Vector2((UIController.Instance.cameraWorldSpaceDimensions.x * 2) / backGroundShared.GetComponent<SpriteRenderer>().bounds.size.x, (UIController.Instance.cameraWorldSpaceDimensions.y * 2) / backGroundShared.GetComponent<SpriteRenderer>().bounds.size.y);
			#endregion
			#endregion
		}
	}
}
