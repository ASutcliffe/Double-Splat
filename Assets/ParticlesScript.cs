using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesScript : MonoBehaviour
{
	public ParticleSystem Particles;
	public float Min = 5f, Max = 100f;
    // Start is called before the first frame update
	
	public void SetPosition(Vector2 Position,Vector3 Rotation,bool vertical)
	{
		gameObject.transform.position = Position;
		gameObject.transform.eulerAngles = Rotation;
		ParticleSystem.ShapeModule shape = Particles.GetComponent<ParticleSystem>().shape;
		if (vertical)
		{
			
			shape.radius = (UIController.Instance.cameraWorldSpaceDimensions.x/2)*0.5f;

		}
		else
		{
			shape.radius = (UIController.Instance.cameraWorldSpaceDimensions.y / 2);
		}
	}
	public void Play()
	{
		Particles.Play();
	}
	public void Stop()
	{
		Particles.Stop();
	}

	public void SetParticleImage(Sprite _particleSprite)
	{
		Particles.GetComponent<Renderer>().material.SetTexture("_BaseMap",_particleSprite.texture);
	}
	public void EmissionRate(float _lerpVal)
	{
		ParticleSystem.EmissionModule emission = Particles.GetComponent<ParticleSystem>().emission;

		emission.rateOverTime = Mathf.Lerp(Min, Max, _lerpVal);
	}
}
