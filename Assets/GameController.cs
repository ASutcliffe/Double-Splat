#define DEBUG
///When building product should always #undef DEBUG
#undef DEBUG
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
	public static GameController Instance = null;
	#region Enumerators
	public enum PlayArea
	{
		Left,
		Right,
		Full,
		Custom,
		Count
	}

	#endregion
	#region Basic Variables
	#region Public
	public byte
		modifiers;
	/*
	 * Random   Movement    1
	 * Random   Rotation    2
	 * Random   Scale       4
	 * Random   Target      8
	 * Random   Color       16
	 * ReloadBetweenHits    32
	*/
	public float movementSpeedP1
	{
		get { return _movementSpeedP1; }
		set
		{
			_movementSpeedP1 = value;
			foreach (GameObject _obj in objectsP1)
			{
				_obj.GetComponentInChildren<Target>().stats.SetSpeed(_movementSpeedP1);
			}
		}
	}
	public float movementSpeedP2
	{
		get { return _movementSpeedP2; }
		set
		{
			_movementSpeedP2 = value;
			foreach (GameObject _obj in objectsP2)
			{
				_obj.GetComponentInChildren<Target>().stats.SetSpeed(_movementSpeedP2);
			}
		}
	}
	public float scaleP1
	{
		get { return _scaleP1; }
		set
		{
			_scaleP1 = value;
			foreach (GameObject _obj in objectsP1)
			{
				_obj.GetComponentInChildren<Target>().stats.SetScale(_scaleP1);
			}
		}
	}
	public float scaleP2
	{
		get { return _scaleP2; }
		set
		{
			_scaleP2 = value;
			foreach (GameObject _obj in objectsP2)
			{
				_obj.GetComponentInChildren<Target>().stats.SetScale(_scaleP2);
			}
		}
	}
	public int
		itemPerRoundP1 = 1,
		itemPerRoundP2 = 1,
		itemsPerRound = 1;
	public bool ShowAreaP1
	{
		set
		{
			_showAreaP1 = value;
			if (_showAreaP1)
			{
				areaHighlightP1.SetActive(true);
			}
			else
			{
				areaHighlightP1.SetActive(false);
			}

		}
		get
		{
			return _showAreaP1;
		}
	}
	public bool ShowAreaP2
	{
		set
		{
			_showAreaP2 = value;
			if (_showAreaP2)
			{
				areaHighlightP2.SetActive(true);
			}
			else
			{
				areaHighlightP2.SetActive(false);
			}

		}
		get
		{
			return _showAreaP2;
		}
	}
	public bool ShowHighlightP2
	{
		set
		{
			_showHighlightP2 = value;
			if (_showHighlightP2)
			{
				highlightP2.SetActive(true);
			}
			else
			{
				highlightP2.SetActive(false);
			}

		}
		get
		{
			return _showHighlightP2;
		}
	}

	public bool ShowHighlightP1
	{
		set
		{
			_showHighlightP1 = value;
			if (_showHighlightP1)
			{
				highlightP1.SetActive(true);
			}
			else
			{
				highlightP1.SetActive(false);
			}

		}
		get
		{
			return _showHighlightP1;
		}
	}

	#endregion
	#region Private
	private float
		_movementSpeedP1 = 0f,
		_movementSpeedP2 = 0f;
	[SerializeField]
	private float _scaleP1;
	[SerializeField]
	private float _scaleP2;
	private bool
		_showAreaP1 = false,
		_showAreaP2 = false,
		_showHighlightP1 = false,
		_showHighlightP2 = false;
	#endregion
	#endregion
	#region Complex Variables
	#region Public

	public AudioClip
		backgroundMusicClip;
	public AudioManager
		audioManager,
		audioManagerP1,
		audioManagerp2;
	public GameObject
		PF_target,
		areaP1,
		areaP2,
		areaHighlightP1,
		areaHighlightP2,
		highlightP1,
		highlightP2;
	public Color colorP1, colorP2;
	public List<GameObject>
		objectsP1 = new List<GameObject>(),
		objectsP2 = new List<GameObject>();
	public Vector2Int targetP1
	{
		set
		{
			_targetP1 = value;
#if DEBUG
			LogWriter.Log("Setting Player 1 Target");
#endif
			Player.Player1.target = _targetP1;
		}
		get { return _targetP1; }

	}
	public Vector2Int targetP2
	{
		set
		{
			_targetP2 = value;
#if DEBUG
			LogWriter.Log("Setting Player 2 Target");
#endif
			Player.Player2.target = _targetP2;
		}
		get { return _targetP2; }

	}
	public Vector2
		areaMinP1,
		areaMaxP1,
		areaMinP2,
		areaMaxP2;
	public PlayArea areaModeP1
	{
		set
		{
			_areaModeP1 = value;
			if (value != PlayArea.Count)
			{
				player1Area = SetPlayArea(playAreaDictionary[(ParameterManager.Instance.VerticalPlay ? "V" : "H") + ("P1") + _areaModeP1.ToString().ToUpper()]);
				PlayerArea(areaP1.transform, ref areaMinP1, ref areaMaxP1, player1Area);
				areaHighlightP1.GetComponent<SpriteRenderer>().size = new Vector2(areaMaxP1.x - areaMinP1.x, areaMaxP1.y - areaMinP1.y);
				areaHighlightP1.transform.position = new Vector2((areaMaxP1.x + areaMinP1.x) / 2, (areaMaxP1.y + areaMinP1.y) / 2);
			}
		}
		get { return _areaModeP1; }
	}
	public PlayArea areaModeP2
	{
		set
		{
			_areaModeP2 = value;
			if (value != PlayArea.Count)
			{
				player2Area = SetPlayArea(playAreaDictionary[(ParameterManager.Instance.VerticalPlay ? "V" : "H") + ("P2") + _areaModeP2.ToString().ToUpper()]);
				PlayerArea(areaP2.transform, ref areaMinP2, ref areaMaxP2, player2Area);
				areaHighlightP2.GetComponent<SpriteRenderer>().size = new Vector2(areaMaxP2.x - areaMinP2.x, areaMaxP2.y - areaMinP2.y);
				areaHighlightP2.transform.position = new Vector2((areaMaxP2.x + areaMinP2.x) / 2, (areaMaxP2.y + areaMinP2.y) / 2);

			}
		}
		get { return _areaModeP2; }
	}
	#endregion
	#region Private
	private PlayArea
		_areaModeP1 = PlayArea.Full,
		_areaModeP2 = PlayArea.Full;
	private Vector4
		player1Area,
		player2Area;
	private Vector2Int
		_targetP1,
		_targetP2;
	//ScreenSpace v4 xMax,yMax,xMin,YMin
	Dictionary<string, Vector4> playAreaDictionary = new Dictionary<string, Vector4>()
	{
		{"HP1LEFT",new Vector4(0.7f,1f,1f,0f)},
		{"HP1RIGHT",new Vector4(0.7f,0f,1f,1f)},
		{"HP1FULL",new Vector4(0.7f,1f,1f,1f)},
		{"HP2LEFT",new Vector4(1f,0f,0.7f,1f)},
		{"HP2RIGHT",new Vector4(1f,1f,0.7f,0f)},
		{"HP2FULL",new Vector4(1f,1f,0.7f,1f)},
		{"VP1LEFT",new Vector4(0f,0.7f,1f,1f)},
		{"VP1RIGHT",new Vector4(1f,0.7f,0f,1f)},
		{"VP1FULL",new Vector4(1f,0.7f,1f,1f)},
		{"VP2LEFT",new Vector4(0f,0.7f,1f,1f)},
		{"VP2RIGHT",new Vector4(1f,0.7f,0f,1f)},
		{"VP2FULL",new Vector4(1f,0.7f,1f,1f)}

	};
	#endregion
	#endregion

	#region Awake
	private void Awake()
	{
#if DEBUG
		LogWriter.Log("Game Controller is Awake");
#endif
		Instance = this;
		areaP1.transform.position = new Vector3(-(UIController.Instance.cameraWorldSpaceDimensions.x/2),0,0);
		areaP2.transform.position = new Vector3(UIController.Instance.cameraWorldSpaceDimensions.x / 2, 0, 0);

	}
	#endregion
	private void OnEnable()
	{
#if DEBUG
		LogWriter.Log("Game Controller Started");
#endif
		InitBackgroundMusic();
		InitPlayAreas();
		InitObjectSpeed();
		InitScale();
		InitPlayerColours();
		InitItemsPerRound();
		FetchModesFromParameters();
		audioManager.Play(1f, false, true);
		PlayerArea(areaP1.transform, ref areaMinP1, ref areaMaxP1, player1Area);
		PlayerArea(areaP2.transform, ref areaMinP2, ref areaMaxP2, player2Area);
		EffectController.Instance.GameControllerLoadStatus = true;
	}
	#region Start


	void InitBackgroundMusic()
	{
		audioManager.Setup(ParameterManager.Instance.BackgroundSound, ref backgroundMusicClip);

	}

	void InitScale()
	{
		scaleP1 = ParameterManager.Instance.ObjectScale;
		scaleP2 = ParameterManager.Instance.ObjectScale;
	}

	void InitPlayAreas()
	{
		SetCustomPlayAreas();
		player1Area = SetPlayArea(playAreaDictionary[(ParameterManager.Instance.VerticalPlay ? "V" : "H") + ("P1") + areaModeP1.ToString().ToUpper()]);
		player2Area = SetPlayArea(playAreaDictionary[(ParameterManager.Instance.VerticalPlay ? "V" : "H") + ("P2") + areaModeP2.ToString().ToUpper()]);
	}

	void InitObjectSpeed()
	{
		movementSpeedP1 = ParameterManager.Instance.ObjectSpeed;
		movementSpeedP2 = ParameterManager.Instance.ObjectSpeed;

	}

	void InitPlayerColours()
	{
		setPlayerColors();
		SpriteRenderer _hlp1sr = highlightP1.GetComponent<SpriteRenderer>();
		SpriteRenderer _hlp2sr = highlightP2.GetComponent<SpriteRenderer>();
		_hlp1sr.color = new Color(colorP1.r, colorP1.g, colorP1.b, 0.1f);
		_hlp2sr.color = new Color(colorP2.r, colorP2.g, colorP2.b, 0.1f);
		UIController.Instance.ReloadP1.GetComponent<ReloadButton>().Icon.color = colorP1;
		UIController.Instance.ReloadP2.GetComponent<ReloadButton>().Icon.color = colorP2;
	}
	void InitItemsPerRound()
	{
		itemPerRoundP1 = ParameterManager.Instance.ItemsPerRound;
		itemPerRoundP2 = ParameterManager.Instance.ItemsPerRound;
	}

	private void setPlayerColors()
	{
		if (ParameterManager.Instance.PlayerColour.Count > 1)
		{
			colorP1 = ParameterManager.Instance.PlayerColour[0];
			colorP2 = ParameterManager.Instance.PlayerColour[1];
		}
		else
		{
			colorP1 = ParameterManager.Instance.PlayerColour[0];
			colorP2 = ParameterManager.Instance.PlayerColour[0];
		}
	}
	#region SetCustomPlayAreas
	void SetCustomPlayAreas()
	{
				playAreaDictionary.Add("HP1CUSTOM",
					ParameterManager.Instance.PlayArea);
				playAreaDictionary.Add("VP1CUSTOM",
					ParameterManager.Instance.PlayArea);
				playAreaDictionary.Add("HP2CUSTOM", new Vector4(
					ParameterManager.Instance.PlayArea.z,
					ParameterManager.Instance.PlayArea.w,
					ParameterManager.Instance.PlayArea.x,
					ParameterManager.Instance.PlayArea.y));
				playAreaDictionary.Add("VP2CUSTOM",
					ParameterManager.Instance.PlayArea);
	}
	#endregion
	#region Fetch Modes From Parameters
	/// <summary>
	/// Sets mode from parameters
	/// </summary>
	void FetchModesFromParameters()
	{
#if DEBUG
		Debug.Log("Fetching Parameters for Modes");
#endif
		bool _tmpBool = false;
		_tmpBool = ParameterManager.Instance.RandomMovement;
		modifiers = (byte)(modifiers | (_tmpBool ? 1 : 0));
		_tmpBool = ParameterManager.Instance.RandomRotation; 
		modifiers = (byte)(modifiers | (_tmpBool ? 2 : 0));
		_tmpBool = ParameterManager.Instance.RandomScale;
		modifiers = (byte)(modifiers | (_tmpBool ? 4 : 0));
		_tmpBool = ParameterManager.Instance.RandomTarget;
		modifiers = (byte)(modifiers | (_tmpBool ? 8 : 0));
		_tmpBool = ParameterManager.Instance.RandomColor;
		modifiers = (byte)(modifiers | (_tmpBool ? 16 : 0));
		_tmpBool = ParameterManager.Instance.ReloadBetweenHits;
		modifiers = (byte)(modifiers | (_tmpBool ? 32 : 0));
	}
	#endregion
	#region Set Play Area
	/// <summary>
	/// Sets a world space area based on a normalized input
	/// </summary>
	/// <param name="_area"></param>
	/// <returns></returns>
	Vector4 SetPlayArea(Vector4 _area)
	{
#if DEBUG
		LogWriter.Log("Setting Play Area in world space from normalised value");
#endif
		Vector4 _playArea = new Vector4(((UIController.Instance.cameraWorldSpaceDimensions.x/2)-1f) * _area.x, ((UIController.Instance.cameraWorldSpaceDimensions.y)-1f) * _area.y, -((UIController.Instance.cameraWorldSpaceDimensions.x / 2)-1f) * _area.z, -((UIController.Instance.cameraWorldSpaceDimensions.y)-1) * _area.w);
		return _playArea;
	}
	#endregion
	#endregion
	#region Player Area
	/// <summary>
	/// Sets the Play Area for the chosen player
	/// </summary>
	/// <param name="_playArea">The parent transform of a play area needs two children one tagged as "Min" the other "Max"</param>
	/// <param name="_playAreaMin">Refrenced Global Variable for the max position</param>
	/// <param name="_playAreaMax">Refrenced Global Variable for the min position</param>
	/// <param name="_playAreaSizeFromCenter">Sets the Positions of the min and max from the parameters</param>
	void PlayerArea(Transform _playArea, ref Vector2 _playAreaMin, ref Vector2 _playAreaMax, Vector4 _playAreaSizeFromCenter)
	{
#if DEBUG
		LogWriter.Log("Setting up Player Area");
#endif
		foreach (Transform _tf in _playArea)
		{
			if (_tf.tag == "Min")
			{
				_tf.localPosition = new Vector2(_playAreaSizeFromCenter.z, _playAreaSizeFromCenter.w);
				_playAreaMin = new Vector2(_tf.position.x, _tf.position.y);
			}
			if (_tf.tag == "Max")
			{
				_tf.localPosition = new Vector2(_playAreaSizeFromCenter.x, _playAreaSizeFromCenter.y);
				_playAreaMax = new Vector2(_tf.position.x, _tf.position.y);
			}
		}
	}
	#endregion
	#region Update
	// Update is called once per frame
	void Update()
	{
		#region NewRound Test Button
#if DEBUG
		if (Input.GetKeyDown(KeyCode.O))
		{
			NewRound(
				ref objectsP1,
				ref PF_target,
				ref areaMinP1,
				ref areaMaxP1,
				UIController.Player.Player1);

			NewRound(
				ref objectsP2,
				ref PF_target,
				ref areaMinP2,
				ref areaMaxP2,
				UIController.Player.Player2);
		}
#endif
		#endregion
	}
	#endregion
	#region Accessed Only Externally
	#region Game Start
	/// <summary>
	/// Creates the Initial Round
	/// </summary>
	public void GameStart()
	{
#if DEBUG
		LogWriter.Log("GameController Game Start Generating Intial Rounds");
#endif
		DynamicVariableManager.Instance.SetDifficulty(DynamicVariableManager.Instance.player1Difficulty, UIController.Player.Player1);
		DynamicVariableManager.Instance.SetDifficulty(DynamicVariableManager.Instance.player2Difficulty, UIController.Player.Player2);

		NewRound(
			ref objectsP1,
			ref PF_target,
			ref areaMinP1,
			ref areaMaxP1,
			UIController.Player.Player1);

		NewRound(
			ref objectsP2,
			ref PF_target,
			ref areaMinP2,
			ref areaMaxP2,
			UIController.Player.Player2);
	}

	#endregion
	#region Target Hit
	/// <summary>
	/// applies score to player and sets up a new round
	/// </summary>
	/// <param name="_player"></param>
	public void TargetHit(UIController.Player _player)
	{
		switch (_player)
		{
			case UIController.Player.Player1:
					UIController.Instance.scorePlayer1 += player1ComboCount;
				NewRound(
					ref objectsP1,
					ref PF_target,
					ref areaMinP1,
					ref areaMaxP1,
					UIController.Player.Player1);
				break;
			case UIController.Player.Player2:
					UIController.Instance.scorePlayer2 += player2ComboCount;
				NewRound(
					ref objectsP2,
					ref PF_target,
					ref areaMinP2,
					ref areaMaxP2,
					UIController.Player.Player2);
				break;
			default:
				break;
		}
	}

	#region Combo
	public int 
		player1ComboCount = 1,
		player2ComboCount = 1;
	public void combo(UIController.Player _player,bool Break = false)
	{
		switch (_player)
		{
			case UIController.Player.Player1:
				if (!Break)
				{
					player1ComboCount = Mathf.Clamp(player1ComboCount + 1, 1, 7);
				}
				else
				{
					player1ComboCount = 1;
				}
				//Debug.Log(_player + " GameController Combo Value =" + player1ComboCount);
				UIController.Instance.comboBarP1.GetComponent<ComboBar>().ComboLevel = player1ComboCount - 1;

				break;
			case UIController.Player.Player2:
				if (!Break)
				{
					player2ComboCount = Mathf.Clamp(player2ComboCount+1, 1, 7);
				}
				else
				{
					player2ComboCount = 1;
				}
				UIController.Instance.comboBarP2.GetComponent<ComboBar>().ComboLevel = player2ComboCount - 1;

				break;
			default:
				break;
		}
	}
	#endregion
	int P1RoundCount = 0, P2RoundCount = 0;
	void ReloadSideSwap(UIController.Player _player)
	{
		if (_player == UIController.Player.Player1)
		{
			P1RoundCount++;
			if (P1RoundCount > ParameterManager.Instance.RoundsBetweenReloadSwaps)
			{
				UIController.Instance.ReloadPositionP1 = 1f - UIController.Instance.ReloadPositionP1;
				P1RoundCount = 0;
			}
		}
		else
		{
			P2RoundCount++;
			if (P2RoundCount > ParameterManager.Instance.RoundsBetweenReloadSwaps)
			{
				UIController.Instance.ReloadPositionP2 = 1f - UIController.Instance.ReloadPositionP2;
				P2RoundCount = 0;
			}
		}
	}

	#region New Round
	/// <summary>
	/// Sets up a new round by removing all the selected players objects and generating a new target as well as a bunch of clutter objects
	/// </summary>
	/// <param name="_gameObjectsList">This is the list of objects the player can see target plus clutter</param>
	/// <param name="_goPrefab">Prefab is a foundation gameobject used to build both the target and clutter</param>
	/// <param name="_playAreaMin">Minimum point that objects are spawned</param>
	/// <param name="_playAreaMax">Maximum Point that objects are spawned</param>
	/// <param name="_player">Enum from the UI Controller lets you pick between working with player 1 and 2 used for UI Managment</param>
	void NewRound(ref List<GameObject> _gameObjectsList, ref GameObject _goPrefab, ref Vector2 _playAreaMin, ref Vector2 _playAreaMax, UIController.Player _player)
	{
		#region Clear Old Round
		ClearObjects(ref _gameObjectsList);
		if (UIController.Instance.GamePlaying)
		{
			if (ParameterManager.Instance.ReloadSwapSides)
			{
				ReloadSideSwap(_player);
			}
			_gameObjectsList = new List<GameObject>();
			#endregion
			#region Target Spawn
			GameObject _tmp = Instantiate(_goPrefab);
			Vector2 spawnMin = _playAreaMin, spawnMax = _playAreaMax;

			if (ParameterManager.Instance.ReloadBetweenHits)
			{
				if (ParameterManager.Instance.VerticalPlay)
				{
					if (_player == UIController.Player.Player1)
					{
						spawnMax = _playAreaMax;
						spawnMin = new Vector2(_playAreaMin.x, UIController.Instance.ReloadP1.GetComponent<ReloadButton>().Max.transform.position.y);
					}
					else
					{
						spawnMax = _playAreaMax;
						spawnMin = new Vector2(_playAreaMin.x, UIController.Instance.ReloadP2.GetComponent<ReloadButton>().Max.transform.position.y);
					}
				}
				else
				{
					if (_player == UIController.Player.Player1)
					{
						spawnMax = _playAreaMax;
						spawnMin = new Vector2(UIController.Instance.ReloadP1.GetComponent<ReloadButton>().Max.transform.position.x, _playAreaMin.y);
					}
					else
					{
						spawnMax = new Vector2(UIController.Instance.ReloadP2.GetComponent<ReloadButton>().Max.transform.position.x, _playAreaMax.y);
						spawnMin = _playAreaMin;

					}
				}
			}
			else
			{
				spawnMin = _playAreaMin;
				spawnMax = _playAreaMax;
			}
			_tmp.GetComponentInChildren<Target>().stats = new TargetStats(
				_ID: new Vector2Int((modifiers & 8) == 8 ? Random.Range(0, ParameterManager.Instance.ObjectImages.Count) : 0,(modifiers & 16) == 16 ? Random.Range(0, ParameterManager.Instance.ObjectColors.Count) : 0),
				_position: MoreRandom.RangeVector2(spawnMin, spawnMax),
				_rotation: (modifiers & 2) == 2 ? Random.Range(0f, 360f) : -90f,
				_scale: (modifiers & 4) == 4 && (_player == UIController.Player.Player1 ? DynamicVariableManager.Instance.player1Difficulty != DynamicVariableManager.Difficulty.Easy:DynamicVariableManager.Instance.player2Difficulty != DynamicVariableManager.Difficulty.Easy)?Random.Range(0.5f, _player == UIController.Player.Player1 ? scaleP1 : scaleP2) : _player == UIController.Player.Player1 ? scaleP1 :	scaleP2,
				_speed: (modifiers & 1) == 1 ?Random.Range(0f, _player == UIController.Player.Player1 ? movementSpeedP1 : movementSpeedP2) :_player == UIController.Player.Player1 ? movementSpeedP1 : movementSpeedP2,
				_player: _player);
			switch (_player)
			{
				case UIController.Player.Player1:
					targetP1 = _tmp.GetComponentInChildren<Target>().stats.ID;
					UIController.Instance.targetTextureP1 = ParameterManager.Instance.ObjectImages[targetP1.x];
					UIController.Instance.targetColorP1 = ParameterManager.Instance.ObjectColors[targetP1.y];
					break;
				case UIController.Player.Player2:
					targetP2 = _tmp.GetComponentInChildren<Target>().stats.ID;
					UIController.Instance.targetTextureP2 = ParameterManager.Instance.ObjectImages[targetP2.x];
					UIController.Instance.targetColorP2 = ParameterManager.Instance.ObjectColors[targetP2.y];
					break;
				default:
					break;
			}

			_gameObjectsList.Add(_tmp);
			_tmp.SetActive(false);
			#endregion
			#region Clutter Spawn

			while (_gameObjectsList.Count < (_player == UIController.Player.Player1 ? itemPerRoundP1 : itemPerRoundP2))
			{
				GameObject _tmpLoop = Instantiate(_goPrefab);
				_tmpLoop.GetComponentInChildren<Target>().stats = new TargetStats(
					MoreRandom.RangeVector2Int(new Vector2Int(0, 0), new Vector2Int(ParameterManager.Instance.ObjectImages.Count, ParameterManager.Instance.ObjectColors.Count)),
					MoreRandom.RangeVector2(spawnMin, spawnMax),
					Random.Range(0f, 360f),
				(modifiers & 4) == 4 && (_player == UIController.Player.Player1 ? DynamicVariableManager.Instance.player1Difficulty != DynamicVariableManager.Difficulty.Easy : DynamicVariableManager.Instance.player1Difficulty != DynamicVariableManager.Difficulty.Easy) ?
					Random.Range(0.5f, _player == UIController.Player.Player1 ? scaleP1 : scaleP2) : _player == UIController.Player.Player1 ? scaleP1 : scaleP2,
					(modifiers & 1) == 1 ?
						Random.Range(0f, _player == UIController.Player.Player1 ? movementSpeedP1 : movementSpeedP2) :
						_player == UIController.Player.Player1 ? movementSpeedP1 : movementSpeedP2,
					_player);
				_gameObjectsList.Add(_tmpLoop);
				_tmpLoop.SetActive(false);
				_tmpLoop.transform.position = new Vector3(_tmpLoop.transform.position.x, _tmpLoop.transform.position.y, ((float)_gameObjectsList.Count / (_player == UIController.Player.Player1 ? itemPerRoundP1 : itemPerRoundP2)));
			}
			#endregion
			#region Initialise and activate
			foreach (GameObject _obj in _gameObjectsList)
			{
				_obj.GetComponentInChildren<Target>().Initialised = true;
				_obj.SetActive(true);
			}
			#endregion
		}
	}
	#endregion

	#region ClearObjects
	public void ClearObjects(ref List<GameObject> _gameObjectsList)
	{
		foreach (GameObject _obj in _gameObjectsList)
		{
			if (_obj != null)
			{
				_obj.GetComponentInChildren<Target>().ClearTarget(_obj);
			}
		}
		_gameObjectsList = new List<GameObject>();
	}
	#endregion

	#endregion
	#region Hide Setting UI
	public void HideSettingUI()
	{
		ShowAreaP1 = false;
		ShowAreaP2 = false;
		ShowHighlightP1 = false;
		ShowHighlightP2 = false;
	}
	#endregion
	#endregion
}
