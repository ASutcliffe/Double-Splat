using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefreshButton : MonoBehaviour
{
	
    public SpriteRenderer Background,Inlay, Icon;
	
	public void SetStyle()
	{
		Background.sprite = ParameterManager.Instance.RefreshBorder.CreateSprite(
			new Rect(0,0, ParameterManager.Instance.RefreshBorder.Dimensions.x, ParameterManager.Instance.RefreshBorder.Dimensions.y),
			new Vector2(0.5f,0.5f),
			768f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		Inlay.sprite = ParameterManager.Instance.RefreshInlay.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.RefreshInlay.Dimensions.x, ParameterManager.Instance.RefreshInlay.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			768f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		Background.drawMode = SpriteDrawMode.Tiled;
		Inlay.drawMode = SpriteDrawMode.Tiled;
	}
}
