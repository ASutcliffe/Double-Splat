using System;
using System.Collections.Generic;
using UnityEngine;

public class ParameterManager : MonoBehaviour
{
	public static ParameterManager Instance = null;

	public bool
		TimeMode = false,
		ScoreMode = false,
		ReloadBetweenHits = false,
		VerticalPlay = false,
		RandomRotation = false,
		RandomScale = false,
		RandomColor = false,
		RandomTarget = false,
		RandomMovement = false,
		useForeground = false,
		BackgroundSplitMode = false,
		ReticleVisible = false,
		CamXFlip = false,
		CamYFlip = false,
		IsShowingFPS = false,
		ComboMode = false,
		Particles = true,
		ReloadSwapSides = false, 
		PlayAreaCustomMode = false;
	public float
		TimeLimit = 0f,
		ObjectSpeed = 1f,
		ObjectFadeInTime = 0.5f,
		ObjectFadeOutTime = 1f,
		ObjectScale = 1f,
		ComboGracePeriod = 1f;
	public float
		ReloadPosition = 1f;
	public int
		ScoreLimit = 0,
		ItemsPerRound = 5,
		BackgroundCameraHeight = 320,
		BackgroundCameraWidth = 240,
		RoundsBetweenReloadSwaps = 5;
	public string
		Correct = " ",
		Wrong = " ",
		Reload = " ",
		Refresh = " ",
		BackgroundSound = " ",
		CameraFeed = " ",
		Language = "DEFAULT",
		DynamicVariableOption = " ";
	public DynamicVariableManager.Difficulty Difficulty = DynamicVariableManager.Difficulty.Default;
	public Color
		TimerStartColor = Color.green,
		TimerEndColor = Color.red;
	public OmiTexture
		First,
		Second,
		Reticle,
		load,
		LoadingScreenImage,
		ComboTexture,
		RefreshBorder,RefreshInlay,
		ReloadBorder,ReloadInlay,ReloadIcon,
		ComboBarBorder,ComboBarInlay,ComboParticle,
		TimeBarBorder,TimeBarInlay,TimeBarInformation,
		ScoreBoardBorder,ScoreBoardInlay,
		TargetBorder,TargetInlay,
		EndScreenBorder,EndScreenInlay;
	public List<Color>
		ObjectColors = new List<Color>(),
		ForegroundColors = new List<Color>(),
		BackgroundColors = new List<Color>(),
		PlayerColour = new List<Color>();
	public List<OmiTexture>
		ObjectImages = new List<OmiTexture>(),
		ObjectsTappedState = new List<OmiTexture>(),
		ForegroundHorizontal = new List<OmiTexture>(),
		ForegroundVertical = new List<OmiTexture>(),
		Background = new List<OmiTexture>();
	public Vector2
		ReloadSize = new Vector2();
	public Vector2Int
		InteractionDimensions;
	public Vector4
		PlayArea = new Vector4();
	public Globals.DynamicVariables 
		DynamicVariable = Globals.DynamicVariables.GameMode;


	/*
	 * DEPENDENT ON PARAMETERLOADER ENSURE IT LOADS AFTER PARAMETERLOADER HAS FINISHED ITS ENABLE FUCTION
	 */
	private void OnEnable()
	{
		Instance = this;
		LogWriter.Log("ParameterManager Stated Loading");
		LoadingScreenImage = ParametersExtended.Instance.GetTexture("param_LoadingScreenImage");

		#region Bools
		TimeMode = ParameterLoader.Instance.getParamValue("param_TimeModeDS", ref TimeMode);
		ScoreMode = ParameterLoader.Instance.getParamValue("param_ScoreModeDS", ref ScoreMode);
		ReloadBetweenHits = ParameterLoader.Instance.getParamValue("Param_ReloadBetweenHitsDS", ref ReloadBetweenHits);
		VerticalPlay = ParameterLoader.Instance.getParamValue("Param_VerticalPlayDS", ref VerticalPlay);
		RandomRotation = ParameterLoader.Instance.getParamValue("param_RandomRotationDS", ref RandomRotation);
		RandomScale = ParameterLoader.Instance.getParamValue("param_RandomScaleDS", ref RandomScale);
		RandomColor = ParameterLoader.Instance.getParamValue("param_RandomColorDS", ref RandomColor);
		RandomTarget = ParameterLoader.Instance.getParamValue("param_RandomTargetDS", ref RandomTarget);
		RandomMovement = ParameterLoader.Instance.getParamValue("param_RandomMovementDS", ref RandomMovement);
		useForeground = ParameterLoader.Instance.getParamValue("param_useForegroundDS", ref useForeground);
		BackgroundSplitMode = ParameterLoader.Instance.getParamValue("param_BackgroundSplitModeDS", ref BackgroundSplitMode);
		ReticleVisible = ParameterLoader.Instance.getParamValue("param_ReticleVisibleDS", ref ReticleVisible);
		CamXFlip = ParameterLoader.Instance.getParamValue("param_CamXFlip", ref CamXFlip);
		CamYFlip = ParameterLoader.Instance.getParamValue("param_CamYFlip", ref CamYFlip);
		IsShowingFPS = ParameterLoader.Instance.getParamValue("param_IsShowingFPS", ref IsShowingFPS);
		ComboMode = ParameterLoader.Instance.getParamValue("param_ComboModeDS",ref ComboMode);
		Particles = ParameterLoader.Instance.getParamValue("param_ParticlesDS", ref Particles);
		ReloadSwapSides = ParameterLoader.Instance.getParamValue("param_ReloadSwapSidesDS",ref ReloadSwapSides);
		PlayAreaCustomMode = false;
		#endregion

		#region Float
		TimeLimit = ParameterLoader.Instance.getParamValue("param_TimeLimitDS", ref TimeLimit);
		ReloadPosition = ParameterLoader.Instance.getParamValue("param_ReloadPositionDS", ref ReloadPosition);
		ObjectSpeed = ParameterLoader.Instance.getParamValue("param_ObjectSpeedDS", ref ObjectSpeed);
		ObjectScale = ParameterLoader.Instance.getParamValue("param_ScaleDS",ref ObjectScale);
		ObjectFadeInTime = ParameterLoader.Instance.getParamValue("param_ObjectFadeInTimeDS", ref ObjectFadeInTime);
		ObjectFadeOutTime = ParameterLoader.Instance.getParamValue("param_ObjectFadeOutTimeDS", ref ObjectFadeOutTime);
		ComboGracePeriod = ParameterLoader.Instance.getParamValue("param_ComboGracePeriodDS",ref ComboGracePeriod);
		#endregion

		#region Integer 
		ScoreLimit = ParameterLoader.Instance.getParamValue("param_ScoreLimitDS", ref ScoreLimit);
		ItemsPerRound = ParameterLoader.Instance.getParamValue("param_ItemsPerRoundDS", ref ItemsPerRound);
		BackgroundCameraHeight = ParameterLoader.Instance.getParamValue("param_BackgroundCameraHeight", ref BackgroundCameraHeight);
		BackgroundCameraWidth = ParameterLoader.Instance.getParamValue("param_BackgroundCameraWidth", ref BackgroundCameraWidth);
		RoundsBetweenReloadSwaps = ParameterLoader.Instance.getParamValue("param_RoundsBetweenReloadSwapsDS",ref RoundsBetweenReloadSwaps);

		#endregion

		#region String
		Correct = ParameterLoader.Instance.getParamValue("param_CorrectDS", ref Correct);
		Wrong = ParameterLoader.Instance.getParamValue("param_WrongDS", ref Wrong);
		Reload = ParameterLoader.Instance.getParamValue("param_ReloadDS",ref Reload);
		Refresh = ParameterLoader.Instance.getParamValue("param_RefreshDS",ref Refresh);
		BackgroundSound = ParameterLoader.Instance.getParamValue("param_BackgroundSoundDS", ref BackgroundSound);
		CameraFeed = ParameterLoader.Instance.getParamValue("param_CameraFeed", ref CameraFeed);
		Language = ParameterLoader.Instance.getParamValue("param_Language", ref Language);
		DynamicVariableOption = ParameterLoader.Instance.getParamValue("param_DynamicVariable", ref DynamicVariableOption);
		string tmpDiff = " ";
		tmpDiff = ParameterLoader.Instance.getParamValue("param_DifficultyDS", ref tmpDiff);
		Difficulty = (DynamicVariableManager.Difficulty)Enum.Parse(typeof(DynamicVariableManager.Difficulty),tmpDiff,true);
		#endregion

		#region Color
		TimerStartColor = ParametersExtended.Instance.GetColor("param_TimerStartColorDS");
		TimerEndColor = ParametersExtended.Instance.GetColor("param_TimerEndColorDS");
		ObjectColors = ParametersExtended.Instance.GetColorList("param_ObjectColorsDS");
		BackgroundColors = ParametersExtended.Instance.GetColorList("param_BackgroundColorsDS");
		ForegroundColors = ParametersExtended.Instance.GetColorList("param_ForegroundColorsDS");
		PlayerColour = ParametersExtended.Instance.GetColorList("param_PlayerColourDS");
		#endregion

		#region OmiTexture
		First = ParametersExtended.Instance.GetTexture("param_FirstDS");
		Second = ParametersExtended.Instance.GetTexture("param_SecondDS");
		Reticle = ParametersExtended.Instance.GetTexture("param_ReticleDS");
		ObjectImages = ParametersExtended.Instance.GetTextureList("param_ObjectImagesDS");
		ObjectsTappedState = ParametersExtended.Instance.GetTextureList("param_ObjectsTappedStateDS");
		ForegroundVertical = ParametersExtended.Instance.GetTextureList("param_ForegroundVerticalDS");
		ForegroundHorizontal = ParametersExtended.Instance.GetTextureList("param_ForegroundHorizontalDS");
		Background = ParametersExtended.Instance.GetTextureList("param_BackgroundDS");
		ComboTexture = ParametersExtended.Instance.GetTexture("Param_ComboTextureDS");

		#region UI Customization
		RefreshBorder = ParametersExtended.Instance.GetTexture("param_RefreshBorderDS");
		RefreshInlay = ParametersExtended.Instance.GetTexture("param_RefreshInlayDS");
		ReloadBorder = ParametersExtended.Instance.GetTexture("param_ReloadBorderDS");
		ReloadInlay = ParametersExtended.Instance.GetTexture("param_ReloadInlayDS");
		ReloadIcon = ParametersExtended.Instance.GetTexture("param_ReloadIconDS");
		ComboBarBorder = ParametersExtended.Instance.GetTexture("param_ComboBarBorderDS");
		ComboBarInlay = ParametersExtended.Instance.GetTexture("param_ComboBarInlayDS");
		ComboParticle = ParametersExtended.Instance.GetTexture("param_ComboParticleDS");
		TimeBarBorder = ParametersExtended.Instance.GetTexture("param_TimeBarBorderDS");
		TimeBarInlay = ParametersExtended.Instance.GetTexture("param_TimeBarInlayDS");
		TimeBarInformation = ParametersExtended.Instance.GetTexture("param_TimeBarInformationDS");
		ScoreBoardBorder = ParametersExtended.Instance.GetTexture("param_ScoreBoardBorderDS");
		ScoreBoardInlay = ParametersExtended.Instance.GetTexture("param_ScoreBoardInlayDS");
		TargetBorder = ParametersExtended.Instance.GetTexture("param_TargetBorderDS");
		TargetInlay = ParametersExtended.Instance.GetTexture("param_TargetInlayDS");
		EndScreenBorder = ParametersExtended.Instance.GetTexture("param_EndScreenBorderDS");
		EndScreenInlay = ParametersExtended.Instance.GetTexture("param_EndScreenInlayDS");
		#endregion
		#endregion

		#region Vectors
		ReloadSize = ParametersExtended.Instance.GetVector("param_ReloadSizeXDS", "param_ReloadSizeYDS");
		PlayArea = ParametersExtended.Instance.GetVector("param_PlayAreaRightDS", "param_PlayAreaTopDS", "param_PlayAreaLeftDS", "param_PlayAreaBottomDS");
		InteractionDimensions = ParametersExtended.Instance.GetVectorInt("param_InteractionWidth", "param_InteractionHeight");
		#endregion

		#region Other
		DynamicVariable = Globals.DynamicVariables.Player1Area;
		DynamicVariable = ParameterLoader.Instance.getParamValue("param_DynamicVariable", ref DynamicVariable);
		#endregion
		LogWriter.Log("ParameterManager Finished Loading");

		EffectController.Instance.ActivateLoadScreenManager();
		EffectController.Instance.ActivateGameManager();
		EffectController.Instance.ParameterManagerLoadStatus = true;
	}
}

// ParameterManager.Instance.DynamicVariable