using System.Collections.Generic;
using UnityEngine;

public class ForeGroundManager : MonoBehaviour
{
	public GameObject
		foreGroundVerticalPlayerOne,
		foreGroundVerticalPlayerTwo,
		foreGroundHorizontalPlayerOne,
		foreGroundHorizontalPlayerTwo;
	private void OnEnable()
	{
		SetForeground();
		EffectController.Instance.ForeGroundLoadStatus = true;
	}
	void SetForeground()
	{
		if (ParameterManager.Instance.useForeground)
		{
			if (ParameterManager.Instance.VerticalPlay)
			{
				ForegroundVertical();
			}
			else
			{
				ForegroundHorizontal();
			}
		}
		else
		{
			NoForeGrounds();
		}
	}

	void ForegroundHorizontal()
	{
		OmiTexture _foregroundTexP1, _foregroundTexP2;
		Color _foregroundColorP1, _foregroundColorP2;

		foreGroundVerticalPlayerOne.SetActive(false);
		foreGroundVerticalPlayerTwo.SetActive(false);
		foreGroundHorizontalPlayerOne.SetActive(true);
		foreGroundHorizontalPlayerTwo.SetActive(true);
		if (ParameterManager.Instance.ForegroundHorizontal.Count > 1)
		{
			_foregroundTexP1 = ParameterManager.Instance.ForegroundHorizontal[0];
			_foregroundTexP2 = ParameterManager.Instance.ForegroundHorizontal[1];
		}
		else
		{
			_foregroundTexP1 = ParameterManager.Instance.ForegroundHorizontal[0];
			_foregroundTexP2 = ParameterManager.Instance.ForegroundHorizontal[0];
		}

		if (ParameterManager.Instance.ForegroundColors.Count > 1)
		{
			_foregroundColorP1 = ParameterManager.Instance.ForegroundColors[0];
			_foregroundColorP2 = ParameterManager.Instance.ForegroundColors[1];
		}
		else
		{
			_foregroundColorP1 = ParameterManager.Instance.ForegroundColors[0];
			_foregroundColorP2 = ParameterManager.Instance.ForegroundColors[0];
		}
		SpriteRenderer sr_foregroundP1 = foreGroundHorizontalPlayerOne.GetComponent<SpriteRenderer>();
		SpriteRenderer sr_foregroundP2 = foreGroundHorizontalPlayerTwo.GetComponent<SpriteRenderer>();
		sr_foregroundP1.sprite = _foregroundTexP1.CreateSprite();
		sr_foregroundP2.sprite = _foregroundTexP2.CreateSprite();
		sr_foregroundP1.color = _foregroundColorP1;
		sr_foregroundP2.color = _foregroundColorP2;
		foreGroundHorizontalPlayerOne.transform.position = new Vector3(-UIController.Instance.cameraWorldSpaceDimensions.x / 2, 0f,-0.5f);
		foreGroundHorizontalPlayerTwo.transform.position = new Vector3(UIController.Instance.cameraWorldSpaceDimensions.x / 2, 0f, -0.5f);
		foreGroundHorizontalPlayerOne.transform.localScale = new Vector2((UIController.Instance.cameraWorldSpaceDimensions.y * 2) / sr_foregroundP1.size.x, UIController.Instance.cameraWorldSpaceDimensions.x / sr_foregroundP1.size.y);
		foreGroundHorizontalPlayerTwo.transform.localScale = new Vector2((UIController.Instance.cameraWorldSpaceDimensions.y * 2) / sr_foregroundP2.size.x, UIController.Instance.cameraWorldSpaceDimensions.x / sr_foregroundP2.size.y);

	}
	void ForegroundVertical()
	{
		OmiTexture _foregroundTexP1, _foregroundTexP2;
		Color _foregroundColorP1, _foregroundColorP2;

		foreGroundVerticalPlayerOne.SetActive(true);
		foreGroundVerticalPlayerTwo.SetActive(true);
		foreGroundHorizontalPlayerOne.SetActive(false);
		foreGroundHorizontalPlayerTwo.SetActive(false);
		if (ParameterManager.Instance.ForegroundVertical.Count > 1)
		{
			_foregroundTexP1 = ParameterManager.Instance.ForegroundVertical[0];
			_foregroundTexP2 = ParameterManager.Instance.ForegroundVertical[1];
		}
		else
		{
			_foregroundTexP1 = ParameterManager.Instance.ForegroundVertical[0];
			_foregroundTexP2 = ParameterManager.Instance.ForegroundVertical[0];
		}
		if (ParameterManager.Instance.ForegroundColors.Count > 1)
		{
			_foregroundColorP1 = ParameterManager.Instance.ForegroundColors[0];
			_foregroundColorP2 = ParameterManager.Instance.ForegroundColors[1];
		}
		else
		{
			_foregroundColorP1 = ParameterManager.Instance.ForegroundColors[0];
			_foregroundColorP2 = ParameterManager.Instance.ForegroundColors[0];
		}
		SpriteRenderer sr_foregroundP1 = foreGroundVerticalPlayerOne.GetComponent<SpriteRenderer>();
		SpriteRenderer sr_foregroundP2 = foreGroundVerticalPlayerTwo.GetComponent<SpriteRenderer>();
		sr_foregroundP1.sprite = _foregroundTexP1.CreateSprite();
		sr_foregroundP2.sprite = _foregroundTexP2.CreateSprite();
		sr_foregroundP1.color = _foregroundColorP1;
		sr_foregroundP2.color = _foregroundColorP2;
		foreGroundVerticalPlayerOne.transform.position = new Vector3(-UIController.Instance.cameraWorldSpaceDimensions.x / 2, 0f, -0.5f);
		foreGroundVerticalPlayerTwo.transform.position = new Vector3(UIController.Instance.cameraWorldSpaceDimensions.x / 2, 0f, -0.5f);
		foreGroundVerticalPlayerOne.transform.localScale = new Vector2(UIController.Instance.cameraWorldSpaceDimensions.x / sr_foregroundP1.bounds.size.x, (UIController.Instance.cameraWorldSpaceDimensions.y * 2) / sr_foregroundP1.bounds.size.y);
		foreGroundVerticalPlayerTwo.transform.localScale = new Vector2(UIController.Instance.cameraWorldSpaceDimensions.x / sr_foregroundP2.bounds.size.x, (UIController.Instance.cameraWorldSpaceDimensions.y * 2) / sr_foregroundP2.bounds.size.y);
	}

	void NoForeGrounds()
	{
		foreGroundVerticalPlayerOne.SetActive(false);
		foreGroundVerticalPlayerTwo.SetActive(false);
		foreGroundHorizontalPlayerOne.SetActive(false);
		foreGroundHorizontalPlayerTwo.SetActive(false);
	}


}
