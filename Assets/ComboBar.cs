using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ComboBar : MonoBehaviour
{
	public UIController.Player player;
	public Image Background, Inlay;
	public Image combo1Image, combo2Image, combo3Image, combo4Image, combo5Image, combo6Image;
	public RectTransform comboBarRectTrans, background, inlay;
	public GameObject[] combos;
	public GameObject particles;
	public GameObject PFparticles;
	private ParticlesScript PScript;
	public int ComboLevel
	{
		get { return comboLevel; }
		set 
		{ 
			comboLevel = Mathf.Clamp(value, 0, 6);
			//Debug.Log(player+" comboBar Value ="+comboLevel);
			
			if (value>0&&ParameterManager.Instance.Particles)
			{
				PScript.Play();
				comboLerp = (float)comboLevel / (float)6;
				PScript.EmissionRate(comboLerp);
			}
			else
			{
				PScript.Stop();
			}
			for (int i = 0; i < combos.Length; i++)
			{
				if (i+1>comboLevel)
				{
					combos[i].SetActive(false);
				}
				else
				{
					combos[i].SetActive(true);
				}
			}
		}
	}
	float comboLerp = 0;
	private int comboLevel = 0;
	private void Awake()
	{
		comboBarRectTrans = gameObject.GetComponent<RectTransform>();
		particles = Instantiate(PFparticles);
		PScript = particles.GetComponent<ParticlesScript>();
	}

	private void Update()
	{

	}
	public void SetBackground(Vector2 _backgroundDimensions,bool _vertical)
	{
		background.sizeDelta = _backgroundDimensions;
		inlay.sizeDelta = new Vector2(background.sizeDelta.x - 10, background.sizeDelta.y - 5);
		comboBarRectTrans.sizeDelta = background.sizeDelta;
		if (_vertical)
		{
			comboBarRectTrans.anchoredPosition = new Vector2(Screen.width * 0.25732421875f * (player == UIController.Player.Player1 ? -1 : 1), Screen.height/2);

			comboBarRectTrans.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
			PScript.SetPosition(new Vector2((UIController.Instance.cameraWorldSpaceDimensions.x/2) * (player == UIController.Player.Player1 ? -1 : 1), UIController.Instance.cameraWorldSpaceDimensions.y),new Vector3(0,0,0),_vertical);
		}
		else
		{
			comboBarRectTrans.anchoredPosition = new Vector2(15 * (player == UIController.Player.Player1 ? -1 : 1), 0);

			comboBarRectTrans.rotation = Quaternion.Euler(new Vector3(0, 0, player == UIController.Player.Player1 ? -90 : 90));
			PScript.SetPosition(new Vector2(0.15f * (player == UIController.Player.Player1 ? -1 : 1), 0), new Vector3(0,0,player == UIController.Player.Player1 ? -90 : 90), _vertical);

		}
		SetComboBlocks();

	}

	public void SetComboBlocks()
	{
		for (int i = 0; i < combos.Length; i++)
		{
			RectTransform combo = combos[i].GetComponent<RectTransform>();
			combo.sizeDelta = new Vector2(inlay.sizeDelta.x * 0.1639344262295082f, inlay.sizeDelta.y);
			combo.anchoredPosition = new Vector2((combo.sizeDelta.x * i) + (i * ((float)Screen.width / 1024f)), 0);

		}
	}
	public void SetStyle()
	{
		Background.sprite = ParameterManager.Instance.ComboBarBorder.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.ComboBarBorder.Dimensions.x, ParameterManager.Instance.ComboBarBorder.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			128f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		Inlay.sprite = ParameterManager.Instance.ComboBarInlay.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.ComboBarInlay.Dimensions.x, ParameterManager.Instance.ComboBarInlay.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			128f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		PScript.SetParticleImage(ParameterManager.Instance.ComboParticle.CreateSprite());
		Background.type = Image.Type.Tiled;
		Inlay.type = Image.Type.Tiled;


	}
}
