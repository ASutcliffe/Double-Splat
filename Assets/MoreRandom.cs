using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoreRandom : MonoBehaviour
{
    #region Random Vector2
    /// <summary>
    /// Generates random vector 2 between range.
    /// </summary>
    /// <param name="min">Minimum value</param>
    /// <param name="max">Maximum value</param>
    /// <returns>Random vector 2 position within range.</returns>
    public static Vector2 RangeVector2(Vector2 min, Vector2 max)
    {
        Vector2 result = new Vector2(Random.Range(min.x, max.x), Random.Range(min.y, max.y));
        return result;
    }
    #endregion
    #region Random Vector2
    /// <summary>
    /// Generates random vector 2 interger between range.
    /// </summary>
    /// <param name="min">Minimum value</param>
    /// <param name="max">Maximum value</param>
    /// <returns>Random vector 2 interger position within range.</returns>
    public static Vector2Int RangeVector2Int(Vector2Int min, Vector2Int max)
    {
        Vector2Int result = new Vector2Int(Random.Range(min.x, max.x), Random.Range(min.y, max.y));
        return result;
    }
    #endregion
}
