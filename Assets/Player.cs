#define DEBUG
///When building product should always #undef DEBUG
#undef DEBUG

using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
	public static Player Player1 = null;
	public static Player Player2 = null;
	#region Basic Variables
	#region Public
	public bool isInteractable
	{
		get { return _isInteractable; }
		set
		{
			_isInteractable = value;
			Color _tmpCol = player == UIController.Player.Player1 ? UIController.Instance.ReloadP1.GetComponent<ReloadButton>().Icon.color : UIController.Instance.ReloadP2.GetComponent<ReloadButton>().Icon.color;
			if (_isInteractable)
			{
				switch (player)
				{
					case UIController.Player.Player1:
						UIController.Instance.ReloadP1.GetComponent<ReloadButton>().Icon.color = new Color(_tmpCol.r, _tmpCol.g, _tmpCol.b, 1f);
						break;
					case UIController.Player.Player2:
						UIController.Instance.ReloadP2.GetComponent<ReloadButton>().Icon.color = new Color(_tmpCol.r, _tmpCol.g, _tmpCol.b, 1f);
						break;
					default:
						break;
				}
			}
			else
			{
				switch (player)
				{
					case UIController.Player.Player1:
						UIController.Instance.ReloadP1.GetComponent<ReloadButton>().Icon.color = new Color(_tmpCol.r, _tmpCol.g, _tmpCol.b, 0.2f);
						break;
					case UIController.Player.Player2:
						UIController.Instance.ReloadP2.GetComponent<ReloadButton>().Icon.color = new Color(_tmpCol.r, _tmpCol.g, _tmpCol.b, 0.2f);
						break;
					default:
						break;
				}
			}
		}
	}
	#endregion
	#region Private
	private bool _isInteractable = true;
	private string
		playlistAddress;
	#endregion
	#endregion
	#region Complex Variables
	#region Public
	public UIController.Player
		player;
	public Vector2Int
		target;
	#endregion
	public float CollisionTimer = 0;
	public float ReloadTimer = 0;
	public float CollisionDelay = 0.5f;
	public float ReloadDelay = 0.5f;
	#region Private
	private AudioClip
		audioClip;
	private AudioManager
		playerAudioManager;
	private AudioSource
		audioSource;
	private SpriteRenderer
		spriteRenderer;
	#endregion
	#endregion
	#region Awake
	private void Awake()
	{
#if DEBUG
        LogWriter.Log(player + " is Awake");
#endif
		switch (player)
		{
			case UIController.Player.Player1:
				Player1 = this;
				break;
			case UIController.Player.Player2:
				Player2 = this;
				break;
			default:
				break;
		}
	}
	#endregion
	#region Start
	private void Start()
	{

	}
	private void OnEnable()
	{
#if DEBUG
        LogWriter.Log(player + " Started");
#endif
		spriteRenderer = GetComponentInChildren<SpriteRenderer>();
		InitReticle();
		InitPlayerAudio();

	}

	private void Update()
	{
		CollisionTimer += Time.deltaTime;
		ReloadTimer += Time.deltaTime;
	}

	void InitReticle()
	{
		if (ParameterManager.Instance.ReticleVisible)
		{
			spriteRenderer.color = player == UIController.Player.Player1 ? GameController.Instance.colorP1 : GameController.Instance.colorP2;
			spriteRenderer.sprite = ParameterManager.Instance.Reticle.CreateSprite();
		}
		spriteRenderer.enabled = ParameterManager.Instance.ReticleVisible;
	}

	void InitPlayerAudio()
	{
		playlistAddress = ParameterManager.Instance.Correct + ";" + ParameterManager.Instance.Wrong + ";" + ParameterManager.Instance.Reload + ";" + ParameterManager.Instance.Refresh;
		playerAudioManager = gameObject.GetComponent<AudioManager>();
		playerAudioManager.Setup(playlistAddress, ref audioClip);
		audioSource = playerAudioManager.GetAudioSource();
	}
	#endregion
	#region On Collision Enter 2D
	/* 
     * checks to see if the collided object is tagged as a target object or the refesh button.
     * If it is a target it checks if it THE target the player is trying to hit.
    */
	bool BadCollision = false;
	IEnumerator StartGracePeriod()
	{
		BadCollision = true;
		yield return new WaitForSecondsRealtime(ParameterManager.Instance.ComboGracePeriod);
		if (BadCollision)
		{
			GameController.Instance.combo(player, true);
			BadCollision = false;
		}
	}


	private void OnCollisionEnter2D(Collision2D collision)
	{
#if DEBUG
        Debug.Log(player + " Collided With " + collision.gameObject.name);
#endif
		switch (collision.gameObject.tag)
		{
			case "Target":
				if (isInteractable && CollisionTimer > CollisionDelay)
				{

					switch (player)
					{
						case UIController.Player.Player1:
							if (DynamicVariableManager.Instance.player1Difficulty != DynamicVariableManager.Difficulty.Easy)
							{
								if (collision.gameObject.GetComponent<Target>().stats.ID == target)
								{
									GameController.Instance.TargetHit(player);
									if (ParameterManager.Instance.ComboMode)
									{
										GameController.Instance.combo(player, false);
										BadCollision = false;
									}
									playerAudioManager.PlayClipFromPlaylist(0, 1f, false, false);
									if ((GameController.Instance.modifiers & 32) == 32)
									{
										isInteractable = false;
									}
									CollisionTimer = 0;
									ReloadTimer = 0;

								}
								else
								{
									if (ParameterManager.Instance.ComboMode)
									{
										StartCoroutine(StartGracePeriod());
										//GameController.Instance.combo(player, true);
									}
									if (!audioSource.isPlaying)
									{
										playerAudioManager.PlayClipFromPlaylist(1, 1f, false, false);
									}
								}
							}
							else
							{
								GameController.Instance.TargetHit(player);
								if (ParameterManager.Instance.ComboMode)
								{
									GameController.Instance.combo(player, false);
									BadCollision = false;
								}
								playerAudioManager.PlayClipFromPlaylist(0, 1f, false, false);
								if ((GameController.Instance.modifiers & 32) == 32)
								{
									isInteractable = false;
								}
								CollisionTimer = 0;
								ReloadTimer = 0;
							}
							break;
						case UIController.Player.Player2:
							if (DynamicVariableManager.Instance.player2Difficulty != DynamicVariableManager.Difficulty.Easy)
							{
								if (collision.gameObject.GetComponent<Target>().stats.ID == target)
								{
									GameController.Instance.TargetHit(player);
									if (ParameterManager.Instance.ComboMode)
									{
										GameController.Instance.combo(player, false);
										BadCollision = false;
									}
									playerAudioManager.PlayClipFromPlaylist(0, 1f, false, false);
									if ((GameController.Instance.modifiers & 32) == 32)
									{
										isInteractable = false;
									}
									CollisionTimer = 0;
									ReloadTimer = 0;
								}
								else
								{
									if (ParameterManager.Instance.ComboMode)
									{
										StartCoroutine(StartGracePeriod());
										//GameController.Instance.combo(player, true);
									}
									if (!audioSource.isPlaying)
									{
										playerAudioManager.PlayClipFromPlaylist(1, 1f, false, false);
									}
								}
							}
							else
							{
								GameController.Instance.TargetHit(player);
								if (ParameterManager.Instance.ComboMode)
								{
									GameController.Instance.combo(player, false);
									BadCollision = false;
								}
								playerAudioManager.PlayClipFromPlaylist(0, 1f, false, false);
								if ((GameController.Instance.modifiers & 32) == 32)
								{
									isInteractable = false;
								}
								CollisionTimer = 0;
								ReloadTimer = 0;
							}
							break;
					}

				}
				break;
			case "Refresh":
				switch (player)
				{
					case UIController.Player.Player1:
						if (!UIController.Instance.Player1Ready)
						{
							playerAudioManager.PlayClipFromPlaylist(3, 1f, false, false);
						}
						UIController.Instance.Player1Ready = true;

						break;
					case UIController.Player.Player2:
						if (!UIController.Instance.Player2Ready)
						{
							playerAudioManager.PlayClipFromPlaylist(3, 1f, false, false);
						}
						UIController.Instance.Player2Ready = true;

						break;
					default:
						break;
				}
				break;
			case "Reload":
				if (ReloadTimer > ReloadDelay)
				{
					CollisionTimer = 0;
					if (!isInteractable)
					{
						playerAudioManager.PlayClipFromPlaylist(2, 1f, false, false);
					}
					isInteractable = true;
				}
				break;
			default:
				break;
		}
	}
	#endregion
}
