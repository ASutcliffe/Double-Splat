using UnityEngine;

public class ReloadButton : MonoBehaviour
{

	public GameObject Min, Max;
	public SpriteRenderer Background, Inlay, Icon;
	public BoxCollider2D boxCollider;
	public Rect Area;
	public void SetDimensions(Vector2 _Dimensions)
	{
		boxCollider.size = _Dimensions;
		Background.size = _Dimensions;
		Icon.size = _Dimensions;
		Inlay.size = _Dimensions;
		//Background.gameObject.transform.localScale = _Dimensions;
		//Inlay.gameObject.transform.localScale = new Vector2(_Dimensions.x - 0.15f,_Dimensions.y - 0.15f);
		//Icon.gameObject.transform.localScale = new Vector2(_Dimensions.x - 0.15f, _Dimensions.y - 0.15f);
		Area = new Rect(boxCollider.bounds.min, boxCollider.size);
	}

	public void setArea()
	{
		Area = new Rect(boxCollider.bounds.min, boxCollider.size);
		Min.transform.position = Area.min - new Vector2(1f, 1f);
		Max.transform.position = Area.max + new Vector2(1f, 1f);
	}

	public void SetStyle()
	{
		Background.sprite = ParameterManager.Instance.ReloadBorder.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.ReloadBorder.Dimensions.x, ParameterManager.Instance.ReloadBorder.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			768f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		Inlay.sprite = ParameterManager.Instance.ReloadInlay.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.ReloadInlay.Dimensions.x, ParameterManager.Instance.ReloadInlay.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			768f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		Icon.sprite = ParameterManager.Instance.ReloadIcon.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.ReloadIcon.Dimensions.x, ParameterManager.Instance.ReloadIcon.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			768f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		Background.drawMode = SpriteDrawMode.Tiled;
		Inlay.drawMode = SpriteDrawMode.Tiled;
		Icon.drawMode = SpriteDrawMode.Tiled;

	}
}
