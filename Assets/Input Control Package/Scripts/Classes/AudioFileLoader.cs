﻿using NAudio;
using NAudio.Wave;
using System.IO;
using UnityEngine;

// Removed this to stop Warning about creating Monobehaviour using the 'new' keyword
//public class AudioFileLoader : MonoBehaviour {
public class AudioFileLoader
{
    public AudioClip Load(string pathToFile)
    {
        if (File.Exists(pathToFile))
        {
            string ext = pathToFile.Substring(pathToFile.Length - 3).ToLower();
            string pathToConverted = null;

            Directory.CreateDirectory(System.IO.Path.GetTempPath() + @"\temp");

            if (ext.Contains("mp3")) //Convert MP3 To WAV and save to file
            {
                pathToConverted = System.IO.Path.GetTempPath() + @"\temp\currentsong.wav";
                Mp3ToWav(pathToFile, pathToConverted);
                ext = "wav";
            }
            else if (ext.Contains("wav")) //Dont bother converting wav files
            {
                pathToConverted = pathToFile;
            }
            else if (ext.Contains("ogv"))
            {
                // Audio should be handled by the video player
                // Code obsoleted due to Unity 2019 update
                //return getVideoAudio(pathToFile);
            }
            else //If other format try writing as a wav
            {
                pathToConverted = System.IO.Path.GetTempPath() + @"\temp\currentsong." + ext;
                File.WriteAllBytes(pathToConverted, File.ReadAllBytes(pathToFile));
            }

            //Load to AudioClip using unity WWW 
            WWW www = new WWW("file:///" + pathToConverted);
            AudioClip audio = www.GetAudioClip();
            audio.name = Path.GetFileName(pathToFile);
            while (audio.loadState != AudioDataLoadState.Loaded) { } //Wait for Sounds to fully load
            return audio;
        }
#if (DEBUG == true)
        LogWriter.Log("AUDIOFILELOADER: Audio File Does not Exist @ Location " + pathToFile);
#endif
        return null;
    }

    public void Mp3ToWav(string mp3File, string outputFile)
    {
        using (Mp3FileReader reader = new Mp3FileReader(mp3File))
        {
            WaveFileWriter.CreateWaveFile(outputFile, reader);
        }
    }

    private static AudioClip getVideoAudio(string path)
    {
        AudioClip ac = null;
        string urlToUse = path.Replace('\\', '/');
        WWW www = new WWW(@"file:///" + urlToUse);
        if (www.error != null)
            LogWriter.Log("AUDIOFILELOADER: Can't load movie! - " + www.error);

        //return www.GetMovieTexture().audioClip;
        return ac;
    }
}
