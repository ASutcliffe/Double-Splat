﻿
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class Extensions {


    public static Texture GetTextureFromFile(this Texture _defaultTexture, string _path)
    {
        if (_path != null)
        {
            if (File.Exists(_path))
            {
				Texture2D texture2D = new Texture2D(2,2);
				byte[] _rawData = File.ReadAllBytes(_path);
				texture2D.name = Path.GetFileNameWithoutExtension(_path);
				texture2D.LoadImage(_rawData);
				_defaultTexture = texture2D;
				_defaultTexture.name = texture2D.name;
			}
			else
			{
				return null;
			}
        }
        return _defaultTexture;
    }

    public static IEnumerable<T> GetRemoveSafeEnumerator<T>(this List<T> list)
    {
        for (int i = list.Count - 1; i >= 0; i--)
        {
            // Reset the value of i if it is invalid.
            // This occurs when more than one item
            // is removed from the list during the enumeration.
            if (i >= list.Count)
            {
                if (list.Count == 0)
                    yield break;

                i = list.Count - 1;
            }

            yield return list[i];
        }
    }

    public static IEnumerable GetRemoveSafeEnumerator(this ArrayList list)
    {
        for (int i = list.Count - 1; i >= 0; i--)
        {
            // Reset the value of i if it is invalid.
            // This occurs when more than one item
            // is removed from the list during the enumeration.
            if (i >= list.Count)
            {
                if (list.Count == 0)
                    yield break;

                i = list.Count - 1;
            }

            yield return list[i];
        }
    }
}
