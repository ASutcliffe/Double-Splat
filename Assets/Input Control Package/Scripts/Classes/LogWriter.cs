﻿using UnityEngine;
using System.IO;
using System;

public class LogWriter{
    private static string filePath;
    static StreamWriter writer;
    public LogWriter(bool DeleteOnStartup = false)
    {
        filePath = Application.persistentDataPath + "/LogFile.txt";

        Debug.Log(filePath);

        try
        {
            if (File.Exists(filePath))
            {
                if (DeleteOnStartup)
                {
                    File.Delete(filePath);
                }

            }
            else
            {
                File.Create(filePath);
            }
            Log("SYSTEM STARTED");
        }

        catch (System.Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    ~LogWriter()
    {
        if(writer != null)
        {
            writer.Close();
        }
    }

    public static void Log(string Message)
    {
        try
        {
            //Debug.Log(Message);
            Debug.Log(System.DateTime.Now.ToString() + " : " + Message);

            // Can also access Debug log for hardcopy. Use Editor menu or Control-V to get folder.
            //writer = new StreamWriter(filePath, true);
            //writer.Write(System.DateTime.Now.ToString() + " : " + Message + Environment.NewLine);
            //writer.Close();
        }
        catch(System.Exception e)
        {
            Debug.Log(e.ToString());
        }

    }
}
