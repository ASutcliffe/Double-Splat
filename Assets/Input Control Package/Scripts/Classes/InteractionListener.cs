﻿using UnityEngine;

public interface InteractionListener
{
    void OnInteractionReceived(int[] interaction, int iWidth, int iHeight);
}
