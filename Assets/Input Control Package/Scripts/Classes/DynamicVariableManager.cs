﻿
// During debugging this must be defined
// To debug language, send debug Language parameter with DEBUG_UNITY=true for host(e.g. Dutch) and app and language (e.g. nl-NL) folders included
// DEBUG_LANGUAGE=true enables language testing when DEBUG_UNITY=true
#define DEBUG_LANGUAGE
// During debugging this must be commented out
// For product release uncomment this
#undef DEBUG_LANGUAGE

using omiLibrary;
using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class: GUIDynamicVariable
/// This class supports the management of textual display
/// for dynamic variables on the screen.
/// Only one instance or object of this class exists.
/// The sealed class prevents inheritance.
/// All access must be made using:
/// GUIDynamicVariable s = GUIDynamicVariable.Instance; 
/// 
/// </summary>
public sealed class DynamicVariableManager : MonoBehaviour, IKeyInputObserver
{
	#region properties



	// Create the single object
	// Put it in the global _instance using a static store but
	// make it accessible only through a getter with private.
	// Ensure that it can only be allocated once with the readonly.
	// Text area adjustments are automatically performed by Unity
	// using scripts configured in the Inspector
	private static DynamicVariableManager _instance;

	// Define a getter (read-only) for the global reference to the object.
	// Users are not allowed to write to the global. 
	// Cannot use readonly keyword due to the need to update variable dynamically.
	public static DynamicVariableManager Instance
	{
		get
		{
			return _instance;
		}
	}// getter

	public DynamicVariable DVariable = new DynamicVariable();
	float nextRepeat = 0.0f;
	float keyRepeat = 0.05f;
	float singlePressDelay = 0.5f;


	// Prefab instances to be dropped by Inspector
	public Canvas sheet;
	public UnityEngine.UI.Button buttonGUI;

	public enum Difficulty { First, Default, Easy, Normal, Hard, Last };
	public Difficulty player1Difficulty = ParameterManager.Instance.Difficulty;
	public Difficulty player2Difficulty = ParameterManager.Instance.Difficulty;
	public Language omiLanguage = Language.DEFAULT;
	public const Globals.DynamicVariables defaultDynamicVariableOption = Globals.DynamicVariables.GameMode;
	public Globals.DynamicVariables dynamicVariableOption = defaultDynamicVariableOption;

	// Getters and Setters for Dynamic Variables
	private string _dynamicVariableValue = "Default";
	public string dynamicVariableValue
	{
		get { return _dynamicVariableValue; }
		set { _dynamicVariableValue = value; }
	}
	private string _dynamicVariableName = "NoName";
	public string dynamicVariableName
	{
		get { return _dynamicVariableName; }
		set { _dynamicVariableName = value; }
	}


	// Locals
	private Canvas newCanvas;
	private UnityEngine.UI.Button newButton;

	// Locals for managing display duration
	public float showMessageTimer = 0;
	const float ShowMessageEndTime = 5f;
	#endregion properties

	#region handlers

	void Awake()
	{
		_instance = this;
	}

	/// <summary>
	/// Method:
	/// This is used for initialization
	/// </summary>
	public void Start()
	{
		// Configure language for dyn var display and listeners of key inputs
		setupDynamicVariables();

		for (int currentDynaVar = 1; currentDynaVar < (int)Globals.DynamicVariables.Last; currentDynaVar++)
		{
#if DEBUG
			Debug.Log("Registering Dynamic Variable " + (Globals.DynamicVariables)currentDynaVar);
#endif
			DVariable.registerGroupListener(this, currentDynaVar);
		}

		// Setup GUI as a button for the title
		newCanvas = Instantiate(sheet);
		newButton = Instantiate(buttonGUI);

		//For debugging, we want to see the button all the time
		//to make adjustments using the Inspector
#if DEBUG_DYNAMIC_VARIABLE
		// Make button a child of the canvas
		newButton.transform.SetParent(newCanvas.transform, false);
#endif

		KeyboardManager.OnKeyDown += KeyboardManager_OnKeyDown;
	}

	private void KeyboardManager_OnKeyDown(KeyCode key)
	{

		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			DVariable.notifyGroupListeners(KeyInputEvent.UpArrow);
			nextRepeat = Time.time + singlePressDelay;
		}
		else if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			DVariable.notifyGroupListeners(KeyInputEvent.DownArrow);
			nextRepeat = Time.time + singlePressDelay;
		}
		else if (Input.GetKey(KeyCode.UpArrow) && Time.time > nextRepeat)
		{
			DVariable.notifyGroupListeners(KeyInputEvent.UpArrow);
			nextRepeat = Time.time + keyRepeat;
		}
		else if (Input.GetKey(KeyCode.DownArrow) && Time.time > nextRepeat)
		{
			DVariable.notifyGroupListeners(KeyInputEvent.DownArrow);
			nextRepeat = Time.time + keyRepeat;
		}
		else if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			// Shift to the next dynamic variable 
			DVariable.shiftListenerGroup(KeyInputEvent.RightArrow, (int)Globals.DynamicVariables.Last);

			// Use conditional compilation instead of active code:
			//Shift past dynamic variable if its disabled
			//if (Globals.DisabledDynamicVariables.Contains((Globals.DynamicVariables)DVariable.activeGroup))
			//    DVariable.shiftListenerGroup(KeyInputEvent.RightArrow, (int)Globals.DynamicVariables.Last);

			// Convert the active group (dynamic variable) into an enum name
			Globals.DynamicVariables enumOption = (Globals.DynamicVariables)DVariable.activeGroup;
			// Translate the name into foreign language if required
			//Instance.dynamicVariableName = DVariable.translateString(enumOption.ToString());
			dynamicVariableName = Globals.translateDynamicVariable(enumOption.ToString());
			// Display current group dynamic variable
			DVariable.notifyGroupListeners(KeyInputEvent.RightArrow);

		}
		else if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			// Shift to the next dynamic variable 
			DVariable.shiftListenerGroup(KeyInputEvent.LeftArrow, (int)Globals.DynamicVariables.Last);

			//Shift past dynamic variable if its disabled
			//if (Globals.DisabledDynamicVariables.Contains((Globals.DynamicVariables)DVariable.activeGroup))
			//    DVariable.shiftListenerGroup(KeyInputEvent.LeftArrow, (int)Globals.DynamicVariables.Last);

			// Convert the active group (dynamic variable) into an enum name
			Globals.DynamicVariables enumOption = (Globals.DynamicVariables)DVariable.activeGroup;
			// Translate the name into foreign language if required
			//Instance.dynamicVariableName = DVariable.translateString(enumOption.ToString());
			dynamicVariableName = Globals.translateDynamicVariable(enumOption.ToString());
			// Display current group dynamic variable
			DVariable.notifyGroupListeners(KeyInputEvent.LeftArrow);
		}

	}

	// Update is called once per frame
	void Update()
	{
		DisplayDynamicVariable();
	}// method

	float
		easySpeed = 0.5f,
		normalSpeed = 3f,
		hardSpeed = 7f,
		easyScale = 0.6f,
		normalScale = 0.5f,
		hardScale = 0.4f;
	int
		easyCount = 3,
		normalCount = 5,
		hardCount = 7;

	public void SetDifficulty(Difficulty _difficulty,UIController.Player _player)
	{
		switch (_player)
		{
			case UIController.Player.Player1:
				switch (_difficulty)
				{
					case Difficulty.First:
						break;
					case Difficulty.Default:
						GameController.Instance.scaleP1 = ParameterManager.Instance.ObjectScale;
						GameController.Instance.movementSpeedP1 = ParameterManager.Instance.ObjectSpeed;
						GameController.Instance.itemPerRoundP1 = ParameterManager.Instance.ItemsPerRound;
						Player.Player1.CollisionDelay = 0.75f;
						Player.Player1.ReloadDelay = 0.5f;
						UIController.Instance.showTargetP1 = true;
						break;

					case Difficulty.Easy:
						GameController.Instance.scaleP1 = easyScale;
						GameController.Instance.movementSpeedP1 = easySpeed;
						GameController.Instance.itemPerRoundP1 = easyCount;
						Player.Player1.CollisionDelay = 0.75f;
						Player.Player1.ReloadDelay = 1f;
						UIController.Instance.showTargetP1 = false;

						break;
					case Difficulty.Normal:
						GameController.Instance.scaleP1 = normalScale;
						GameController.Instance.movementSpeedP1 = normalSpeed;
						GameController.Instance.itemPerRoundP1 = normalCount;
						Player.Player1.CollisionDelay = 0.75f;
						Player.Player1.ReloadDelay = 0.5f;
						UIController.Instance.showTargetP1 = true;

						break;
					case Difficulty.Hard:
						GameController.Instance.scaleP1 = hardScale;
						GameController.Instance.movementSpeedP1 = hardSpeed;
						GameController.Instance.itemPerRoundP1 = hardCount;
						Player.Player1.CollisionDelay = 0.75f;
						Player.Player1.ReloadDelay = 0.5f;
						UIController.Instance.showTargetP1 = true;

						break;
					case Difficulty.Last:
						break;
					default:
						break;
				}
				break;
			case UIController.Player.Player2:
				switch (_difficulty)
				{
					case Difficulty.First:
						break;
					case Difficulty.Default:
						GameController.Instance.scaleP2 = ParameterManager.Instance.ObjectScale;
						GameController.Instance.movementSpeedP2 = ParameterManager.Instance.ObjectSpeed;
						GameController.Instance.itemPerRoundP2 = ParameterManager.Instance.ItemsPerRound;
						Player.Player2.CollisionDelay = 0.75f;
						Player.Player2.ReloadDelay = 0.5f;
						UIController.Instance.showTargetP2 = true;

						break;
					case Difficulty.Easy:
						GameController.Instance.scaleP2 = easyScale;
						GameController.Instance.movementSpeedP2 = easySpeed;
						GameController.Instance.itemPerRoundP2 = easyCount;
						Player.Player2.CollisionDelay = 0.75f;
						Player.Player2.ReloadDelay = 1f;
						UIController.Instance.showTargetP2 = false;

						break;
					case Difficulty.Normal:
						GameController.Instance.scaleP2 = normalScale;
						GameController.Instance.movementSpeedP2 = normalSpeed;
						GameController.Instance.itemPerRoundP2 = normalCount;
						Player.Player2.CollisionDelay = 0.75f;
						Player.Player2.ReloadDelay = 0.5f;
						UIController.Instance.showTargetP2 = true;

						break;
					case Difficulty.Hard:
						GameController.Instance.scaleP2 = hardScale;
						GameController.Instance.movementSpeedP2 = hardSpeed;
						GameController.Instance.itemPerRoundP2 = hardCount;
						Player.Player2.CollisionDelay = 0.75f;
						Player.Player2.ReloadDelay = 0.5f;
						UIController.Instance.showTargetP2 = true;

						break;
					case Difficulty.Last:
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}

	/// <summary>
	/// Method:
	/// </summary>
	public void UpArrowKeyAction() // Implements IKeyInputObserver
	{
		switch ((Globals.DynamicVariables)DVariable.activeGroup)
		{
			#region Player1Area
			case Globals.DynamicVariables.Player1Area:
				++GameController.Instance.areaModeP1;
				if (ParameterManager.Instance.PlayAreaCustomMode)
				{
					if (GameController.Instance.areaModeP1 >= GameController.PlayArea.Count)
					{
						GameController.Instance.areaModeP1 = 0;
					}
				}
				else
				{
					if (GameController.Instance.areaModeP1 >= GameController.PlayArea.Custom)
					{
						GameController.Instance.areaModeP1 = 0;
					}
				}

				GameController.Instance.ShowAreaP1 = true;
				GameController.Instance.ShowHighlightP1 = true;
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(GameController.Instance.areaModeP1);

				break;
			#endregion
			#region Player2Area
			case Globals.DynamicVariables.Player2Area:
				++GameController.Instance.areaModeP2;
				if (ParameterManager.Instance.PlayAreaCustomMode)
				{
					if (GameController.Instance.areaModeP2 >= GameController.PlayArea.Count)
					{
						GameController.Instance.areaModeP2 = 0;
					}
				}
				else
				{
					if (GameController.Instance.areaModeP2 >= GameController.PlayArea.Custom)
					{
						GameController.Instance.areaModeP2 = 0;
					}
				}

				GameController.Instance.ShowAreaP2 = true;
				GameController.Instance.ShowHighlightP2 = true;
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(GameController.Instance.areaModeP2);

				break;
			#endregion
			#region Player1Speed
			//case Globals.DynamicVariables.Player1Speed:
			//	GameController.Instance.movementSpeedP1 = Mathf.Clamp((float)Math.Round((double)(GameController.Instance.movementSpeedP1 += 0.5f), 2), 0f, 7f);
			//	GameController.Instance.ShowHighlightP1 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.movementSpeedP1);

			//	break;
			#endregion
			#region Player2Speed
			//case Globals.DynamicVariables.Player2Speed:
			//	GameController.Instance.movementSpeedP2 = Mathf.Clamp((float)Math.Round((double)(GameController.Instance.movementSpeedP2 += 0.5f), 2), 0f, 7f);
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.movementSpeedP2);

			//	break;
			#endregion
			#region Player1Count
			//case Globals.DynamicVariables.Player1Count:

			//	GameController.Instance.itemPerRoundP1 = Mathf.Clamp(GameController.Instance.itemPerRoundP1 += 1, 1, 15);
			//	GameController.Instance.ShowHighlightP1 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.itemPerRoundP1);

			//	break;
			#endregion
			#region Player2Count
			//case Globals.DynamicVariables.Player2Count:
			//	GameController.Instance.itemPerRoundP2 = Mathf.Clamp(GameController.Instance.itemPerRoundP2 += 1, 1, 15);
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.itemPerRoundP2);

			//	break;
			#endregion
			#region Player1Scale
			//case Globals.DynamicVariables.Player1Scale:
			//	GameController.Instance.scaleP1 = Mathf.Clamp((float)Math.Round((double)(GameController.Instance.scaleP1 += 0.2f), 1), 0.4f, 2f);
			//	GameController.Instance.ShowHighlightP1 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.scaleP1);

			//	break;
			#endregion
			#region Player2Scale
			//case Globals.DynamicVariables.Player2Scale:
			//	GameController.Instance.scaleP2 = Mathf.Clamp((float)Math.Round((double)(GameController.Instance.scaleP2 += 0.2f), 1), 0.4f, 2f);
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.scaleP2);

			//	break;
			#endregion
			#region Particles
			case Globals.DynamicVariables.ParticlesDS:
				ParameterManager.Instance.Particles = !ParameterManager.Instance.Particles;
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(ParameterManager.Instance.Particles ? "On" : "Off");
				break;
			#endregion
			#region Player 1 Difficulty
			//case Globals.DynamicVariables.Player1Difficulty:
			//	player1Difficulty++;
			//	if (player1Difficulty == Difficulty.Last)
			//	{
			//		player1Difficulty = Difficulty.Default;

			//	}
			//	SetDifficulty(player1Difficulty, UIController.Player.Player1);
			//	//switch (player1Difficulty)
			//	//{
			//	//	case Difficulty.First:
			//	//		break;
			//	//	case Difficulty.Default:
			//	//		GameController.Instance.scaleP1 = ParameterManager.Instance.ObjectScale;
			//	//		GameController.Instance.movementSpeedP1 = ParameterManager.Instance.ObjectSpeed;
			//	//		GameController.Instance.itemPerRoundP1 = ParameterManager.Instance.ItemsPerRound;
			//	//		UIController.Instance.showTargetP1 = true;
			//	//		break;

			//	//	case Difficulty.Easy:
			//	//		GameController.Instance.scaleP1 = easyScale;
			//	//		GameController.Instance.movementSpeedP1 = easySpeed;
			//	//		GameController.Instance.itemPerRoundP1 = easyCount;
			//	//		UIController.Instance.showTargetP1 = false;

			//	//		break;
			//	//	case Difficulty.Normal:
			//	//		GameController.Instance.scaleP1 = normalScale;
			//	//		GameController.Instance.movementSpeedP1 = normalSpeed;
			//	//		GameController.Instance.itemPerRoundP1 = normalCount;
			//	//		UIController.Instance.showTargetP1 = true;

			//	//		break;
			//	//	case Difficulty.Hard:
			//	//		GameController.Instance.scaleP1 = hardScale;
			//	//		GameController.Instance.movementSpeedP1 = hardSpeed;
			//	//		GameController.Instance.itemPerRoundP1 = hardCount;
			//	//		UIController.Instance.showTargetP1 = true;

			//	//		break;
			//	//	case Difficulty.Last:
			//	//		break;
			//	//	default:
			//	//		break;
			//	//}

			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(player1Difficulty);
			//	break;
			#endregion
			#region Player 2 Difficulty
			//case Globals.DynamicVariables.Player2Difficulty:
			//	player2Difficulty++;
			//	if (player2Difficulty == Difficulty.Last)
			//	{
			//		player2Difficulty = Difficulty.Default;
			//	}
			//	SetDifficulty(player2Difficulty, UIController.Player.Player2);

			//	//switch (player2Difficulty)
			//	//{
			//	//	case Difficulty.First:
			//	//		break;
			//	//	case Difficulty.Default:
			//	//		GameController.Instance.scaleP2 = ParameterManager.Instance.ObjectScale;
			//	//		GameController.Instance.movementSpeedP2 = ParameterManager.Instance.ObjectSpeed;
			//	//		GameController.Instance.itemPerRoundP2 = ParameterManager.Instance.ItemsPerRound;
			//	//		UIController.Instance.showTargetP2 = true;

			//	//		break;
			//	//	case Difficulty.Easy:
			//	//		GameController.Instance.scaleP2 = easyScale;
			//	//		GameController.Instance.movementSpeedP2 = easySpeed;
			//	//		GameController.Instance.itemPerRoundP2 = easyCount;
			//	//		UIController.Instance.showTargetP2 = false;

			//	//		break;
			//	//	case Difficulty.Normal:
			//	//		GameController.Instance.scaleP2 = normalScale;
			//	//		GameController.Instance.movementSpeedP2 = normalSpeed;
			//	//		GameController.Instance.itemPerRoundP2 = normalCount;
			//	//		UIController.Instance.showTargetP2 = true;

			//	//		break;
			//	//	case Difficulty.Hard:
			//	//		GameController.Instance.scaleP2 = hardScale;
			//	//		GameController.Instance.movementSpeedP2 = hardSpeed;
			//	//		GameController.Instance.itemPerRoundP2 = hardCount;
			//	//		UIController.Instance.showTargetP2 = true;

			//	//		break;
			//	//	case Difficulty.Last:
			//	//		break;
			//	//	default:
			//	//		break;
			//	//}
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(player2Difficulty);
			//	break;
			#endregion
			#region PlayLimits
			case Globals.DynamicVariables.GameMode:

				if (UIController.Instance.playLimits == UIController.GameMode.Score)
				{
					UIController.Instance.playLimits = UIController.GameMode.Unlimited;
				}
				else
				{
					UIController.Instance.playLimits++;
				}

				startDynamicVariableDisplayTimer();
				storeDynamicVariable(UIController.Instance.playLimits);
				break;
			#endregion
			#region Somethings gone horribly wrong
			default:
				LogWriter.Log(String.Format("DVariable.activeGroup is out of bounds with a value of {0}", DVariable.activeGroup));

				break;
				#endregion
		}
	}

	public void DownArrowKeyAction() // Implements IKeyInputObserver
	{
		switch ((Globals.DynamicVariables)DVariable.activeGroup)
		{
			#region Player1Area
			case Globals.DynamicVariables.Player1Area:
				if (GameController.Instance.areaModeP1 == 0)
				{
					if (ParameterManager.Instance.PlayAreaCustomMode)
					{
						GameController.Instance.areaModeP1 = GameController.PlayArea.Count;
					}
					else
					{
						GameController.Instance.areaModeP1 = GameController.PlayArea.Custom;
					}
				}
				--GameController.Instance.areaModeP1;
				GameController.Instance.ShowAreaP1 = true;
				GameController.Instance.ShowHighlightP1 = true;
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(GameController.Instance.areaModeP1);

				break;
			#endregion

			#region Player2Area
			case Globals.DynamicVariables.Player2Area:
				if (GameController.Instance.areaModeP2 == 0)
				{
					if (ParameterManager.Instance.PlayAreaCustomMode)
					{
						GameController.Instance.areaModeP2 = GameController.PlayArea.Count;
					}
					else
					{
						GameController.Instance.areaModeP2 = GameController.PlayArea.Custom;
					}
				}
				--GameController.Instance.areaModeP2;
				GameController.Instance.ShowAreaP2 = true;
				GameController.Instance.ShowHighlightP2 = true;

				startDynamicVariableDisplayTimer();
				storeDynamicVariable(GameController.Instance.areaModeP2);

				break;
			#endregion

			#region Player1Speed
			//case Globals.DynamicVariables.Player1Speed:
			//	GameController.Instance.movementSpeedP1 = Mathf.Clamp((float)Math.Round((double)(GameController.Instance.movementSpeedP1 -= 0.5f), 2), 0f, 7f);
			//	GameController.Instance.ShowHighlightP1 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.movementSpeedP1);
			//	break;
			#endregion

			#region Player2Speed
			//case Globals.DynamicVariables.Player2Speed:
			//	GameController.Instance.movementSpeedP2 = Mathf.Clamp((float)Math.Round((double)(GameController.Instance.movementSpeedP2 -= 0.5f), 2), 0f, 7f);
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.movementSpeedP2);

			//	break;
			#endregion

			#region Player1Count
			//case Globals.DynamicVariables.Player1Count:
			//	GameController.Instance.itemPerRoundP1 = Mathf.Clamp(GameController.Instance.itemPerRoundP1 -= 1, 1, 15);
			//	GameController.Instance.ShowHighlightP1 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.itemPerRoundP1);

			//	break;
			#endregion

			#region Player2Count
			//case Globals.DynamicVariables.Player2Count:
			//	GameController.Instance.itemPerRoundP2 = Mathf.Clamp(GameController.Instance.itemPerRoundP2 -= 1, 1, 15);
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.itemPerRoundP2);

			//	break;
			#endregion

			#region Player1Scale
			//case Globals.DynamicVariables.Player1Scale:
			//	GameController.Instance.scaleP1 = Mathf.Clamp((float)Math.Round((double)(GameController.Instance.scaleP1 -= 0.2f), 2), 0.4f, 2f);
			//	GameController.Instance.ShowHighlightP1 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.scaleP1);

			//	break;
			#endregion

			#region Player2Scale
			//case Globals.DynamicVariables.Player2Scale:
			//	GameController.Instance.scaleP2 = Mathf.Clamp((float)Math.Round((double)(GameController.Instance.scaleP2 -= 0.2f), 2), 0.4f, 2f);
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.scaleP2);


			//	break;
			#endregion

			#region Particles
			case Globals.DynamicVariables.ParticlesDS:
				ParameterManager.Instance.Particles = !ParameterManager.Instance.Particles;
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(ParameterManager.Instance.Particles ? "On" : "Off");
				break;
			#endregion

			#region Player 1 Difficulty
			//case Globals.DynamicVariables.Player1Difficulty:
			//	player1Difficulty--;
			//	if (player1Difficulty == Difficulty.First)
			//	{
			//		player1Difficulty = Difficulty.Hard;
			//	}
			//	SetDifficulty(player1Difficulty, UIController.Player.Player1);

			//	//switch (player1Difficulty)
			//	//{
			//	//	case Difficulty.First:
			//	//		break;

			//	//	case Difficulty.Default:
			//	//		GameController.Instance.scaleP1 = ParameterManager.Instance.ObjectScale;
			//	//		GameController.Instance.movementSpeedP1 = ParameterManager.Instance.ObjectSpeed;
			//	//		GameController.Instance.itemPerRoundP1 = ParameterManager.Instance.ItemsPerRound;
			//	//		UIController.Instance.showTargetP1 = true;
			//	//		break;
			//	//	case Difficulty.Easy:
			//	//		GameController.Instance.scaleP1 = easyScale;
			//	//		GameController.Instance.movementSpeedP1 = easySpeed;
			//	//		GameController.Instance.itemPerRoundP1 = easyCount;
			//	//		UIController.Instance.showTargetP1 = false;
			//	//		break;
			//	//	case Difficulty.Normal:
			//	//		GameController.Instance.scaleP1 = normalScale;
			//	//		GameController.Instance.movementSpeedP1 = normalSpeed;
			//	//		GameController.Instance.itemPerRoundP1 = normalCount;
			//	//		UIController.Instance.showTargetP1 = true;
			//	//		break;
			//	//	case Difficulty.Hard:
			//	//		GameController.Instance.scaleP1 = hardScale;
			//	//		GameController.Instance.movementSpeedP1 = hardSpeed;
			//	//		GameController.Instance.itemPerRoundP1 = hardCount;
			//	//		UIController.Instance.showTargetP1 = true;
			//	//		break;
			//	//	case Difficulty.Last:
			//	//		break;
			//	//	default:
			//	//		break;
			//	//}

			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(player1Difficulty);
			//	break;
			#endregion

			#region Player 2 Difficulty
			//case Globals.DynamicVariables.Player2Difficulty:
			//	player2Difficulty--;
			//	if (player2Difficulty == Difficulty.First)
			//	{
			//		player2Difficulty = Difficulty.Hard;
			//	}
			//	SetDifficulty(player2Difficulty, UIController.Player.Player2);

			//	//switch (player2Difficulty)
			//	//{
			//	//	case Difficulty.First:
			//	//		break;
			//	//	case Difficulty.Default:
			//	//		GameController.Instance.scaleP2 = ParameterManager.Instance.ObjectScale;
			//	//		GameController.Instance.movementSpeedP2 = ParameterManager.Instance.ObjectSpeed;
			//	//		GameController.Instance.itemPerRoundP2 = ParameterManager.Instance.ItemsPerRound;
			//	//		UIController.Instance.showTargetP2 = true;

			//	//		break;
			//	//	case Difficulty.Easy:
			//	//		GameController.Instance.scaleP2 = easyScale;
			//	//		GameController.Instance.movementSpeedP2 = easySpeed;
			//	//		GameController.Instance.itemPerRoundP2 = easyCount;
			//	//		UIController.Instance.showTargetP2 = false;

			//	//		break;
			//	//	case Difficulty.Normal:
			//	//		GameController.Instance.scaleP2 = normalScale;
			//	//		GameController.Instance.movementSpeedP2 = normalSpeed;
			//	//		GameController.Instance.itemPerRoundP2 = normalCount;
			//	//		UIController.Instance.showTargetP2 = true;

			//	//		break;
			//	//	case Difficulty.Hard:
			//	//		GameController.Instance.scaleP2 = hardScale;
			//	//		GameController.Instance.movementSpeedP2 = hardSpeed;
			//	//		GameController.Instance.itemPerRoundP2 = hardCount;
			//	//		UIController.Instance.showTargetP2 = true;

			//	//		break;
			//	//	case Difficulty.Last:
			//	//		break;
			//	//	default:
			//	//		break;
			//	//}
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(player2Difficulty);
			//	break;
			#endregion
			#region PlayLimits
			case Globals.DynamicVariables.GameMode:

				if (UIController.Instance.playLimits == UIController.GameMode.Unlimited)
				{
					UIController.Instance.playLimits = UIController.GameMode.Score;
				}
				else
				{
					UIController.Instance.playLimits--;
				}

				startDynamicVariableDisplayTimer();
				storeDynamicVariable(UIController.Instance.playLimits);
				break;
			#endregion
			#region Somethings gone horribly wrong
			default:
				LogWriter.Log(String.Format("DVariable.activeGroup is out of bounds with a value of {0}", DVariable.activeGroup));

				break;
				#endregion
		}
	}//method
	public void LeftArrowKeyAction() // Implements IKeyInputObserver
	{
		GameController.Instance.HideSettingUI();
		switch ((Globals.DynamicVariables)DVariable.activeGroup)
		{
			#region Player1Area
			case Globals.DynamicVariables.Player1Area:
				GameController.Instance.areaModeP1 = GameController.Instance.areaModeP1;
				GameController.Instance.ShowAreaP1 = true;
				GameController.Instance.ShowHighlightP1 = true;
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(GameController.Instance.areaModeP1);

				break;
			#endregion

			#region Player2Area
			case Globals.DynamicVariables.Player2Area:
				GameController.Instance.areaModeP2 = GameController.Instance.areaModeP2;
				GameController.Instance.ShowAreaP2 = true;
				GameController.Instance.ShowHighlightP2 = true;
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(GameController.Instance.areaModeP2);

				break;
			#endregion

			#region Player1Speed
			//case Globals.DynamicVariables.Player1Speed:
			//	GameController.Instance.ShowHighlightP1 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.movementSpeedP1);
			//	break;
			#endregion

			#region Player2Speed
			//case Globals.DynamicVariables.Player2Speed:
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.movementSpeedP2);

			//	break;
			#endregion

			#region Player1Count
			//case Globals.DynamicVariables.Player1Count:
			//	GameController.Instance.ShowHighlightP1 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.itemPerRoundP1);

			//	break;
			#endregion

			#region Player2Count
			//case Globals.DynamicVariables.Player2Count:
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.itemPerRoundP2);

			//	break;
			#endregion

			#region Player1Scale
			//case Globals.DynamicVariables.Player1Scale:
			//	startDynamicVariableDisplayTimer();
			//	GameController.Instance.ShowHighlightP1 = true;
			//	storeDynamicVariable(GameController.Instance.scaleP1);

			//	break;
			#endregion

			#region Player2Scale
			//case Globals.DynamicVariables.Player2Scale:
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.scaleP2);
			//	break;
			#endregion

			#region Particles
			case Globals.DynamicVariables.ParticlesDS:
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(ParameterManager.Instance.Particles ? "On" : "Off");
				break;
			#endregion

			#region Player 1 Difficulty
			//case Globals.DynamicVariables.Player1Difficulty:
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(player1Difficulty);
			//	break;
			#endregion

			#region Player 2 Difficulty
			//case Globals.DynamicVariables.Player2Difficulty:
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(player2Difficulty);
			//	break;
			#endregion
			#region PlayLimits
			case Globals.DynamicVariables.GameMode:
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(UIController.Instance.playLimits);
				break;
			#endregion

			#region Somethings gone horribly wrong
			default:
				LogWriter.Log(String.Format("DVariable.activeGroup is out of bounds with a value of {0}", DVariable.activeGroup));

				break;
				#endregion
		}


	}

	public void RightArrowKeyAction() // Implements IKeyInputObserver
	{
		GameController.Instance.HideSettingUI();
		switch ((Globals.DynamicVariables)DVariable.activeGroup)
		{
			#region Player1Area
			case Globals.DynamicVariables.Player1Area:
				GameController.Instance.areaModeP1 = GameController.Instance.areaModeP1;
				GameController.Instance.ShowAreaP1 = true;
				GameController.Instance.ShowHighlightP1 = true;
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(GameController.Instance.areaModeP1);

				break;
			#endregion

			#region Player2Area
			case Globals.DynamicVariables.Player2Area:
				GameController.Instance.areaModeP2 = GameController.Instance.areaModeP2;
				GameController.Instance.ShowAreaP2 = true;
				GameController.Instance.ShowHighlightP2 = true;
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(GameController.Instance.areaModeP2);

				break;
			#endregion

			#region Player1Speed
			//case Globals.DynamicVariables.Player1Speed:
			//	GameController.Instance.ShowHighlightP1 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.movementSpeedP1);
			//	break;
			#endregion

			#region Player2Speed
			//case Globals.DynamicVariables.Player2Speed:
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.movementSpeedP2);

			//	break;
			#endregion

			#region Player1Count
			//case Globals.DynamicVariables.Player1Count:
			//	GameController.Instance.ShowHighlightP1 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.itemPerRoundP1);

			//	break;
			#endregion

			#region Player2Count
			//case Globals.DynamicVariables.Player2Count:
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.itemPerRoundP2);

			//	break;
			#endregion

			#region Player1Scale
			//case Globals.DynamicVariables.Player1Scale:
			//	GameController.Instance.ShowHighlightP1 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.scaleP1);

			//	break;
			#endregion

			#region Player2Scale
			//case Globals.DynamicVariables.Player2Scale:
			//	GameController.Instance.ShowHighlightP2 = true;
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(GameController.Instance.scaleP2);

			//	break;
			#endregion

			#region Particles
			case Globals.DynamicVariables.ParticlesDS:
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(ParameterManager.Instance.Particles ? "On" : "Off");
				break;
			#endregion

			#region Player 1 Difficulty
			//case Globals.DynamicVariables.Player1Difficulty:
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(player1Difficulty);
			//	break;
			#endregion

			#region Player 2 Difficulty
			//case Globals.DynamicVariables.Player2Difficulty:
			//	startDynamicVariableDisplayTimer();
			//	storeDynamicVariable(player2Difficulty);
			//	break;
			#endregion

			#region PlayLimits
			case Globals.DynamicVariables.GameMode:
				startDynamicVariableDisplayTimer();
				storeDynamicVariable(UIController.Instance.playLimits);
				break;
			#endregion

			#region Somethings gone horribly wrong
			default:
				LogWriter.Log(String.Format("DVariable.activeGroup is out of bounds with a value of {0}", DVariable.activeGroup));

				break;
				#endregion
		}

	}// method
#endregion handlers

	#region other methods

	private void upArrowGameSpeed()
	{
		// GameSpeed up key Action
		++GameManager.Instance.gameSpeed;
		GameManager.Instance.clampGameSpeed();

		// Kick-off dynamic variable display
		startDynamicVariableDisplayTimer();
		storeDynamicVariable(GameManager.Instance.gameSpeed);
	}
	private void downArrowGameSpeed()
	{
		// GameSpeed up key Action
		--GameManager.Instance.gameSpeed;
		GameManager.Instance.clampGameSpeed();

		// Kick-off dynamic variable display
		startDynamicVariableDisplayTimer();
		storeDynamicVariable(GameManager.Instance.gameSpeed);
	}
	private void adjustSpeed(int gameS)
	{


	}
	private void upSpeed()
	{
		int gameS = ++GameManager.Instance.gameSpeed;
		GameManager.Instance.clampGameSpeed();
		// Kick-off dynamic variable display
		startDynamicVariableDisplayTimer();
		storeDynamicVariable(GameManager.Instance.gameSpeed);
	}

	private void downSpeed()
	{
		int gameS = --GameManager.Instance.gameSpeed;
		GameManager.Instance.clampGameSpeed();

		// Kick-off dynamic variable display
		startDynamicVariableDisplayTimer();
		storeDynamicVariable(GameManager.Instance.gameSpeed);

	}

	/// <summary>
	/// Method:
	/// Sets the language used for dyn var display.
	/// Sets up the dyn var events caused by key inputs.
	/// Sets up the storage for the listeners of each dyn var event.
	/// Note: A dyn var event is a parameter to be changed, e.g. speedOfObject.
	/// The event is an option determined by left-right arrow keys of a key input.
	/// </summary>
	public void setupDynamicVariables()
	{
		///Set Language
		omiLanguage = (Language)Enum.Parse(typeof(Language), ParameterManager.Instance.Language);
		DVariable.omiLanguage = omiLanguage;

		dynamicVariableOption = defaultDynamicVariableOption;

#if (DEBUG_LANGUAGE == false)
		if (DVariable.validLanguageDictionary())
		{
			Debug.Log("Valid language resource file in Build");
		}
		else
		{
			if (DVariable.omiLanguage != Language.DEFAULT)
			{
				DVariable.omiLanguage = Language.DEFAULT;
				Debug.LogError("********** Fault: Build does not contain correct language resource file. **********");
			}
		}
#endif

		bool isDefault = false;
		// Go through local list of dyn var for Catch
		foreach (Globals.DynamicVariables dvar in Enum.GetValues(typeof(Globals.DynamicVariables)))
		{
			// Check for valid groups. Each dyn var value has a group of interested listeners.
			// Filter out None and Last so that we look at meaningful vars
			if ((dvar != Globals.DynamicVariables.None) && (dvar != Globals.DynamicVariables.Last))
			{
				// Not None, Not Last - valid group
				// Check if group is the initial one.
				// Initial (default) is the dyn var you see when you startup
				if (dynamicVariableOption == dvar)
				{
					// is dynamicVariableOption
					// is initial group
					isDefault = true;
				}
				else
				{

					// is not dynamicVariableOption
					isDefault = false;
				}
				// Register group leader (which is the dyn var option) for listeners
				DVariable.registerListenerGroup((int)dvar, isDefault);
			}
		}//foreach

		// Translate dyn var to foreign language.
		dynamicVariableName = Globals.translateDynamicVariable(dynamicVariableOption.ToString());

	}

	// Dynamic Variable configuration helpers
	public void storeDynamicVariable<T>(T var)
	{
		dynamicVariableValue = Globals.translateDynamicVariable(var.ToString());
	}
	public void startDynamicVariableDisplayTimer()
	{
		showMessageTimer = 1f;
	}

	/// <summary>
	/// Method:
	/// This method must be called priodically.
	/// Displays dynamic variable name and value.
	/// Display exists for duration determined by count.
	/// </summary>
	public void DisplayDynamicVariable()
	{

		// Effect app initialization complete
		// Check if dynamic variable display required
		if (showMessageTimer > 0 && showMessageTimer < (ShowMessageEndTime + 1f))
		{

			newButton.transform.SetParent(newCanvas.transform, false);

			string showVariable = dynamicVariableValue;

			newButton.GetComponentInChildren<Text>().text = dynamicVariableName + " : " + showVariable;

			showMessageTimer += Time.deltaTime;
		}
		else if (showMessageTimer >= (ShowMessageEndTime +1f))
		{
			// Dynamic variable display not required any more
			// Disable display

			// Reset timing counter
			showMessageTimer = 0;

			// Remove display
			newButton.transform.SetParent(null, false);
			GameController.Instance.HideSettingUI();
		}
	}//method
	private bool isNumber(string s)
	{
		bool isNumberString = false;

		double number = 0;
		isNumberString = double.TryParse(s, out number);

		return isNumberString;
	}

	#endregion other methods

}// class
