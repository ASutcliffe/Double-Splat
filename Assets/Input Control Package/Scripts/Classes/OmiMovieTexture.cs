﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Replacement for depracated MovieTexture class
/// An omiMovieTexture object manages the video player belonging to a game object.
/// A separate game object that wishes to render (play) this video player has to provide a reference
/// for the movie to be played on the game object. 
/// The game object that plays the video player is called the playerObject and
/// the game object that renders the movie that is being played is called the renderObjecct.
/// This means that several game objects can render the same movie.
/// Usage:
/// A typical sequence of calls would be:
/// (1) Constructor
/// (1a) Validate movie location (isVideo)
/// (1b) Set movieUrl
/// (2) initialiseVideoPlayer
/// (3) playMovie
/// </summary>
/// public class OmiMovieTexture: Texture
public class OmiMovieTexture
{
	// Properties -----------------------
	#region properties_public
	// File path to current movie file
	public string movieUrl { get; set; }
	// Index (key) to current movie
	public const int nullMovieIndex = -1;
	public int multiMovieIndex { get; set; }
	// Video player for movies
	public UnityEngine.Video.VideoPlayer videoPlayer { get; set; }
	// Place holder for multi video wipes
	public static Texture2D dummyTextureForMultivideo { get; set; }
	#endregion properties_public

	#region properties_private
	Dictionary<int, string> movieUrls;

	#endregion properties_private
	// Methods -----------------------------
	#region methods_static
	/// <summary>
	/// Method:
	/// Determines if the path given is for an Omi supported video.
	/// </summary>
	/// <param name="path">File path of media</param>
	/// <returns>True if video</returns>
	public static bool isVideo(string path)
	{
		bool v = false;
		string extension = System.IO.Path.GetExtension(path).ToLower();
		if (extension.Equals(".ogv")
			|| extension.Equals(".mp4")
			|| extension.Equals(".mov")
			)
		{
			//Video Texture
			v = true;
		}
		return v;
	}//method

	/// <summary>
	/// Method
	/// Determines if the path given is for an Omi supported image.
	/// </summary>
	/// <param name="path">File path of media</param>
	/// <returns>True if video</returns>
	public static bool isImage(string path)
	{
		bool image = false;
		string extension = System.IO.Path.GetExtension(path).ToLower();
		if (extension.Equals(".jpg")
			|| extension.Equals(".png")
			)
		{
			//Image Texture
			image = true;
		}
		return image;
	}//method
	#endregion methods_static

	#region methods_constructor
	/// <summary>
	/// Constructor
	/// </summary>
	/// <param name="movie_Url"></param>
	public OmiMovieTexture(string movie_Url)
	{
		movieUrl = movie_Url;//done

		// Ensure that the uninitialised have null values
		multiMovieIndex = nullMovieIndex;

		videoPlayer = null;
		movieUrls = null;
	}
	/// <summary>
	/// Constructor
	/// </summary>
	public OmiMovieTexture()
	{
		// Ensure that the uninitialised have null values
		movieUrl = null;
		multiMovieIndex = nullMovieIndex;
		videoPlayer = null;
		movieUrls = new Dictionary<int, string>();
	}
	#endregion methods_constructor

	#region methods_object
	#region video
	#region setUpMovie #DONE
	/// <summary>
	/// Method:
	/// This method creates and prepares a video player.
	/// The video player of a gameObject (playerObject) is created and initialized.
	/// Warning: This method assumes that the movie path (movieUrl) is present.
	/// </summary>
	/// <param name="playerObject">gameObject that has this videoPlayer</param>
	/// <param name="loopMovie">True if movie loops to beginning when play ends</param>
	public void setUpMovie(GameObject playerObject, bool loopMovie = true)
	{
		initialiseVideoPlayer(movieUrl, playerObject, loopMovie);
	}
	#endregion
	#region initialiseVideoPlayer #DONE
	/// <summary>
	/// Method:
	/// Initialize a video player to play a movie file.
	/// Video player belongs to a game object (playerObject).
	/// Note: By default the player object is also the render object but this is designed so that
	/// it is possible to separate the two.
	/// </summary>
	/// <param name="url">url address of the movie file</param>
	/// <param name="playerObject">gameObject that uses this videoPlayer</param>
	/// <param name="loopMovie">True if movie loops to beginning when play ends</param>
	/// <returns>video player</returns>
	/// 
	public UnityEngine.Video.VideoPlayer initialiseVideoPlayer(string url, GameObject playerObject, bool loopMovie = true)
	{
		UnityEngine.Video.VideoPlayer vp = null;

		// Create the video player of playerObject
		vp = playerObject.AddComponent<UnityEngine.Video.VideoPlayer>();

		//vp.source = UnityEngine.Video.VideoSource.Url; // Ref: WebH1
		// Initialise the movie location
		vp.url = url;
		// Loop the movie
		vp.isLooping = loopMovie;
		// Assumption: Override the texture in the TARGET gameObject
		vp.renderMode = UnityEngine.Video.VideoRenderMode.MaterialOverride;
		// Play as soon as the component is awake
		//vp.playOnAwake = true;
		//vp.waitForFirstFrame = true;

		// Need to set specific object renderer to play video like so:
		//vp.targetMaterialRenderer = GetComponent<Renderer>();
		//vp.targetMaterialProperty = "_MainTex";

		// Setting the mode to None should indicate no source which resulted in movie audio played
		//vp.audioOutputMode = UnityEngine.Video.VideoAudioOutputMode.None;
		const int startTrack = 0;
		const bool muteOn = true;
		vp.SetDirectAudioMute(startTrack, muteOn);
		// If a separate audio feed require the video audio use code below:
		//var audioSource = gameObject.AddComponent<AudioSource>();
		//videoPlayer.audioOutputMode = UnityEngine.Video.VideoAudioOutputMode.AudioSource;
		//videoPlayer.SetTargetAudioSource(0, audioSource);

		// Store video player here
		videoPlayer = vp;

		// Return the initialised video player
		return vp;
	}//method
	#endregion
	#region playMovie #DONE
	/// <summary>
	/// Method:
	/// This method plays a video on a gameObject.
	/// </summary>
	/// <param name="renderObject">The gameObject that renders the video</param>
	public void playMovie(GameObject renderObject)
	{
		// Reference the gameObject renderer so that it will play the movie
		videoPlayer.targetMaterialRenderer = renderObject.GetComponent<Renderer>();
		// Play the movie
		videoPlayer.Play();
	}//method
	#endregion
	#region isMoviePlaying
	/// <summary>
	/// Method
	/// Calling VideoPlayer.Play may not set isPlaying to true. 
	/// The VideoPlayer must successfully prepare the content before it can start playing.
	/// </summary>
	/// <returns>True if video player is playing</returns>
	public bool isMoviePlaying()
	{
		return videoPlayer.isPlaying;
	}
	#endregion
	#region hasCurrentMovie
	/// <summary>
	/// Method:
	/// Check if there is a movie to play
	/// </summary>
	/// <returns>True if there is a movie to play</returns>
	public bool hasCurrentMovie()
	{
		return (movieUrl != null);
	}
	#endregion
	#region MultiMovie (#) Not Using in new one
	#region hasMultiMovie
	public bool hasMultiMovie()
	{
		// Check if movie list exists
		if (movieUrls == null)
			return false; //------->

		// Check if any movies in movie list
		int count = movieUrls.Count;
		return (count > 0); //--------->
	}
	#endregion
	#region addMultiMovie
	public bool addMultiMovie(int index, string url)
	{
		bool result = false;
		try
		{
			movieUrls.Add(index, url);
			result = true;
		}
		catch (ArgumentNullException)
		{
			Debug.LogError("Movie fail add. Invalid Index (null). index, count = " + index + "," + movieUrls.Count);
		}
		return result;
	}
	#endregion
	#region deleteMultiMovie
	public bool deleteMultiMovie(int index)
	{
		bool result = false;
		try
		{
			movieUrls.Remove(index);
			result = true;
		}
		catch (ArgumentNullException)
		{
			Debug.LogError("Movie fail delete. Index null. index, count = " + index + "," + movieUrls.Count);
		}

		return result;
	}
	#endregion
	#region getMultiMovie
	/// <summary>
	/// Method:
	/// Find movie using index.
	/// 
	/// </summary>
	/// <param name="index"></param>
	/// <returns>True if found null otherwise</returns>
	public string getMultiMovie(int index)
	{
		string result = null;
		try
		{
			result = movieUrls[index];
		}
		catch (KeyNotFoundException)
		{
			Debug.LogError("Movie retrieval fail. Index not found. index, count = " + index + "," + movieUrls.Count);
		}
		catch (ArgumentNullException)
		{
			Debug.LogError("Movie fail delete. Index null. index, count = " + index + "," + movieUrls.Count);
		}

		return result;
	}//method
	#endregion
	#region isMultiMovie
	public bool isMultiMovie(int index)
	{
		bool result = false;
		string url = null;
		// Check if there are any moives
		if (!hasMultiMovie())
			return false; //----->

		try
		{
			// Result is true if url exists in movieUrls
			result = movieUrls.TryGetValue(index, out url);
		}
		catch (ArgumentNullException)
		{
			Debug.LogError("Movie fail delete. Index null. index, count = " + index + "," + movieUrls.Count);
		}

		return result;
	}//method
	#endregion
	#region computePreviousIndex
	public int computePreviousIndex(int index, int mediaCount)
	{
		int previousIndex = index - 1;
		if (previousIndex == -1)
			previousIndex = mediaCount - 1;
		return previousIndex;
	}
	#endregion
	#region isPreviousMultiMovie
	public bool isPreviousMultiMovie(int index, int mediaCount)
	{
		bool result = false;
		result = isMultiMovie(computePreviousIndex(index, mediaCount));
		return result;
	}
	#endregion
	#region getMultiMovies
	/// <summary>
	/// Method:
	/// Get all movies.
	/// </summary>
	/// <returns>Dictionary of movies</returns>
	public Dictionary<int, string> getMultiMovies()
	{
		return movieUrls;
	}
	public void setMultiMovies(Dictionary<int, string> movies)
	{
		movieUrls = movies;
	}
	#endregion
	#region setMutliMovieTo
	/// <summary>
	/// Method:
	/// Use index to make movie current.
	/// If index cannot get movie return false
	/// </summary>
	/// <param name="index"></param>
	/// <returns>True if valid index</returns>
	public bool setMutliMovieTo(int index)
	{
		bool ok = false;
		// Make current url the movie url referenced by index
		movieUrl = getMultiMovie(index);
		// Check if can get movie
		if (movieUrl != null)
		{
			ok = true; /* Not null. Can get movie */
			multiMovieIndex = index;
		}
		else
		{
			multiMovieIndex = nullMovieIndex;
		}
		return ok;
	}
	#endregion
	#region deleteAllMultiMovies
	public void deleteAllMultiMovies() 
	{ 
		movieUrls.Clear(); 
	}
	#endregion
	#endregion

	#region pauseMovie #DONE
	public void pauseMovie()
	{
		videoPlayer.Pause();
	}
	#endregion
	#region stopMovie #DONE
	public void stopMovie()
	{
		videoPlayer.Stop();
	}
	#endregion

	#endregion video
	#region audio
	public void setAudioSource(AudioSource audioSource)
	{
		const int startTrackIndex = 0;
		const bool muteOff = false;
		// Turn mute off
		videoPlayer.SetDirectAudioMute(startTrackIndex, muteOff);
		// Divert the video audio to an Audio component
		videoPlayer.audioOutputMode = UnityEngine.Video.VideoAudioOutputMode.AudioSource;
		// Set the video audio to this Audio component
		videoPlayer.SetTargetAudioSource(startTrackIndex, audioSource);
	}
	#endregion audio
	#endregion methods_object

}//class
