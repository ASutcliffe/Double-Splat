﻿using System;

[Serializable]
public class Parameter{
    public string ParameterName;
    public string value;

    public Parameter(string _parameterName, string _value)
    {
        ParameterName = _parameterName;
        value = _value;
    }
}
