﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class FreezeInteractionManager {
    // Freeze state change event
    public static event InteractionFreezeStateChangeHandler InteractionFreezeStateChange;
    // Freeze state change event handler signature
    public delegate void InteractionFreezeStateChangeHandler(bool State);



    public Texture2D InteractionImage;

    private int Visible = 1;
    private int Invisible = 0;
    private int Toggle = 2;

    public static FreezeInteractionManager Instance;

    private GameObject go;
    private GameObject goImage;

    public FreezeInteractionManager(Texture2D _InteractionImage)
    {
        Instance = this;
        InteractionImage = _InteractionImage;

		go = new GameObject();
        go.name = "InteractionFreezeCanvas";
        go.AddComponent<Canvas>();

        go.transform.position = new Vector3(0, 0, 100);

        Canvas goCanvas = go.GetComponent<Canvas>();
        goCanvas.renderMode = RenderMode.ScreenSpaceOverlay;

        go.AddComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;

        goImage = new GameObject();
        goImage.name = "InteractionImage";
        goImage.AddComponent<Image>();
        goImage.GetComponent<Image>().sprite = Sprite.Create(InteractionImage, new Rect(0, 0, InteractionImage.width, InteractionImage.height), new Vector2(0.5f, 0.5f));

        goImage.transform.SetParent(goCanvas.transform);

        RectTransform panelRectTransform = goImage.GetComponent<RectTransform>();
        panelRectTransform.anchorMin = new Vector2(0, 0);
        panelRectTransform.anchorMax = new Vector2(1, 1);
        panelRectTransform.anchoredPosition = Vector3.zero;
        panelRectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, goCanvas.GetComponent<RectTransform>().rect.width);
        panelRectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, goCanvas.GetComponent<RectTransform>().rect.height);
        //panelRectTransform.pivot = new Vector2(0f, 0f);

        KeyboardManager.OnKeyUp += KeyboardManager_OnKeyUp;

        Visibility(Invisible);
    }
    /// <summary>
    /// Event Handler:
    /// For key up event
    /// </summary>
    /// <param name="key"></param>
    private void KeyboardManager_OnKeyUp(KeyCode key)
    {
        if(key == KeyCode.I)
        {
            Visibility(Toggle);
        }
    }
    /// <summary>
    /// Method:
    /// Changes visibility of game display
    /// Raise event E::InteractionFreezeStateChange -> FreezeInteractionManager_InteractionFreezeStateChange
    ///     -> E::OnGameFreezeStateChange -> InteractionManager:GameManager_OnGameFreezeStateChange 
    ///     changes interactionFrozen flag
    /// </summary>
    /// <param name="State"></param>
    public void Visibility(int State)
    {
        if (State == Visible)
            go.SetActive(true);
        if (State == Invisible)
            go.SetActive(false);
        if (State == Toggle)
            go.SetActive(!go.activeSelf);

        // Raise interaction freeze state change event
        if(InteractionFreezeStateChange != null)
        {
            InteractionFreezeStateChange(go.activeSelf);
        }
    }
}
