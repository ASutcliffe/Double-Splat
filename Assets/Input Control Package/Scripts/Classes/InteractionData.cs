﻿using System;
using System.Collections.Generic;

using UnityEngine;


public class InteractionData<T>
{
    #region properties_public
    public int interactionWidth { get; set; }

    //public int interactionHeight { get; set; }
    public int interactionCount { get; set; }
    public int maxNumberOfInteractions { get; set; }

    public const int validInteractionSignalThreshold = 0;

    public const int interactionBackgroundNoisePopulation = 2; //20

    public const int interactionHighNoiseThresholdInPercent = 50;

    public ICollection<T> interactions = null;

    // If it is nearer the top of the screen ignore interaction.
    // If screen is 320x240 then 240 will blackout all, 0 allow all
    public int yForMaxEdgeOfBlackoutZone = 0;

    // For debugging
    public int yCoordinate { get; set; }

    #endregion properties_public

    #region properties_private
    // 1D, 2D Coordinates of interaction
    private delegate T MakeIPoint(int index);
    private MakeIPoint iPointFunc;

    #endregion properties_private

    #region constructor
    /// <summary>
    /// Constructor:
    /// Initialise screen width and blackout zone.
    /// Usage:
    /// For 320x240 screen width=320.
    /// Assume: (0,0) 0f screen is top left hand corner.
    /// If it is required to black out top 75% of screen
    /// yyForBlackoutZone = 3/4 * 240 = 180
    /// </summary>
    /// <param name="width">width of screen</param>
    /// <param name="yForBlackoutZone"></param>
    public InteractionData(int width, int yForBlackoutZone)
    {
        //interactionHeight = height;
        interactionWidth = width;

        yForMaxEdgeOfBlackoutZone = yForBlackoutZone;

        // Initialize callback to transform to 1D, 2D coordinates based on T
        // getIPointFunction determines the function to load into iPointFunc
        // This is either getIntegerPointFrom or getVector2PointFrom
        iPointFunc = getIPointFunction();
        if (iPointFunc==null)
        {
            Debug.LogAssertion("Illegal type (not int or Vector2) for InteractionData");
            throw new Exception("Illegal type for InteractionData");
        }

    }//method
    #endregion constructor

    #region methods_public
    /// <summary>
    /// Method:
    /// Get a list of unique (no duplicates) interactions.
    /// Filters are available that depend on interaction positions
    /// and will ignore interactions in certain defined positions.
    /// Assume 320x240 screen linearised.
    /// T allows an n-dimensional (typically 1D or 2D) vector list to be returned.
    /// </summary>
    /// <param name="intArray">linearised x,y positions</param>
    /// <returns>set of interactions</returns>
    public HashSet<T> getUniqueInteractions(int[] intArray)
    {
        HashSet<T> iList = new HashSet<T>();
        return (HashSet<T>) getInteractions(intArray, iList);
    }
    /// <summary>
    /// Method:
    /// Get a list (duplicates possible) of interactions.
    /// Assume 320x240 screen linearised.
    /// T allows an n-dimensional (typically 1D or 2D) vector list to be returned.
    /// 
    /// </summary>
    /// <param name="intArray">linearised x,y positions</param>
    /// <returns>list of positions</returns>
    public List<T> getInteractions (int[] intArray)
    {
        List<T> iList = new List<T>();
        return (List<T>)getInteractions(intArray, iList); ;
    }
    /// <summary>
    /// Method:
    /// Get a generic list of interactions.
    /// Interactions are validated and poor interactions are ignored.
    /// T determines the vectors returned.
    /// An iList is provided to store the returned vectors.
    /// Usage:
    /// An input array (inputA) of linearised x,y interactions 
    /// is required to be converted to a list of 2D vectors of interactions (output2DList).
    /// Call getInteractions to provide list:
    /// List[Vector2] output2DList = getInteractions(inputA, output2DList);
    /// 
    /// </summary>
    /// <param name="intArray">linearised x,y positions</param>
    /// <param name="iList">empty list of vectors to be returned</param>
    /// <returns></returns>
    public ICollection<T> getInteractions(int[] intArray, ICollection<T> iList)
    {
        //HashSet<T> iList = new HashSet<T>();

        // Store for noise evaluation
        maxNumberOfInteractions = intArray.Length;

        // Length = 320 x 240 = 76800
        for (int i = 0; i < intArray.Length; i++)
        {
            // Get the x position value from single array
            // Only interactions are stored
            if (filterInteractions(intArray[i]))
            {
                validInteractions(i, iList);
            }
        }

        // Store results
        interactionCount = iList.Count;
        interactions = iList;

        return iList;
    }
    /// <summary>
    /// Method:
    /// Checks if the count of interactions are too small.
    /// Returns true if too small false otherwise.
    /// </summary>
    /// <returns>true if noisy</returns>
    public bool isBackgroundNoise() { return interactionCount < interactionBackgroundNoisePopulation; }
    /// <summary>
    /// Method:
    /// Check if the count of interactions are too large.
    /// Typically if the entire input array is filled, it is too large.
    /// </summary>
    /// <returns>true if noisy false otherwise</returns>
    public bool isVeryNoisy()
    {
        bool noisy = false;
        noisy = (interactionCount >= maxNumberOfInteractions);
        return noisy;
    }
    /// <summary>
    /// Method:
    /// Check if there are no interactions.
    /// 
    /// </summary>
    /// <returns>true if no interactions false otherwise</returns>
    public bool isEmptyInteractions() { return interactionCount == 0; }

    #endregion methods_public

    #region methods_private
    /// <summary>
    /// Replace this method with appropriate filter for specific apps.
    /// In future this, could be inherited and over-ridden.
    /// </summary>
    /// <param name="signalValue">magnitude of interaction</param>
    /// <returns></returns>
    private bool filterInteractions(int signalValue)
    {
        return signalValue > validInteractionSignalThreshold;
    }
    /// <summary>
    /// Method:
    /// Convert the [index] to a coordinate.
    /// The coordinate can be 1D or 2D.
    /// Store coordinate in iList.
    /// Note: Duplicates are not stored.
    /// </summary>
    /// <param name="index"></param>
    /// <param name="iList"></param>
    private void validInteractions(int index, ICollection<T> iList)
    {
        // Interactions are only allowed in certain screen areas 
        if (!isScreenInsensitive(index))
        {
            // Get the coordinates of the interaction
            // This function var iPointFunc is initialised by the constructor
            T x = iPointFunc(index);
            // Note: Duplicate values are not added to a HashSet
            iList.Add(x);
        }
    }
    /// <summary>
    /// Method:
    /// This method filters out a section of the y coordinates,
    /// making a section of the screen inactive.
    /// Assumption: (0,0) is the top left hand corner
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    private bool isScreenInsensitive(int index)
    {
        // Compute the row
        yCoordinate = index / interactionWidth;
        // If it is nearer the top of the screen ignore interaction.
        // If screen is 320x240 then 240 will blackout all, 0 allow all
        //const int yForMaxEdgeOfBlackoutArea = 120;
        if (yCoordinate < yForMaxEdgeOfBlackoutZone)
            { return true; }

        return false;
    }
    private MakeIPoint getIPointFunction()
    {
        MakeIPoint func = null;
        // Represents the concrete T at run-time
        // Must be initialized
        T point = default(T);

        // Compute interaction point for different T types
        if (point.GetType() == typeof(int))
        {
            func = new MakeIPoint(getIntegerPointFrom);
        }
        else
        if (point.GetType() == typeof(Vector2))
        {
            func = new MakeIPoint(getVector2PointFrom);
        }
        return func;
    }
    /// <summary>
    /// Method:
    /// The index is a linearised representation of x,y coordinates.
    /// Return the x coordinate from the index.
    /// </summary>
    /// <param name="index">x,y representation</param>
    /// <returns>x coordinate</returns>
    private T getIntegerPointFrom(int index)
    {
        // Compute the column (x coordinate) from the index
        object intPoint = index % interactionWidth;

        return (T)intPoint;
    }
    private T getVector2PointFrom(int index)
    {
        object Vector2Point = new Vector2
            (index % interactionWidth,  // remainder value formed by the wrap of the width (column number)
            index / interactionWidth);  // quotient value which is the number of widths (row number)
        return (T)Vector2Point;
    }
    #endregion methods_private

}// generic class







