﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using NAudio;
using NAudio.Wave;
using System;
using System.Linq;

public class AudioManager : MonoBehaviour
{
    AudioSource source;
    AudioClip clip;
    AudioFileLoader audioFileLoader = new AudioFileLoader();
    public List<AudioClip> playlist = new List<AudioClip>();
    public int playlistIndex = -1;
    public bool isPlaylist = false;
    public bool shouldPlay { get; set; }

    public void Setup(string _path, ref AudioClip _defaultClip, bool isNullable = true)
    {
        source = gameObject.AddComponent<AudioSource>();
       

        if (_path != null)
        {
            //Path not null - Split out any audio
            string[] soundStringsPaths = _path.Split(';');

            foreach (string path in soundStringsPaths)
            {
                //If file exists add it to the playlist
                if (File.Exists(path))
                {
                    playlist.Add(audioFileLoader.Load(path));
                }
            }

            if (soundStringsPaths.Length > 1)
            {
                isPlaylist = true;
                shouldPlay = false;
            }
            else
            {
                if (playlist.Count != 0)
                {
                    clip = playlist.First();
                }
            }
        }
        else
        {
            //Path is null - Add default Clip to playlist 
            // If clip is null checks are made not to play
            playlist.Add(_defaultClip);
        }

        source.playOnAwake = false;
        source.Stop();
    }

    public void PlayClipFromPlaylist(int _indexPosition, float _volume = 1f, bool _looping = false, bool _variation = false)
    {
        {
            clip = playlist[_indexPosition];
            playlistIndex = _indexPosition;
            source.clip = clip;
            source.loop = _looping;
            source.pitch = _variation ? UnityEngine.Random.Range(0.75f,1.25f) : 1f;
            source.volume = _volume;
            source.Play();
        }
    }
    
    public AudioSource GetAudioSource()
    {
        return source;
    }
    public void SetupClip(/*AudioClip _clip*/)
    {
        //source = gameObject.AddComponent<AudioSource>();

        //clip = _clip;

        //source.playOnAwake = false;
        //source.Stop();

        source = gameObject.AddComponent<AudioSource>();
        //clip = _clip;

        // Divert audio from movie player to Audio component
        //BackgroundManager.Instance.movieTexture.setAudioSource(source);
    }

    public void Play(float volume, bool random = false, bool looping = false)
    {
        PlaySound(volume, random, looping);
    }
    public void PlaySound(float volume, bool random = false, bool looping = false)
    {
        source.volume = volume;
        PlaySound(random, looping);
    }
    /// <summary>
    /// Method:
    /// If clip is null checks are made to not play
    /// </summary>
    /// <param name="random"></param>
    /// <param name="looping"></param>
    public void Play(bool random = false, bool looping = false)
    {
        PlaySound(random, looping);
    }

    /// <summary>
    /// Method:
    /// clip must not be null otherwise this will do nothing
    /// </summary>
    /// <param name="random"></param>
    /// <param name="looping"></param>
    public void PlaySound(bool random = false, bool looping = false)
    {
        //Random Playback Not Implemented!
        if (source != null && clip != null)
        {
            shouldPlay = true;

            source.loop = looping;
            source.clip = clip;
            source.Play();
        }
    }

    public void Stop()
    {
        if (source != null)
        {
            source.Stop();
        }
    }

    bool fadingUp = false;
    bool fadingDown = false;
    float fadeTimer;
    float fadeTime;
    float fadeInv;
    float fadeTargetVolume;

    public void FadeUp(float targetVolume = 1.0f, float time = 1.0f)
    {
        if (source != null)
        {
            fadeTargetVolume = targetVolume;
            fadeTimer = source.volume;
            fadeTime = time;
            fadingDown = false;
            fadingUp = true;
        }
    }

    public void FadeDown(float targetVolume = 0.0f, float time = 1.0f)
    {
        if (source != null)
        {
            fadeTargetVolume = targetVolume;
            fadeTimer = source.volume;
            fadeTime = time;
            fadingUp = false;
            fadingDown = true;
        }
    }

    private void ProcessPlaylist()
    {
        if (!source.isPlaying && shouldPlay)
        {
            playlistIndex++;

            if (playlistIndex > playlist.Count - 1)
                playlistIndex = 0;

            if (playlist.Count > 0)
            {
                clip = playlist[playlistIndex];
                source.clip = clip;
                source.Play();
            }
        }
    }


    void Update()
    {
        if (source != null)
        {
            if (isPlaylist)
            {
                ProcessPlaylist();
            }

            if (source.isPlaying)
            {
                if (fadingUp)
                {
                    fadeTimer += Time.fixedDeltaTime / fadeTime;

                    if (fadeTimer > fadeTargetVolume)
                    {
                        fadeTimer = fadeTargetVolume;
                        fadingUp = false;
                    }
                    if (source != null)
                    {
                        source.volume = fadeTimer;
                    }
                }

                if (fadingDown)
                {
                    fadeTimer -= Time.fixedDeltaTime / fadeTime;

                    if (fadeTimer < fadeTargetVolume)
                    {
                        fadeTimer = fadeTargetVolume;
                        fadingDown = false;
                    }
                    if (source != null)
                    {
                        source.volume = fadeTimer;
                    }
                }

            }
        }
    }
}