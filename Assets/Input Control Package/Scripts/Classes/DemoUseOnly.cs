﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DemoUseOnly : MonoBehaviour {
    Texture2D demoTexture;
    GameObject go;
    GameObject goImage;

    private void OnEnable()
    {
        demoTexture = new Texture2D(2, 2);
        demoTexture = Resources.Load<Texture2D>("Images/DemoUse");
        this.gameObject.AddComponent<CanvasGroup>();
        this.gameObject.GetComponent<CanvasGroup>().alpha = 0;


        go = new GameObject();
        go.name = "DemoUseOnly";
        go.AddComponent<Canvas>();
        go.transform.SetParent(this.transform);
        go.transform.position = new Vector3(0, 0, 100);

        Canvas goCanvas = go.GetComponent<Canvas>();
        goCanvas.transform.SetParent(go.transform.parent);
        goCanvas.renderMode = RenderMode.ScreenSpaceOverlay;

        go.AddComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;

        goImage = new GameObject();
        goImage.name = "InteractionImage";
        goImage.AddComponent<Image>();
        goImage.GetComponent<Image>().sprite = Sprite.Create(demoTexture, new Rect(0, 0, demoTexture.width, demoTexture.height), new Vector2(0.5f, 0.5f));

        goImage.transform.SetParent(goCanvas.transform);

        RectTransform panelRectTransform = goImage.GetComponent<RectTransform>();
        panelRectTransform.anchorMin = new Vector2(0, 0);
        panelRectTransform.anchorMax = new Vector2(1, 1);
        panelRectTransform.anchoredPosition = Vector3.zero;
        panelRectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, goCanvas.GetComponent<RectTransform>().rect.width);
        panelRectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, goCanvas.GetComponent<RectTransform>().rect.height);
    }

    float lastcall = -25;
    private void Update()
    {
        if(Time.time > lastcall + 30)
        {
            lastcall = Time.time;
            //Fadeup
            StartCoroutine(FadeUp());
        }
    }

    private IEnumerator FadeUp()
    {
       float opacity = 0;
       CanvasGroup cg = this.gameObject.GetComponent<CanvasGroup>();

        while (opacity < 1.0f)
        {
            opacity += 0.01f;
            cg.alpha = opacity;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSecondsRealtime(5);
        StartCoroutine(FadeDown());
    }

    private IEnumerator FadeDown()
    {
        float opacity = 1.0f;
        CanvasGroup cg = this.gameObject.GetComponent<CanvasGroup>();

        while (opacity > 0.0f)
        {
            opacity -= 0.01f;
            cg.alpha = opacity;
            yield return new WaitForEndOfFrame();
        }
    }
}
