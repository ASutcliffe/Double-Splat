﻿#define DEBUG

#undef DEBUG
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenshotManager : MonoBehaviour {
    
    private bool takeShot = false;
    private bool showResult = false;
    private int timeDelay = 10;

    public string screenShotURL = "http://127.0.0.1:9600";

    public Texture2D reflexSnapshotBackground;
    public Texture2D flashTexture;

    Image flashImage = null;

    public AudioClip flashSound = null;

    void OnEnable()
    {
        OnGameReady();
        KeyboardManager.OnKeyUp += KeyboardManager_OnKeyUp;
#if(DEBUG == true)
        LogWriter.Log("Screenshot Event Handlers Bound");
#endif

        if (flashImage == null)
        {
            Canvas canvas = this.gameObject.AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;

            GameObject flash = new GameObject();
            flash.name = "Flash Screen";
            flash.transform.parent = this.transform;

            flashImage = flash.AddComponent<Image>(); //Add the Image Component script
            flashImage.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);

            RectTransform flashRT = flash.GetComponent<RectTransform>();
            flashRT.localPosition = Vector3.zero;
            flashRT.anchorMin = Vector2.zero;
            flashRT.anchorMax = Vector2.one;
            flashRT.pivot = new Vector2(.5f, .5f);

            flash.SetActive(true);
        }

        EffectController.Instance.ScrenshotManagerLoadStatus = true;

    }

    private void OnGameReady()
    {
        if (ParameterLoader.Instance.getParamValue("param_UpdateThumbnail", ref takeShot))
        {
#if (DEBUG == true)
            LogWriter.Log("Screenshot Triggers in "+ timeDelay + " Seconds");
#endif
            StartCoroutine(DelayedShot(timeDelay));
        }
        else
        {
#if (DEBUG == true)
            LogWriter.Log("No Screenshot Needed");
#endif
        }
    }

    /// <summary>
    /// Replaces ReflexTextures with the 'reflexSnapshotBackground' Texture
    /// </summary>
    /// <param name="bypass"></param>
    private void SetReflexSnapshotBypass(bool bypass)
    {
        /// Iterate though all objects with ReflexTexture Tag
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("ReflexTexture"))
        {
            if (bypass)
            {
                ///Replace reflex textures with snapshot
                go.GetComponent<Renderer>().material.mainTexture = reflexSnapshotBackground;
  
            }
            else
            {
                ///Set reflex textures back
                //go.GetComponent<Renderer>().material.mainTexture = TextureLoader.webCamTexture;
            }
        }
    }


    private void KeyboardManager_OnKeyUp(KeyCode key)
    {
        if (key == KeyCode.P && !showResult)
        {
            takeShot = true;
        }
    }

    void OnGUI()
    {
        if (showResult)
        {
            GUI.Label(new Rect(0, 0, 400, 50), "Thumbnail Updated");
        }
    }

    void LateUpdate()
    {
        if (takeShot)
        {
            /// Send to host.
            StartCoroutine(UploadPNG());
            takeShot = false;
        }
    }

    IEnumerator UploadPNG()
    {
#if(DEBUG == true)
        LogWriter.Log("Screenshot Taken");
#endif
        /// Bypass ReflexTextures for screenshots
        SetReflexSnapshotBypass(true);

        ///Wait for frame to render 
        yield return new WaitForEndOfFrame();

        ///Convert Camera render texture to JPEG Encoded byte array
        byte[] bytes = CaptureFromCamera(Camera.main).EncodeToJPG();

        /// Bypass ReflexTextures for screenshots
        SetReflexSnapshotBypass(false);

        /// Upload to a cgi script
        WWW w = new WWW(screenShotURL, bytes);
        yield return w;
        if (!string.IsNullOrEmpty(w.error))
        {
            print(w.error);
        }
        else
        {
#if(DEBUG == true)
            LogWriter.Log("Finished Uploading Screenshot");
#endif
            StartCoroutine(TempShow());
            ///Flash the Screen and Play Chime (This hides the Single logo frame that replaces the camera feed and provides the with feedback of operation)
            StartCoroutine(FlashCoroutine());

        }
    }

    IEnumerator FlashCoroutine()
    {

        //float fadeTimeLength = .75f; ///Time for Flash to fade from screen
        //bool fadedowndone = false;
        //float startTime = Time.time;

        //if (flashImage.sprite == null)
        //{
        //    flashImage.sprite = Sprite.Create(flashTexture, new Rect(0, 0, flashTexture.width, flashTexture.height), new Vector2(0.5f, 0.5f));
        //}

        if (flashSound != null)
        {
            GetComponent<AudioSource>().PlayOneShot(flashSound, 1f);
        }

        //while (!fadedowndone)
        //{
        //    float perc;
        //    Color col = flashImage.color;

        //    perc = Time.time - startTime;
        //    perc = perc / fadeTimeLength;

        //    if (perc > 1.0f)
        //    {
        //        perc = 1.0f;
        //        fadedowndone = true;
        //    }

        //    col.a = Mathf.Lerp(1.0f, 0.0f, perc);
        //    flashImage.color = col;

        //    yield return null;
        //}

        yield break;
    }

    /// <summary>
    /// Captures the Camera frame as a Texture2D
    /// </summary>
    /// <param name="camera">Camera to be captured</param>
    /// <returns>View from camera as Texture2D</returns>
    private Texture2D CaptureFromCamera(Camera camera)
    {
        Rect rect = new Rect(0, 0, camera.pixelWidth, camera.pixelHeight);
        RenderTexture renderTexture = new RenderTexture(camera.pixelWidth, camera.pixelHeight, 24);
        Texture2D screenShot = new Texture2D(camera.pixelWidth, camera.pixelHeight, TextureFormat.RGB24, false);


        camera.targetTexture = renderTexture;
        camera.Render();

        RenderTexture.active = renderTexture;

        screenShot.ReadPixels(rect, 0, 0);
        screenShot.Apply();

        camera.targetTexture = null;
        RenderTexture.active = null;

        Destroy(renderTexture);
        renderTexture = null;
        return screenShot;
    }

    IEnumerator TempShow()
    {
        showResult = true;
        yield return new WaitForSeconds(3f);
        showResult = false;
    }

    public void UpdateThumbnail(float time)
    {
        StartCoroutine(DelayedShot(time));
    }

    IEnumerator DelayedShot(float time)
    {
        yield return new WaitForSeconds(time);

        takeShot = true;
    }
}


