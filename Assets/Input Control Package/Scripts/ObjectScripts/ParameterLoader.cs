#define DEBUG
///When building product should always #undef DEBUG
#undef DEBUG

#define DEBUG_UNITY
///When Building product this should always be #undef DEBUG_UNITY
///This forces parameters to be loaded from debugMessage file 
#undef DEBUG_UNITY

#region DynamicVariableManager
// Note: Do not use DEBUG. Does not work in Unity

#define DEBUG_LANGUAGE 
#undef DEBUG_LANGUAGE 
#endregion


using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using omiLibrary;
using System.Globalization;

public class ParameterLoader : MonoBehaviour {
	// Loader Static Reference
	public static ParameterLoader Instance = null;

	//[SerializeField]
	//public List<Parameter> parameters;
	public Dictionary<string, string> parameters;

	// *****   When building product should always be set to false
#if (DEBUG == true)
		// Debug
	private bool ReadFromFile = true;
#else
		// Not debug; Product
	private bool ReadFromFile = false;
#endif


	void Awake()
	{
		Instance = this;
	}

	void OnEnable() {

		Debug.Log("-----|      " + Application.productName + "  omiLibrary DLL " + omiLibCommon.Getversion() + " | -----");

		//parameters = new List<Parameter>();
		parameters = new Dictionary<string, string>();

#if DEBUG_UNITY
		Debug.LogWarning("WARNING: Visual Studio debugging code executed. Do not release to customers");
		LogWriter.Log("Getting Parameters from File");
		ReadFromFile = true;
#else
		LogWriter.Log("Getting Parameters from Args");
#endif

		if (ReadFromFile)
		{
			//MAINTENANCE CODE
			string ParametersFromFile = ReadString();
			//string[] parametersSplit = ParametersFromFile.Split(',');
			string[] parametersSplit = substring(ParametersFromFile, '"', '"');
			Parameter p;
			foreach (string s in parametersSplit)
			{
				//parameters.Add(GetParam(s));
				p = GetParam(s);
				parameters.Add(p.ParameterName.ToUpper(), p.value);

			}
		}
		else
		{
			// From HOST
			try
			{
				// Get parameters from Windows
				string[] args = Environment.GetCommandLineArgs();
				
				Parameter p;
				// Store parameters in local list
				foreach (string s in args)
				{
					//parameters.Add(GetParam(s));
					p = GetParam(s);
					parameters.Add(p.ParameterName.ToUpper(), p.value);
				}

			}
			catch (Exception ex)
			{
				LogWriter.Log(ex.ToString());
			}
		}

	}

	private void Start()
	{
		/*
		 * Enable Precedes Start Manager is dependent on parameter loader completing its initialisation.
		 * Parameter manager Starts after parameter loader has finished loading all parameters from the host
		 */
		EffectController.Instance.ActivateParameterManager();
		EffectController.Instance.ParameterLoaderLoadStatus = true;

	}

	private string[] substring(string _string, char Delimiter1, char Delimiter2)
	{
		List<string> substrings = new List<string>();

	   
		for(int index = 0; index < _string.Length; index++)
		{
			if (_string[index] == Delimiter1)
			{
				int endex = _string.IndexOf(Delimiter2, index + 1);
				int length = endex - index;

				string substring = _string.Substring(index + 1, length - 1);
				substrings.Add(substring);

				index = endex + 1;
			}

		}
		
		return substrings.ToArray();
	}


	private Parameter GetParam(string paramString)
	{
		int start = paramString.IndexOf('<') + 1;
		int end = paramString.IndexOf('>', start);

		if(end - start < 0)
		{
			return new Parameter("Parameter", "null"); 
		}

		string valueString = paramString.Substring(start, end - start);

		int nameEnd = paramString.IndexOf('<');
		string nameString = paramString.Substring(0, nameEnd);
#if (DEBUG == true)
		//LogWriter.Log("Parameter: " + nameString + " Value : " + valueString);
#endif


		return new Parameter(nameString, valueString);
	}

	public T getParamValue<T>(string parameter, ref T value)
	{
#if Debug
		LogWriter.Log("Fetching " + parameter);
#endif

		if (parameters != null)
		{
			// The code below was replaced as it looks inefficient to search through a 
			// list for every parameter when the key was established at the beginning.
			//foreach (Parameter param in ParameterLoader.Instance.parameters)
			//{
			//    if (param.ParameterName.ToUpper() == parameter.ToUpper())
			//    {

			// Convert search key to upper case for common format
			string key = parameter.ToUpper();
			string paramValue = null;
			// Check if search failure
			if (!parameters.TryGetValue(key, out paramValue))
			{
				// NOT - is a failure
				// return the default value
				return (T)Convert.ChangeType(value, typeof(T)); //------>
			}


			// All successes from look up
			// Convert to code defined type
			if (typeof(T) == typeof(bool))
			{
				bool temp = false;
				if (!bool.TryParse(paramValue, out temp)) temp = (bool)Convert.ChangeType(value, typeof(bool));
				return (T)Convert.ChangeType(temp, typeof(T));
			}
			else if (typeof(T) == typeof(int))
			{
				int temp = 0;
				if (!int.TryParse(paramValue, out temp)) temp = (int)Convert.ChangeType(value, typeof(int));
				return (T)Convert.ChangeType(temp, typeof(T));
			}
			else if (typeof(T) == typeof(float))
			{
				float temp = 0;
				if (!float.TryParse(paramValue, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out temp)) temp = (float)Convert.ChangeType(value, typeof(float));
				return (T)Convert.ChangeType(temp, typeof(T));
			}
			else if (typeof(T) == typeof(string))
			{
				return (T)Convert.ChangeType(paramValue, typeof(T));
			}
			//    }
			//}
		}
		return (T)Convert.ChangeType(value, typeof(T)); 
	}

	static string ReadString()
	{
		// Instead of the path to a fixed file a path is given to a dynamic 
		// file shared by the host for the host to provide the arguments
		//string path = "Assets/Resources/Params.txt";
		string specialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\OMi Surface DEV\";
		string path = specialDirectory + "debugUnityMsg.txt";

		string parameters = "";
		//Read the text from directly from the test.txt file
		StreamReader reader = new StreamReader(path);
		parameters = reader.ReadToEnd();
		reader.Close();
		return parameters;
	}
}
