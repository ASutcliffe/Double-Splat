﻿//#define DEBUG
///When building product should always #undef DEBUG
#undef DEBUG

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KeyboardManager : MonoBehaviour
{
    public static KeyboardManager Instance;

    // Key board event handler signature
    public delegate void KeyboardHandler(KeyCode key);
    // Keydown event
    public static event KeyboardHandler OnKeyDown;
    // Keyup event
    public static event KeyboardHandler OnKeyUp;


    void Awake()
    {
        Instance = this;
    }
	private void OnEnable()
	{
		EffectController.Instance.KeyboardManagerLoadStatus = true;

	}

    List<IEKeyCode> PressedKeys = new List<IEKeyCode>();

    void OnGUI()
    {
        Event e = Event.current;

        if (e.isKey)
        {
#if (DEBUG == true)
        LogWriter.Log("Keyboard Manager: Key {" + e.keyCode + "} KeyDown");
#endif
            // Generate key down event
            if (OnKeyDown != null)
                OnKeyDown(e.keyCode);

            if (!PressedKeys.Exists(x=>x.code == e.keyCode))
                PressedKeys.Add(new IEKeyCode(e.keyCode,Time.time));

        }
    }

    private void Update()
    {
        foreach(IEKeyCode key in PressedKeys.GetRemoveSafeEnumerator())
        {
            if (Input.GetKeyUp(key.code))
            {
#if (DEBUG == true)
                LogWriter.Log("Keyboard Manager: Key {" + key.code + "} KeyUp");
#endif
                PressedKeys.Remove(key);

                // Generate key up event
                if (OnKeyUp != null)
                    OnKeyUp(key.code);
            }


        }
    }
}
