﻿
using System.Collections.Generic;

public static class Globals
{
	/// <summary>
	/// Type: enum DynamicVariables
	/// This type enumerates the dynamic variables
	/// of the Catch Effect (observer)
	/// Usage: 
	/// New parameters
	///     Add new enums for new parameters.
	///     Make sure all language translation exists in omiLibrary.Properties.Resources.
	/// Deleting parameters
	///     Comment out the enum you do not require.
	///     Comment out the key actions for that parameter
	///     Alternative
	///         Use conditional compilation variable to exclude operation
	/// </summary>
	public enum DynamicVariables
	{
		// WARNING: The elements of the enum (except None and Last) key the multi-language dictionaries
		// in Resources and so must exactly match the dictionaries.

		None, // used to tag elements outside this enum list
		GameMode,
		ParticlesDS,
		Player1Area,
		Player2Area,
		//Player1Speed,
		//Player2Speed,
		//Player1Count,
		//Player2Count,
		//Player1Scale,
		//Player2Scale,
		//Player1Difficulty,
		//Player2Difficulty,
		Last // used to tag end of enum list

	}//enum

	// Use conditional compilation instead of active code:
	//public static List<DynamicVariables> DisabledDynamicVariables = new List<DynamicVariables>();
	//public static void DisableDynamicVariable(DynamicVariables toDisable)
	//{
	//    if (!DisabledDynamicVariables.Contains(toDisable))
	//        DisabledDynamicVariables.Add(toDisable);
	//}
	public static string translateDynamicVariable(string oldString)
	{
		string newString = DynamicVariableManager.Instance.DVariable.translateString(oldString);
		if (newString == null) newString = oldString;
		return newString;
	}
}