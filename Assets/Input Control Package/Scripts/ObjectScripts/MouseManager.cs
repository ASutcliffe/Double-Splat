﻿using System.Collections;
using UnityEngine;


public class MouseManager : MonoBehaviour {
    #region football
    public static MouseManager Instance;

    public delegate void MouseHandler(Vector3 pos);
    public static event MouseHandler OnMouseInteraction;

    public Vector2 player1Position,player2Position;
    private Vector2 player1Clamp
    {
        get { return player1Position; }
        set { player1Position = new Vector2(Mathf.Clamp(value.x,0f, Screen.width / 2) /(Screen.width/2), Mathf.Clamp(value.y,0f,Screen.height)/Screen.height); }
    }
	private Vector2 player2Clamp
	{
		get { return player2Position; }
		set { player2Position = new Vector2(Mathf.Abs((Mathf.Clamp(value.x, Screen.width / 2, Screen.width) - Screen.width) / (Screen.width / 2)), Mathf.Clamp(value.y, 0f, Screen.height) / Screen.height); }
	}
    bool mouseAlwaysOn = false;
    //public bool mouseInput = false;
    //public mousePlayers mousePlayer = mousePlayers.Player1;
	//public enum mousePlayers
	//{
	//	Player1 = 0,
	//	Player2 = 1
	//}

    private void Awake()
    {
        Instance = this;
        ///Set Mouse Always on Flag using Mouse Always On Parameter
        mouseAlwaysOn = ParameterLoader.Instance.getParamValue("param_MouseAlwaysOn", ref mouseAlwaysOn);

        ///Override On Previous Assignment if App_Creator License exists
        mouseAlwaysOn |= ParameterLoader.Instance.getParamValue("param_DeveloperMode", ref mouseAlwaysOn);


        if (mouseAlwaysOn)
            Cursor.visible = true;
    }
    private void Start()
    {
        EffectController.Instance.MouseManagerLoadStatus = true;
    }

    void Update()
    {

        // 0 = left button, 1 = right button, 2 = middle button.
        const int leftButton = 0;
        if (Input.GetMouseButton(leftButton) || mouseAlwaysOn)
        {
            if (!TwoPlayerInputController.isTouch)
            {
                // Prevent position from being overwritten by interaction before collision is detected.
                TwoPlayerInputController.Instance.isDeviceInput = true;

                // Scale modifications to the mouse position using the clamp function
                // z = 0 is discarded
                Vector2 mouseHere = Input.mousePosition;
                if (TwoPlayerInputController.Instance.isPlayer1(mouseHere)) 
                   { player1Clamp = mouseHere; } 
                else 
                   { player2Clamp = mouseHere; }

                // Move player. 
                // Note: player1(and2)position is updated by player1(and2)Clamp.
                TwoPlayerInputController.Instance.BindDeviceInteractionToDoubleSplat(mouseHere, player1Position, player2Position);

            }
        }
        else
        {
            // Allow interaction to overwrite position
            TwoPlayerInputController.Instance.isDeviceInput = false;
        }
    }

    /// <summary>
    /// Not currently used
    /// </summary>
    /// <param name="pos"></param>
    void SendUpdate(Vector3 pos)
    {
        if (OnMouseInteraction != null)
            OnMouseInteraction(pos);
    }

    #endregion
}//class
