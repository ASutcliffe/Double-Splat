﻿#define DEMO_USE_ONLY
#undef DEMO_USE_ONLY
#define DEBUG
#undef DEBUG
using UnityEngine;
using System.Collections;


public class EffectController : MonoBehaviour {

	public static EffectController Instance = null;
	public GameObject loadscreenManager;
	public GameObject parameterLoader,parameterManager,GameController,MouseManager,ScreenShotManager,DevToolsManager,BackgroundManager,ForegroundManager,AudioManager,InteractionManager,gameManager;
	private GameObject demoUseOnly;
	public GameObject player1,player2;
	bool gameStarted = false;
	public int LoadProgress = 0;
	// will need to be increased as load items are added either the sum of all values or the next value along minus 1
	public int targetLoadValue = 8191;
	/*
	 ---How Load Status Works---
	 when set to true a load status will set a bit on the interger LoadProgress to 1 and then 0 on a false.
	targetLoadValue is the total value when all loadstatus are set to true.

	So if InteractionManagerLoadStatus is true it has a value of 4 which means the load progess will look like 0100
	if both interaction manager and loadScreenLoadStatus(2) are true then load progress will look like 0110 and will
	have a value of 6.

	if the game gets stuck on the load screen you can check the load progress value to see which item isnt loading
	by checking for missing bits vs the target load value it will either be a gap in a series of bits or it will be 
	the first 0 at the end of a series of bits causing the problem.
	 
	 */
	#region ParameterLoaderLoadStatus       : 1
	public bool ParameterLoaderLoadStatus
	{
		get { return (LoadProgress & 1) == 1 ? true : false; }
		set 
		{ 
			LoadProgress = value ? LoadProgress | 1 : LoadProgress & ~1;
		}
	}
	#endregion
	#region LoadScreenLoadStatus            : 2
	public bool LoadScreenLoadStatus
	{
		get { return (LoadProgress & 2) == 2 ? true : false; }
		set 
		{ 
			LoadProgress = value ? LoadProgress | 2 : LoadProgress & ~2;
			
		}
	}
	#endregion
	#region InteractionManagerLoadStatus    : 4
	public bool InteractionManagerLoadStatus
	{
		get { return (LoadProgress & 4) == 4 ? true : false; }
		set 
		{ 
			LoadProgress = value ? LoadProgress | 4 : LoadProgress & ~4;
		}
	}
	#endregion
	#region KeyboardManagerLoadStatus       : 8
	public bool KeyboardManagerLoadStatus
	{
		get { return (LoadProgress & 8) == 8 ? true : false; }
		set 
		{ 
			LoadProgress = value ? LoadProgress| 8 : LoadProgress & ~8;
			
		}
	}
	#endregion
	#region MouseManagerLoadStatus          : 16
	public bool MouseManagerLoadStatus
	{
		get { return (LoadProgress & 16) == 16 ? true : false; }
		set 
		{ 
			LoadProgress = value ? LoadProgress | 16 : LoadProgress & ~16;
			
		}
	}
	#endregion
	#region ScreenshotManagerLoadStatus     : 32
	public bool ScrenshotManagerLoadStatus
	{
		get { return (LoadProgress & 32) == 32 ? true : false; }
		set 
		{ 
			LoadProgress = value ? LoadProgress | 32 : LoadProgress & ~32;
			
		}
	}
	#endregion
	#region DevelopmentManagerLoadStatus    : 64
	public bool DevelopmentManagerLoadStatus
	{
		get { return (LoadProgress & 64) == 64 ? true : false; }
		set 
		{ 
			LoadProgress = value ? LoadProgress | 64 : LoadProgress & ~64;
			
		}
	}
	#endregion
	#region GameManagerLoadStatus           : 128
	public bool GameManagerLoadStatus
	{
		get { return (LoadProgress & 128) == 128 ? true : false; }
		set 
		{ 
			LoadProgress = value ? LoadProgress | 128 : LoadProgress & ~128;
			
		}
	}
	#endregion
	#region UIControllerLoadStatus          : 256
	public bool UIControllerLoadStatus
	{
		get { return (LoadProgress & 256) == 256 ? true : false; }
		set 
		{ 
			LoadProgress = value ? LoadProgress | 256 : LoadProgress & ~256;
			
		}

	}
	#endregion
	#region GameControllerLoadStatus        : 512
	public bool GameControllerLoadStatus
	{
		get { return (LoadProgress & 512) == 512 ? true : false; }
		set 
		{ 
			LoadProgress = value ? LoadProgress | 512 : LoadProgress & ~512;
			
		}
	}
	#endregion
	#region BackgroundLoadStatus		   : 1024
	public bool BackGroundLoadStatus
	{
		get	{return (LoadProgress & 1024) == 1024 ? true : false;}
		set 
		{ 
			LoadProgress = value ? LoadProgress | 1024 : LoadProgress & ~1024;
			
		}
	}
	#endregion
	#region ForegroundLoadStatus		   : 2048
	public bool ForeGroundLoadStatus
	{
		get { return(LoadProgress & 2048) == 2048 ? true : false;}
		set 
		{ 
			LoadProgress = value ? LoadProgress | 2048 : LoadProgress & ~2048;
			
		}
	}
	#endregion
	#region ParameterManagerLoadStatus	   : 4096
	public bool ParameterManagerLoadStatus
	{
		get { return(LoadProgress & 4096) == 4096 ? true : false;}
		set 
		{ 
			LoadProgress = value ? LoadProgress | 4096 : LoadProgress & ~4096;
		}
	}
	#endregion
	public LogWriter FileLog;

	public static GameObject guideObject { set; get; } // Used to control guide

	//private string LoadingScreenImage = "";

	#region Activate Parameter Loader
	public void ActivateParameterLoader()
	{
		parameterLoader.gameObject.SetActive(true);
	}
	#endregion
	#region Activate Parameter Manager
	public void ActivateParameterManager()
	{
		parameterManager.gameObject.SetActive(true);
	}
	#endregion
	#region Activate Load Screen Manager
	public void ActivateLoadScreenManager()
	{
		loadscreenManager.gameObject.SetActive(true);
	}
	#endregion
	#region Activate Game Manager
	public void ActivateGameManager()
	{
		gameManager.gameObject.SetActive(true);
	}
	#endregion

	private void Awake()
	{
		Application.targetFrameRate = 60;
		Instance = this;
		ActivateParameterLoader();
	}
	// Use this for initialization
	void Start () {
		FileLog = new LogWriter(true);
		Cursor.visible = false;



#if DEMO_USE_ONLY
		setDemoUse();
#endif
		Transform[] allChildren = GetComponentsInChildren<Transform>(true);
		foreach (Transform child in allChildren)
		{
			LogWriter.Log(child.name);
			child.gameObject.SetActive(true);
		}


		// Handler for game ready
		//GameManager.OnGameReady += Instance_OnGameReady;
		
		// game will call the Handler here when ready


	}// method
	/// <summary>
	/// Unity Handler:
	/// This switches a specific object (GuideSphere) of CatcherForce.
	/// This code cannot be in that object because Update does not
	/// run if it is switched OFF.
	/// This code could be in GameManager(GM) 
	/// but since it is possible GM may be redundant, it is placed here. 
	/// </summary>
	private void Update()
	{
		if (LoadProgress == targetLoadValue && !gameStarted)
		{
			player1.SetActive(true);
			Player.Player1.isInteractable = Player.Player1.isInteractable;
			player2.SetActive(true);
			Player.Player2.isInteractable = Player.Player2.isInteractable;
			loadscreenManager.GetComponent<LoadScreen>().LoadscreenHide();
			gameStarted = true;
		}
	}
	/// <summary>
	/// Method:
	/// </summary>
	/// <summary>
	/// Event Handler:
	/// For key down event
	/// </summary>
	/// <param name="key"></param>
	private void KeyboardManager_OnKeyDown(KeyCode key)
	{
		switch (key)
		{
			case KeyCode.Escape:
			case KeyCode.Return:
				Utilities.Quit();
				break;
		}
	}

	void OnApplicationQuit()
	{
#if (DEBUG == true)
		LogWriter.Log("EffectController: Application Closing");
#endif
	}

	void Ondisabled()
	{
		//GameManager.OnGameReady -= Instance_OnGameReady;
	}

	private void Instance_OnGameReady()
	{        
		loadscreenManager.GetComponent<LoadScreen>().LoadscreenHide();

		//GameManager.Instance.  DynamicVariableSetup();

		StartCoroutine(DelayedStartup(2));

	}

	IEnumerator DelayedStartup(float delay)
	{
		yield return new WaitForSecondsRealtime(delay);

		KeyboardManager.OnKeyDown += KeyboardManager_OnKeyDown;
		yield return null;


	}

	private void setDemoUse()
	{
		demoUseOnly = new GameObject("DemoUseOnly");
		demoUseOnly.transform.SetParent(transform.parent);
		demoUseOnly.AddComponent<DemoUseOnly>();
		demoUseOnly.SetActive(true);
	}
}
