﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Interfaces;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
	// Game ready handler
	//public delegate void EventHandler();
	// Game ready event
	//public static event EventHandler OnGameReady;

	//public static event EventHandler OnGameReset;
	//public static event EventHandler OnGameEnd;
	//public static event EventHandler OnGameFreeze;
	//public static event EventHandler OnGameUnfreeze;

	// Freeze state change event
	public static event FreezeInteractionManager.InteractionFreezeStateChangeHandler OnGameFreezeStateChange;

	//public static event DynamicVariableChangeHandler DynamicVariableChanged;
	//public delegate void DynamicVariableChangeHandler();

	public static GameManager Instance;
	public Texture2D InteractionFreezeImage;

	public Canvas sheet;
	public UnityEngine.UI.Button buttonGUI;
	public int gameSpeed = 0;


	private void Start()
	{
		EffectController.Instance.GameManagerLoadStatus = true;
	}
	DynamicVariableManager dv;
	void DynamicVariableSetup()
	{
		if (dv == null)
		{

			dv = gameObject.AddComponent<DynamicVariableManager>();
			dv.buttonGUI = buttonGUI;
			dv.sheet = sheet;
		}
	}

	FreezeInteractionManager ifm;
	void InteractionFreezeSetup()
	{
		if (ifm == null)
		{
			ifm = new FreezeInteractionManager(InteractionFreezeImage);
			//ifm = gameObject.GetComponent<FreezeInteractionManager>();
					
			FreezeInteractionManager.InteractionFreezeStateChange += FreezeInteractionManager_InteractionFreezeStateChange;
		}
	}
	/// <summary>
	/// Event Handler
	/// For freeze state change event from FreezeInteractionManager.
	/// This code is redundant for Catch.
	/// This code raises an event that is picked up by the InteractionManager for handling.
	/// Catch could trigger the InteractionManager directly.
	/// </summary>
	/// <param name="State"></param>
	private void FreezeInteractionManager_InteractionFreezeStateChange(bool State)
	{

#if (DEBUG == true)

		if (State)
			LogWriter.Log("Game Manager: Game Freeze");
		else
			LogWriter.Log("Game Manager: Game Unfreeze");
#endif
		// This handler changes the frozen flag in InteractionManager
		// by raising an event
		// 
		if (OnGameFreezeStateChange != null)            
			OnGameFreezeStateChange(State);

		//if (State)
		//    if (OnGameFreeze != null)
		//        OnGameFreeze();
		//    else
		//    if (OnGameUnfreeze != null)
		//        OnGameUnfreeze();
	}

	// Use this for triggered initialization
	void OnEnable()
	{
		Instance = this;
		//List<IAwaitableLoad> awaitableLoads = new List<IAwaitableLoad>();

  //      //Initialize Game Elements
  //      Transform[] allChildren = GetComponentsInChildren<Transform>(true);
  //      foreach (Transform child in allChildren)
  //      {
  //          child.gameObject.SetActive(true);

  //          Component[] cs = child.gameObject.GetComponents(typeof(IAwaitableLoad));
  //          if (cs.Length > 0)
  //          {
  //              awaitableLoads.Add((cs[0] as IAwaitableLoad));
  //          }
  //      }

		DynamicVariableSetup();
		InteractionFreezeSetup();


		//StartCoroutine(WaitForAllIAwaitables(awaitableLoads));

	}

	//    IEnumerator WaitForAllIAwaitables(List<IAwaitableLoad> awaitables)
	//    {
	//        // Wait for every element in the awaitables list to be ready
	//        // Currently only 1 element - Background
	//        yield return new WaitUntil(() => awaitables.TrueForAll(i => i.isReady == true));

	//        //Force a slight delay otherwise people get scared
	//        //yield return new WaitForSecondsRealtime(2);

	//        //Trigger GameReady State
	////        if (OnGameReady != null)
	////        {
	////#if (DEBUG == true)
	////            LogWriter.Log("Game Manager: Game Ready");
	////#endif
	////            //OnGameReady();
	////        }
	//    }//method

	public void clampGameSpeed()
	{
		gameSpeed = Mathf.Clamp(gameSpeed, -100, 100);
	}
}// class

