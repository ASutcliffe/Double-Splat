﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadScreen : MonoBehaviour
{
	public Canvas canvas;
	public Image LoadScreenImage;
	public static LoadScreen Instance;

	void OnEnable()
	{
		Instance = this;
		if (ParameterManager.Instance.LoadingScreenImage != null)
		{
			LoadScreenImage.sprite = ParameterManager.Instance.LoadingScreenImage.CreateSprite();
		}

		canvas.enabled = true;
#if (DEBUG == true)
		LogWriter.Log("Creating Loadscreen");
#endif

		EffectController.Instance.LoadScreenLoadStatus = true;
	}

	public void LoadscreenHide()
	{
#if (DEBUG == true)
		LogWriter.Log("Killing Loadscreen");
#endif
		StartCoroutine(killLoadScreen());
	}

	IEnumerator killLoadScreen()
	{

		GameController.Instance.GameStart();
		yield return new WaitForSecondsRealtime(2);
		canvas.enabled = false;
#if (DEBUG == true)
		LogWriter.Log("Loadscreen killed");
#endif

	}
}