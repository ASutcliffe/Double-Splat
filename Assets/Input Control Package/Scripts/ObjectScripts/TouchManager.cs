﻿using UnityEngine;

public class TouchManager : MonoBehaviour
{

    public static TouchManager Instance;

    // Touch event handler signature
    public delegate void TouchHandler(Touch touch, Vector2 player1Position, Vector2 player2Position);
    // Touch event
    public static event TouchHandler OnTouch;


    public Vector2 player1Position, player2Position;
    private Vector2 player1Clamp
    {
        get { return player1Position; }
        set { player1Position = new Vector2(   Mathf.Clamp(value.x, 0f, Player1_2Border) / Player1_2Border, 
                                               Mathf.Clamp(value.y, 0f, Screen.height) / Screen.height   ); }
    }
    private Vector2 player2Clamp
    {
        get { return player2Position; }
        set { player2Position = new Vector2(   Mathf.Abs(  (Mathf.Clamp(value.x, Player1_2Border, Screen.width) - Screen.width) / Player1_2Border  ), 
                                               Mathf.Clamp(value.y, 0f, Screen.height) / Screen.height   ); }
    }
    static float Player1_2Border = Screen.width / 2;

    void Awake()
    {
        Instance = this;
    }

    void Update()
    {
        Touch[] allTouches = Input.touches;

        if (Input.touchCount > 0)
        {
            Debug.Log("touch count = " + Input.touchCount);
            // Prevent position from being overwritten by interaction before collision is detected.
            TwoPlayerInputController.Instance.isDeviceInput = true;

            // Enable multiple touches
            foreach (Touch touch in allTouches)
            {
                // Scale modifications to the mouse position using the clamp function
                if (TwoPlayerInputController.Instance.isPlayer1(touch.position))
                    { player1Clamp = touch.position; }
                else
                    { player2Clamp = touch.position; }

                // Send event to move player object
                // Note: player1(and2)position is updated by player1(and2)Clamp.
                SendUpdates(touch, player1Position, player2Position);
            }
        }
        else
        {
            // Allow interaction to overwrite position
            TwoPlayerInputController.Instance.isDeviceInput = false;
        }
    }

    void SendUpdates(Touch touch, Vector2 player1Position, Vector2 player2Position)
    {
        if (OnTouch != null)
            OnTouch(touch, player1Position, player2Position);
    }



    //private void BindDeviceInteractionToDoubleSplat(Vector3 actHere)
    //{
    //    if (actHere.x > Player1_2Border)
    //    {
    //        //Player 2
    //        // Apply modifications to the mouse position using clamp function
    //        player2Clamp = actHere;
    //        // Move Player 2
    //        TwoPlayerInputController.Instance.Player2Input(player2Position);
    //    }
    //    else
    //    {
    //        //Player 1
    //        // Apply modifications to the mouse position using clamp function
    //        player1Clamp = actHere;
    //        // Move Player 1
    //        TwoPlayerInputController.Instance.Player1Input(player1Position);
    //    }

    //}

}//class
