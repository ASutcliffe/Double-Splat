﻿#define DEBUG
///When building product should always #undef DEBUG
#undef DEBUG
using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System;
using System.Collections.Generic;

public class InteractionManager : MonoBehaviour
{
    public static InteractionManager Instance;

    public delegate void InteractionHandler(int[] interaction, int iWidth, int iHeight);
    public static event InteractionHandler OnInteractionReceived;


    public delegate void ListInteractionHandler(List<Vector2> interactions, int iWidth, int iHeight);
    public static event ListInteractionHandler OnInteractionListReceived;

    public delegate void NetworkInteractionHandler();
    public static event NetworkInteractionHandler OnNetworkInteraction;

    TcpClient client;
    NetworkStream stream;

    int dwidth = 320;
    int dheight = 240;

    bool dbgTcpConnected = false;
    bool dbgTcpReceived = false;
    public bool interactionFrozen = false;

    byte[] dataToReceive;
    byte[] dataToProcess;

    public int gridDivider = 1;

    void Awake()
    {

        Instance = this;
        try
        {
            client = new TcpClient();
            client.Connect("127.0.0.1", 4444);
            //client.Connect("192.168.0.136", 4444);
            stream = client.GetStream();
#if (DEBUG == true)
            LogWriter.Log("Interaction Manager: Connecting to TCP Server to receive interactions");
#endif
        }
        catch
        {
#if (DEBUG == true)
            LogWriter.Log("Interaction Manager: Connection Failed..");
#endif
        }
    }

    void Start()
    {
        dwidth = ParameterLoader.Instance.getParamValue("param_InteractionWidth", ref dwidth);
        dheight = ParameterLoader.Instance.getParamValue("param_InteractionHeight", ref dheight);
        dataToProcess = new byte[(dwidth * dheight)];
        dataToReceive = new byte[(dwidth * dheight) + 5];

        GameManager.OnGameFreezeStateChange += GameManager_OnGameFreezeStateChange;
        EffectController.Instance.InteractionManagerLoadStatus = true;
    }

    private void GameManager_OnGameFreezeStateChange(bool State)
    {
        interactionFrozen = State;
#if (DEBUG == true)
        if (State)
            LogWriter.Log("Interaction Manager: Interaction Stopped");
        else
            LogWriter.Log("Interaction Manager: Interaction Resumed");
#endif
    }

    int buildingIndex = 0;

    void FixedUpdate()
    {


        if (!client.Connected)
            return;
        else
        {
            if (!dbgTcpConnected)
            {
#if (DEBUG == true)
                LogWriter.Log("Interaction Manager: Connected to TCP Server");
#endif
                dbgTcpConnected = true;
            }
        }// if client

        try
        {
            if (stream.DataAvailable)
            {
                int recv = stream.Read(dataToReceive, buildingIndex, dataToReceive.Length - buildingIndex);

                if (recv + buildingIndex < dataToReceive.Length)
                {
                    buildingIndex = recv;
                    return;
                }
                else
                {
                    buildingIndex = 0;
                }

                if (dataToReceive[0] == (byte)'S' && dataToReceive[1] == (byte)'T' && dataToReceive[2] == (byte)'A' && dataToReceive[3] == (byte)'R' && dataToReceive[4] == (byte)'T')
                {
					//char[] chars = { (char)dataToReceive[0], (char)dataToReceive[1], (char)dataToReceive[2], (char)dataToReceive[3], (char)dataToReceive[4] };
					//Debug.Log(new string(chars)=="START");
                    if (!dbgTcpReceived)
                    {
#if (DEBUG == true)
                        LogWriter.Log("Interaction Manager: Receiving data from TCP Server");
#endif
                        dbgTcpReceived = true;
                    }
                    // if dbg
                    //Array.Copy(dataToReceive, 5, dataToProcess, 0, dataToProcess.Length);
                    Buffer.BlockCopy(dataToReceive, 5, dataToProcess, 0, dataToProcess.Length);
                    //int[] interactions = dataToProcess.Select(x => (int)x).ToArray();
                    if (!interactionFrozen)
                    {
                        if (OnInteractionListReceived != null)
                            OnInteractionListReceived(getInteractions(dataToProcess), dwidth / gridDivider, dheight / gridDivider);


                        int[] interactions = GetIntArrayFromByteArray(dataToProcess);

                        if (gridDivider > 1)
                        {
                            int[] downscaledInteractions = DescaleInteractions(interactions);
                            SendUpdates(downscaledInteractions);
                        }
                        else
                        {
                            SendUpdates(interactions);
                        }
                    }
                }
                else
                {
#if (DEBUG == true)
                    LogWriter.Log("Interaction Manager: Not START");
#endif
                    stream.Flush();
                }
            }
        }
        catch (Exception ex)
        {
#if (DEBUG == true)
            LogWriter.Log("Interaction Manager: ERROR: " + ex.ToString());
#endif
        }
    }

    void SendUpdates(int[] interaction)
    {
        if (OnInteractionReceived != null)
            OnInteractionReceived(interaction, dwidth / gridDivider, dheight / gridDivider);


        if (UserInteraction(interaction))
        {
            if (OnNetworkInteraction != null)
                OnNetworkInteraction();
        }

    }
    bool UserInteraction(int[] interaction)
    {
        bool result = false;
        int certainty = 20;
        foreach (int i in interaction)
        {
            if (i > 0)
                certainty--;

            if (certainty <= 0)
                result = true;
        }
        return result;
    }

    int[] GetIntArrayFromByteArray(byte[] byteArray)
    {
        int[] shortArray = new int[byteArray.Length];
        for (int i = 0; i < byteArray.Length; i++)
            shortArray[i] = (int)byteArray[i];
        return shortArray;
    }

    int downsample = 2;
    List<Vector2> getInteractions(byte[] byteArray)
    {
        List<Vector2> interactions = new List<Vector2>();

        for (int i = 0; i < byteArray.Length; i++)
        {
            if (i % downsample == 0)
            {
                if (byteArray[i] > 0)
                    interactions.Add(getVectorFromByteIndex(i));
            }
        }

        return interactions;
    }

    public Vector2 getVectorFromByteIndex(int i)
    {

        float x = (i % dwidth);
        float y = (i / dwidth);

        return new Vector2(x, y);
    }

    private int[] DescaleInteractions(int[] interactions)
    {
        int[] result = new int[(dwidth / gridDivider) * (dheight / gridDivider)];
        for (int y = 0; y < dheight; y += gridDivider)
        {
            for (int x = 0; x < dwidth; x += gridDivider)
            {
                for (int i = y; i < y + gridDivider; i++)
                {
                    for (int j = x; j < x + gridDivider; j++)
                    {
                        if (interactions[(i * dwidth) + j] > 0)
                        {
                            result[((y / gridDivider) * (dwidth / gridDivider)) + (x / gridDivider)] = 1;
                        }
                    }
                }
            }
        }
        return result;
    }


    void OnApplicationQuit()
    {
        if (stream != null)
        {
            stream.Close();
            stream.Dispose();
        }

        if (client != null)
        {
            client.Close();
        }
    }

}