#define DEBUG
///When building product should always #undef DEBUG
#undef DEBUG

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using omiLibrary;

public class ParametersExtended : MonoBehaviour
{

	public static ParametersExtended Instance = null;
	private void Awake()
	{
		Instance = this;
	}
	public List<float> GetFloatList(string param)
	{
		List<float> _floatList = new List<float>();
		string _values = "";
		_values = ParameterLoader.Instance.getParamValue(param, ref _values);
		List<string> _stringList = _values.Split(';').ToList();

		for (int i = 0; i < _stringList.Count; i++)
		{
			_floatList.Add(float.TryParse(_stringList[i], out float _val) ? _val : 0);
		}

		return _floatList;
	}

	#region Get Vector
	#region Vector2
	/// <summary>
	/// Fetches Vector2 Values from Parameters
	/// </summary>
	/// <param name="_xParam">String Used to find Parameter x</param>
	/// <param name="_yParam">String Used to find Parameter y</param>
	/// <returns>Returns a Vector 2</returns>
	public Vector2 GetVector(string _xParam, string _yParam)
	{
		Vector2 _vector2 = new Vector2();
		_vector2.x = ParameterLoader.Instance.getParamValue(_xParam, ref _vector2.x);
		_vector2.y = ParameterLoader.Instance.getParamValue(_yParam, ref _vector2.y);
		return _vector2;
#if DEBUG
		Debug.Log(string.Format("Fetched Vector 2 from parameters with values {0}", _vector2));
#endif
	}
	public Vector2Int GetVectorInt(string _xParam, string _yParam)
	{
		Vector2Int _vector2Int = new Vector2Int();
		int refrenceInt = 80085;
		_vector2Int.x = ParameterLoader.Instance.getParamValue(_xParam, ref refrenceInt);
		_vector2Int.y = ParameterLoader.Instance.getParamValue(_yParam, ref refrenceInt);
		return _vector2Int;
#if DEBUG
		Debug.Log(string.Format("Fetched Vector 2 from parameters with values {0}", _vector2));
#endif
	}
	#endregion
	#region Vector3
	/// <summary>
	/// Fetches Vector 3 Values from Parameters
	/// </summary>
	/// <param name="_xParam">String Used to find Parameter x</param>
	/// <param name="_yParam">String Used to find Parameter y</param>
	/// <param name="_zParam">String Used to find Parameter z</param>
	/// <returns>Returns a Vector 3</returns>
	public Vector3 GetVector(string _xParam, string _yParam, string _zParam)
	{
		Vector3 _vector3 = new Vector3();
		_vector3.x = ParameterLoader.Instance.getParamValue(_xParam, ref _vector3.x);
		_vector3.y = ParameterLoader.Instance.getParamValue(_yParam, ref _vector3.y);
		_vector3.z = ParameterLoader.Instance.getParamValue(_zParam, ref _vector3.z);
#if DEBUG
		Debug.Log(string.Format("Fetched Vector 3 from parameters with values {0}", _vector3));
#endif
		return _vector3;
	}
	public Vector3Int GetVectorInt(string _xParam, string _yParam, string _zParam)
	{
		Vector3Int _vector3Int = new Vector3Int();
		int refrenceInt = 80085;
		_vector3Int.x = ParameterLoader.Instance.getParamValue(_xParam, ref refrenceInt);
		_vector3Int.y = ParameterLoader.Instance.getParamValue(_yParam, ref refrenceInt);
		_vector3Int.z = ParameterLoader.Instance.getParamValue(_zParam, ref refrenceInt);
		return _vector3Int;
#if DEBUG
		Debug.Log(string.Format("Fetched Vector 2 from parameters with values {0}", _vector2));
#endif
	}
	#endregion
	#region Vector4
	/// <summary>
	/// Fetches Vector 4 Values from Parameters
	/// </summary>
	/// <param name="_xParam">String Used to find Parameter x</param>
	/// <param name="_yParam">String Used to find Parameter y</param>
	/// <param name="_zParam">String Used to find Parameter z</param>
	/// <param name="_wParam">String Used to find Parameter w</param>
	/// <returns>Returns a Vector 4</returns>
	public Vector4 GetVector(string _xParam, string _yParam, string _zParam, string _wParam)
	{
		Vector4 _vector4 = new Vector4();
		_vector4.x = ParameterLoader.Instance.getParamValue(_xParam, ref _vector4.x);
		_vector4.y = ParameterLoader.Instance.getParamValue(_yParam, ref _vector4.y);
		_vector4.z = ParameterLoader.Instance.getParamValue(_zParam, ref _vector4.z);
		_vector4.w = ParameterLoader.Instance.getParamValue(_wParam, ref _vector4.w);
#if DEBUG
		Debug.Log(string.Format("Fetched Vector 4 from parameters with values {0}",_vector4));
#endif
		return _vector4;
	}
	#endregion
	#endregion
	#region Get Vector List
	#region GetVector2 List
	/// <summary>
	/// Fetches a List of vector 2s
	/// </summary>
	/// <param name="_xParam">String Used to find Parameter x</param>
	/// <param name="_yParam">String Used to find Parameter y</param>
	/// <returns></returns>
	public List<Vector2> GetVectorList(string _xParam, string _yParam)
	{
		List<Vector2> _vector2List = new List<Vector2>();
		string _xValue = "", _yValue = "";
		_xValue = ParameterLoader.Instance.getParamValue(_xParam, ref _xValue);
		_yValue = ParameterLoader.Instance.getParamValue(_yParam, ref _yValue);
		List<string> _xParamList = _xValue.Split(';').ToList();
		List<string> _yParamList = _yValue.Split(';').ToList();

		int _longestArrayLength = 0;

		_longestArrayLength = _longestArrayLength > _xParamList.Count ? _longestArrayLength : _xParamList.Count;
		_longestArrayLength = _longestArrayLength > _yParamList.Count ? _longestArrayLength : _yParamList.Count;

		if (_xParamList.Count < _longestArrayLength)
		{
			for (int i = _xParamList.Count; i < _longestArrayLength; i++)
			{
				_xParamList.Add(_xParamList[0]);
			}
		}

		if (_yParamList.Count < _longestArrayLength)
		{
			for (int i = _yParamList.Count; i < _longestArrayLength; i++)
			{
				_yParamList.Add(_yParamList[0]);
			}
		}

		for (int i = 0; i < _xParamList.Count; i++)
		{
			Vector2 _vec2 = new Vector2();
			_vec2.x = float.TryParse(_xParamList[i], out float x) ? x : 0;
			_vec2.y = float.TryParse(_yParamList[i], out float y) ? y : 0;
			_vector2List.Add(_vec2);
		}
		return _vector2List;
	}
	#endregion
	#region GetVector3 list
	/// <summary>
	/// Fetches list of Vector 3s
	/// </summary>
	/// <param name="_xParam">String Used to find Parameter x</param>
	/// <param name="_yParam">String Used to find Parameter y</param>
	/// <param name="_zParam">String Used to find Parameter z</param>
	/// <returns></returns>
	public List<Vector3> GetVectorList(string _xParam, string _yParam, string _zParam)
	{
		List<Vector3> _vector3List = new List<Vector3>();
		string _xValue = "", _yValue = "", _zValue = "";
		_xValue = ParameterLoader.Instance.getParamValue(_xParam, ref _xValue);
		_yValue = ParameterLoader.Instance.getParamValue(_yParam, ref _yValue);
		_zValue = ParameterLoader.Instance.getParamValue(_zParam, ref _zValue);
		List<string> _xParamList = _xValue.Split(';').ToList();
		List<string> _yParamList = _yValue.Split(';').ToList();
		List<string> _zParamList = _zValue.Split(';').ToList();

		int _longestArrayLength = 0;

		_longestArrayLength = _longestArrayLength > _xParamList.Count ? _longestArrayLength : _xParamList.Count;
		_longestArrayLength = _longestArrayLength > _yParamList.Count ? _longestArrayLength : _yParamList.Count;
		_longestArrayLength = _longestArrayLength > _zParamList.Count ? _longestArrayLength : _zParamList.Count;

		if (_xParamList.Count < _longestArrayLength)
		{
			for (int i = _xParamList.Count; i < _longestArrayLength; i++)
			{
				_xParamList.Add(_xParamList[0]);
			}
		}

		if (_yParamList.Count < _longestArrayLength)
		{
			for (int i = _yParamList.Count; i < _longestArrayLength; i++)
			{
				_yParamList.Add(_yParamList[0]);
			}
		}

		if (_zParamList.Count < _longestArrayLength)
		{
			for (int i = _zParamList.Count; i < _longestArrayLength; i++)
			{
				_zParamList.Add(_zParamList[0]);
				;
			}
		}


		for (int i = 0; i < _xParamList.Count; i++)
		{
			Vector3 _vec3 = new Vector3();
			_vec3.x = float.TryParse(_xParamList[i], out float x) ? x : 0;
			_vec3.y = float.TryParse(_yParamList[i], out float y) ? y : 0;
			_vec3.z = float.TryParse(_zParamList[i], out float z) ? z : 0;
			_vector3List.Add(_vec3);
		}
		return _vector3List;
	}
	#endregion
	#region GetVector4 List
	/// <summary>
	/// Fetches a list of vector 4s
	/// </summary>
	/// <param name="_xParam">String Used to find Parameter x</param>
	/// <param name="_yParam">String Used to find Parameter y</param>
	/// <param name="_zParam">String Used to find Parameter z</param>
	/// <param name="_wParam">String Used to find Parameter w</param>
	/// <returns></returns>
	public List<Vector4> GetVectorList(string _xParam, string _yParam, string _zParam, string _wParam)
	{
		List<Vector4> _vector4List = new List<Vector4>();
		string _xValue = "", _yValue = "", _zValue = "", _wValue = "";
		_xValue = ParameterLoader.Instance.getParamValue(_xParam, ref _xValue);
		_yValue = ParameterLoader.Instance.getParamValue(_yParam, ref _yValue);
		_zValue = ParameterLoader.Instance.getParamValue(_zParam, ref _zValue);
		_wValue = ParameterLoader.Instance.getParamValue(_wParam, ref _wValue);
		List<string> _xParamList = _xValue.Split(';').ToList();
		List<string> _yParamList = _yValue.Split(';').ToList();
		List<string> _zParamList = _zValue.Split(';').ToList();
		List<string> _wParamList = _wValue.Split(';').ToList();

		int _longestArrayLength = 0;

		_longestArrayLength = _longestArrayLength > _xParamList.Count ? _longestArrayLength : _xParamList.Count;
		_longestArrayLength = _longestArrayLength > _yParamList.Count ? _longestArrayLength : _yParamList.Count;
		_longestArrayLength = _longestArrayLength > _zParamList.Count ? _longestArrayLength : _zParamList.Count;
		_longestArrayLength = _longestArrayLength > _wParamList.Count ? _longestArrayLength : _wParamList.Count;

		if (_xParamList.Count < _longestArrayLength)
		{
			for (int i = _xParamList.Count; i < _longestArrayLength; i++)
			{
				_xParamList.Add(_xParamList[0]);
			}
		}

		if (_yParamList.Count < _longestArrayLength)
		{
			for (int i = _yParamList.Count; i < _longestArrayLength; i++)
			{
				_yParamList.Add(_yParamList[0]);
			}
		}

		if (_zParamList.Count < _longestArrayLength)
		{
			for (int i = _zParamList.Count; i < _longestArrayLength; i++)
			{
				_zParamList.Add(_zParamList[0]);
				;
			}
		}

		if (_wParamList.Count < _longestArrayLength)
		{
			for (int i = _wParamList.Count; i < _longestArrayLength; i++)
			{
				_wParamList.Add(_wParamList[0]);
			}
		}

		for (int i = 0; i < _xParamList.Count; i++)
		{
			Vector4 _vec4 = new Vector4();
			_vec4.x = float.TryParse(_xParamList[i], out float x) ? x : 0;
			_vec4.y = float.TryParse(_yParamList[i], out float y) ? y : 0;
			_vec4.z = float.TryParse(_zParamList[i], out float z) ? z : 0;
			_vec4.w = float.TryParse(_wParamList[i], out float w) ? w : 0;
			_vector4List.Add(_vec4);
		}
		return _vector4List;
	}
	#endregion
	#endregion
	#region Texture
	public OmiTexture GetTexture(string _paramName)
	{
		string _location = " ";
		_location = ParameterLoader.Instance.getParamValue(_paramName, ref _location);
		if (System.IO.File.Exists(_location))
		{
			OmiTexture _texture = null;
			if (OmiTexture.IsGIF(_location))
			{
				_texture = new OmiGIF();
				_texture.GetTexture(_location);
			}
			else if (OmiTexture.IsVideo(_location))
			{
				_texture = new OmiMovie(_location);
			}
			else
			{
				_texture = new OmiImage(_location);
			}
			return _texture;
		}
		return null;
	}
	#endregion
	#region TextureList
	public List<OmiTexture> GetTextureList(string _paramName)
	{
		List<OmiTexture> _Textures = new List<OmiTexture>();
		OmiTexture _texture = null;
		string[] _imageLocations;
		string _paramFilePathString = "";
		_paramFilePathString = ParameterLoader.Instance.getParamValue(_paramName, ref _paramFilePathString);
		_imageLocations = _paramFilePathString.Split(';');
		foreach (string _location in _imageLocations)
		{
			if (System.IO.File.Exists(_location))
			{
				if (OmiTexture.IsGIF(_location))
				{
					_texture = new OmiGIF();
					_texture.GetTexture(_location);
				}
				else if (OmiTexture.IsVideo(_location))
				{
					_texture = new OmiMovie(_location);
				}
				else
				{
					_texture = new OmiImage(_location);
				}

				_Textures.Add(_texture);
			}
		}
		return _Textures;
	}
	#endregion
	#region Texture2D
	#region GetTexture2D
	/// <summary>
	/// Fetches Texture2D From Parameters
	/// </summary>
	/// <param name="_ParamName">String Used to find Parameter</param>
	/// <returns>Returns a Texture 2D</returns>
	public Texture2D GetTexture2D(string _ParamName)
	{
		string _location = " ";
		_location = ParameterLoader.Instance.getParamValue(_ParamName, ref _location);
		if (System.IO.File.Exists(_location))
		{
			byte[] _rawData = System.IO.File.ReadAllBytes(_location);
			Texture2D _texture = new Texture2D(2, 2);
			_texture.LoadImage(_rawData);
			_texture.name = System.IO.Path.GetFileNameWithoutExtension(_location);
			return _texture;
		}
		return null;
#if DEBUG
		Debug.Log("Texture Loaded Name " + _texture.name);
#endif
	}
	#endregion
	#region GetTexture2D List
	/// <summary>
	/// Fetches a texture 2d list file locations provided by the parameters
	/// </summary>
	/// <param name="_paramName">String Used to find Parameter</param>
	/// <returns>Returns a Texture 2D List</returns>
	public List<Texture2D> GetTexture2DList(string _paramName)
	{
		List<Texture2D> _Textures = new List<Texture2D>();

		string[] _imageLocations;
		string _paramFilePathString = "";
		_paramFilePathString = ParameterLoader.Instance.getParamValue(_paramName, ref _paramFilePathString);
		_imageLocations = _paramFilePathString.Split(';');
		foreach (string _location in _imageLocations)
		{
			if (System.IO.File.Exists(_location))
			{
				byte[] _rawData = System.IO.File.ReadAllBytes(_location);
				Texture2D _texture = new Texture2D(2, 2);
				_texture.LoadImage(_rawData);
				_texture.name = System.IO.Path.GetFileNameWithoutExtension(_location);
				_Textures.Add(_texture);
			}
		}
		return _Textures;
	}
	#endregion
	#endregion
	#region Color
	#region GetColor
	/// <summary>
	/// Fetches A color from the parameters
	/// </summary>
	/// <param name="_paramName">String Used to find Parameter</param>
	/// <returns>Returns a Color</returns>
	public Color GetColor(string _paramName)
	{
		Color _color = new Color();
		string _hex = null;
		_hex = ParameterLoader.Instance.getParamValue(_paramName, ref _hex);
		if (ColorUtility.TryParseHtmlString(_hex, out _color))
		{
			_color = new Color(_color.g, _color.b, _color.a, _color.r);

		}
		else
		{
			LogWriter.Log(_paramName + " is not Readable as a colour check the value '" + _hex + "' is correct Setting value to pink #FF00DC for easier identification");
			_color = new Color(1f, 0f, 0.86f);
		}

		return _color;
	}
	#endregion
	#region GetColor List
	/// <summary>
	/// Gets Hex color values from the parameters and converts them into a list of colors
	/// </summary>
	/// <param name="_paramName">String Used to find Parameter</param>
	/// <returns>Returns a list of Colors</returns>
	public List<Color> GetColorList(string _paramName)
	{
		//TODO REMOVE BLANK ITEM AT END OF STRING
		List<Color> _Colors = new List<Color>();
		List<string> _hexList = new List<string>();
		string _paramXListString = null;
		_paramXListString = ParameterLoader.Instance.getParamValue(_paramName, ref _paramXListString);
		_hexList = _paramXListString.Split(';').ToList();
		_hexList.RemoveAll(x => x == "");

		foreach (string _hexValue in _hexList)
		{


			if (ColorUtility.TryParseHtmlString(_hexValue, out Color _col))
			{
				_col = new Color(_col.g, _col.b, _col.a, _col.r);

			}
			else
			{
				LogWriter.Log("Value in " + _paramName + " is not Readable as a colour check the value '" + _hexValue + "' is correct Setting value to pink #FF00DC for easier identification");
				_col = new Color(1f, 0f, 0.86f);
			}

			_Colors.Add(_col);
		}
		return _Colors;
	}
	#endregion
	#endregion

}
