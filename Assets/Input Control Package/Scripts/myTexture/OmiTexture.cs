﻿
using omiLibrary.GifTexture;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.Video;
#region Type Enumerator
/// <summary>
/// Types used to determin which time of texture an OmiTexture is
/// if adding new texture type add a 3 character identifier to this list
/// for it
/// </summary>
public enum TextureType { GIF, OBJ, IMG, MOV }
#endregion
#region OmiTexture Abstract Class
/// <summary>
/// Abstract Class
/// </summary>
public abstract class OmiTexture
{
	#region Variables
	public abstract string name { get; set; }
	public abstract string filePath { get; set; }
	public abstract Vector2Int Dimensions { get; set; }
	public abstract TextureType Type { get; set; }
	#endregion
	/// <summary>
	/// Method:
	/// Get image from file.
	/// </summary>
	/// <param name="path">path of file</param>
	/// <returns>image texture</returns>
	public abstract Texture GetTexture(string path);

	/// <summary>
	/// Method:
	/// Create sprite from image.
	/// Assumption: GetTexture is called first.
	/// </summary>
	/// <param name="gameobj">Game object for sprite</param>
	public abstract void CreateSprite(GameObject gameobj);
	public abstract Sprite CreateSprite();
	public abstract Sprite CreateSprite(Rect rect, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType, Vector4 border, bool generateFallbackPhysicsShape);

	public static bool IsGIF(string _path)
	{
		if (_path == null)
		{
			return false;
		}
		string extension = Path.GetExtension(_path).ToLower();

		return (extension.Equals(".gif"));
	}

	public static bool IsVideo(string _path)
	{
		if (_path == null)
		{
			return false;
		}
		string extension = Path.GetExtension(_path).ToLower();
		return (extension.Equals(".ogv") || extension.Equals(".mp4") || extension.Equals(".mov"));
	}

}
#endregion
#region OmiGIF Class
/// <summary>
/// Concrete Class
/// For GIF texture
/// </summary>
public class OmiGIF : OmiTexture
{
	#region Variables
	public override string name { get; set; }
	#region filePath
	public override string filePath
	{
		get
		{
			return _FilePath;
		}
		set
		{
			_FilePath = value;
			name = System.IO.Path.GetFileNameWithoutExtension(filePath);
		}
	}
	private string _FilePath;
	#endregion
	public override Vector2Int Dimensions { get; set; }
	public override TextureType Type { get; set; }
	private GIFTexture gifTexture = null;
	public Texture firstFrame { get; set; }
	#endregion
	#region Constructor
	/// <summary>
	/// Constructor for OmiGif sets type to Gif
	/// </summary>
	public OmiGIF()
	{
		Type = TextureType.GIF;
	}
	/// <summary>
	/// Constructor for OmiGif sets Type to Gif
	/// and set texture to the file path.
	/// </summary>
	/// <param name="_path">Url for Gif File</param>
	public OmiGIF(string _path)
	{
		Type = TextureType.GIF;
		GetTexture(_path);
	}
	#endregion
	#region GetTextures
	/// <summary>
	/// WARNING: A valid gif texture called "null" is returned.
	/// </summary>
	/// <param name="path">Url of gif to be used</param>
	/// <returns>gif called "null" or actual null</returns>
	public override Texture GetTexture(string _path)
	{
		GIFTexture _gif = null;
		filePath = _path;
		try
		{
			_gif = new GIFTexture(filePath); // WARNING: Valid "null" named object return
			gifTexture = _gif;
		}
		catch (Exception ex) { Debug.LogError(ex); }

		if (_gif.currentSprite != null)
		{
			firstFrame = _gif.currentSprite.texture;
			firstFrame.name = System.IO.Path.GetFileNameWithoutExtension(filePath);
			name = firstFrame.name;
			Dimensions = new Vector2Int(firstFrame.width, firstFrame.height);
		}

		return _gif;
	}
	#endregion
	#region CreateSprite
	/// <summary>
	/// Creats a gif and applies it to the gameobject entered
	/// </summary>
	/// <param name="_gameObject">gameObject that will have gif applied to it</param>
	public override void CreateSprite(GameObject _gameObject)
	{
		GIF.Create(_gameObject, gifTexture);
	}
	/// <summary>
	/// Returns the first frame of the gif as a sprite
	/// useful for using where gifs might not be supported
	/// or you only want a still image and animated image in two places
	/// </summary>
	/// <returns>A Sprite</returns>
	public override Sprite CreateSprite()
	{
		Sprite _tmpSprite = Sprite.Create((Texture2D)firstFrame, new Rect(0, 0, firstFrame.width, firstFrame.height), new Vector2(0.5f, 0.5f));
		_tmpSprite.name = name;
		return _tmpSprite;
	}

	public override Sprite CreateSprite(Rect rect, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType, Vector4 border, bool generateFallbackPhysicsShape)
	{
		Sprite _tmpSprite = Sprite.Create((Texture2D)firstFrame,rect,pivot,pixelsPerUnit,extrude,meshType,border,generateFallbackPhysicsShape);
		_tmpSprite.name = name;
		return _tmpSprite;
	}
	#endregion
}
#endregion
#region OmiObjectTexture Class
/// <summary>
/// Concrete Class
/// For GameObject texture
/// </summary>
public class OmiObjectTexture : OmiTexture
{
	#region Variables
	public override string name { get; set; }
	#region FilePath
	public override string filePath
	{
		get
		{
			return _FilePath;
		}
		set
		{
			_FilePath = value;
			name = System.IO.Path.GetFileNameWithoutExtension(filePath);
		}
	}
	private string _FilePath;
	#endregion
	public override Vector2Int Dimensions { get; set; }
	public override TextureType Type { get; set; }
	#endregion
	#region Constructor
	/// <summary>
	/// Constructor sets Type to OBJ
	/// </summary>
	public OmiObjectTexture()
	{
		Type = TextureType.OBJ;
	}
	#endregion
	#region Not Used
	public override Texture GetTexture(string path)
	{
		throw new NotImplementedException();
		//return null;
	}
	public override void CreateSprite(GameObject gameobj)
	{
		throw new NotImplementedException();
	}
	public override Sprite CreateSprite()
	{
		throw new NotImplementedException();
	}
	public override Sprite CreateSprite(Rect rect,Vector2 pivot,float pixelsPerUnit,uint extrude,SpriteMeshType meshType,Vector4 border,bool generateFallbackPhysicsShape)
	{
		throw new NotImplementedException();
	}
	#endregion
}
#endregion
#region OMIImage Class
/// <summary>
/// Concrete Class
/// For png, jpg images
/// </summary>
public class OmiImage : OmiTexture
{
	#region Variables
	public override string name { get; set; }
	#region FilePath
	public override string filePath
	{
		get
		{
			return _FilePath;
		}
		set
		{
			_FilePath = value;
			name = System.IO.Path.GetFileNameWithoutExtension(filePath);
		}
	}
	private string _FilePath;
	#endregion
	public override Vector2Int Dimensions { get; set; }
	public override TextureType Type { get; set; }
	public Texture2D texture2D { get; set; }
	public Texture texture;
	#endregion
	#region Constructor
	/// <summary>
	/// Constructor for OmiImage
	/// </summary>
	public OmiImage()
	{
		Type = TextureType.IMG;
	}

	/// <summary>
	/// Constructor for OmiImage Sets path and applies texture
	/// </summary>
	/// <param name="_path">Url of image to be used</param>
	public OmiImage(string _path)
	{
		GetTexture(_path);
		Type = TextureType.IMG;
	}
	#endregion
	#region GetTexture
	/// <summary>
	/// Gets Texture from URL Path or if texture already has a value it returns texture
	/// </summary>
	/// <param name="_path">Location of the image as a URL</param>
	/// <returns>texture</returns>
	public override Texture GetTexture(string _path)
	{
		if (texture != null)
		{
			return texture;
		}
		else
		{
			filePath = _path;
			if (File.Exists(filePath))
			{
				texture2D = new Texture2D(2,2);
				byte[] _rawData = File.ReadAllBytes(filePath);
				name = Path.GetFileNameWithoutExtension(filePath);
				texture2D.LoadImage(_rawData);
				texture2D.name = name;
				texture2D.filterMode = FilterMode.Bilinear;
				texture2D.Apply(true);
				texture = texture2D;
				texture.name = name;
				Dimensions = new Vector2Int(texture2D.width,texture2D.height);
				return texture;
			}
			else
			{
				return null;
			}
		}
	}
	#endregion
	#region CreateSprite
	/// <summary>
	/// Creates a sprite for the SpriteRenderer
	/// Only Use if there is a known Spriterenderer
	/// as this attempts access the spriterenderer
	/// which may not be there.
	/// </summary>
	/// <param name="_srGameObject">GameObject CreateSprite will look for a SpriteRenderer On</param>
	public override void CreateSprite(GameObject _srGameObject)
	{
		_srGameObject.GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f));
	}



	/// <summary>
	/// Creates a sprite from the OmiImage Texture.
	/// used when using renderers other than the SpriterRenderers 
	/// but still use sprites like the 
	/// </summary>
	/// <returns>Sprite from omiImage Texture</returns>
	public override Sprite CreateSprite()
	{
		Sprite _tmpSprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f));
		_tmpSprite.name = name;
		return _tmpSprite;
	}
	public override Sprite CreateSprite(Rect rect, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType, Vector4 border, bool generateFallbackPhysicsShape)
	{
		Sprite _tmpSprite = Sprite.Create(texture2D,rect,pivot,pixelsPerUnit,extrude,meshType,border,generateFallbackPhysicsShape);
		_tmpSprite.name = name;
		return _tmpSprite;
	}
	#endregion
}
#endregion
#region OmiMovie Class
/// <summary>
/// OmiMovie Class
/// Use for Video Formats
/// </summary>
public class OmiMovie : OmiTexture
{
	#region Variables
	public override string name { get; set; }
	#region FilePath
	public override string filePath
	{
		get
		{
			return _FilePath;
		}
		set
		{
			_FilePath = value;
			name = System.IO.Path.GetFileNameWithoutExtension(filePath);
		}
	}
	private string _FilePath;
	#endregion
	public override Vector2Int Dimensions { get; set; }
	public override TextureType Type { get; set; }
	public VideoPlayer videoPlayer { get; set; }
	public AudioSource audioSource { get; set; }
	#endregion
	#region Constuctor
	/// <summary>
	/// Constructor for OmiMovie
	/// <para>Requires Path Before using SetUpMovie</para>
	/// </summary>
	public OmiMovie()
	{
		filePath = null;
		videoPlayer = null;
		Type = TextureType.MOV;
	}

	/// <summary
	/// Constructor for OmiMovie sets Path to File Location
	/// </summary>
	/// <param name="_path">Url for the location of the file</param>
	public OmiMovie(string _path)
	{
		filePath = _path;
		name = System.IO.Path.GetFileNameWithoutExtension(filePath);
		videoPlayer = null;
		Type = TextureType.MOV;
	}
	#endregion
	#region VideoControls
	#region Play
	public void Play()
	{
		videoPlayer.Play();
	}
	#endregion
	#region Pause
	public void Pause()
	{
		videoPlayer.Pause();
	}
	#endregion
	#region Stop
	public void stop()
	{
		videoPlayer.Stop();
	}
	#endregion
	#region Mute
	public bool mute
	{
		get
		{
			return audioSource.mute;
		}
		set
		{
			audioSource.mute = value;
		}
	}
	#endregion
	#endregion
	#region Set Up Movie
	/// <summary>
	/// <para>Intializes the Audio.</para>
	/// <para>Intializes the VideoPlayer.</para>
	/// <para>Sets Dimensions of texture.</para>
	/// </summary>
	/// <param name="_vPlayerObject">Target Game Object for VideoPlayer and AudioSource</param>
	/// <param name="_loop">Decides If the Video will loop or not.</param>
	/// <param name="_muted">Decides If the Videos Audio Will Be muted</param>
	public void SetUpMovie(GameObject _vPlayerObject, bool _loop = true, bool _muted = false)
	{
		InitializeAudio(_vPlayerObject);
		InitializeVideoPlayer(filePath, _vPlayerObject, _loop, false);
		Dimensions = new Vector2Int((int)videoPlayer.width, (int)videoPlayer.height);


	}
	#endregion
	#region Intialize Video Player
	/// <summary>
	/// <para>Either gets the VideoPlayer or adds one if none is already attatched to the Gameobject</para>
	/// <para>Does Setup on the VideoPlayer.</para>
	/// <para>Checks Which Renderer is being used wether it is a normal renderer or a sprite renderer.</para>
	/// <para>Prepares the VideoPlayer.</para>
	/// </summary>
	/// <param name="_url">Url for the location of the file</param>
	/// <param name="_vPlayerObject">Object the VideoPlayer is attatched to</param>
	/// <param name="_loop">Decides If the Video will loop or not</param>
	/// <param name="_muted">Decides If the Videos Audio Will Be muted</param>
	public void InitializeVideoPlayer(string _url, GameObject _vPlayerObject, bool _loop = true, bool _muted = true)
	{
		const int startTrack = 0;
		videoPlayer = null;
		if (_vPlayerObject.GetComponent<VideoPlayer>() == null)
		{
			videoPlayer = _vPlayerObject.AddComponent<VideoPlayer>();
		}
		else
		{
			videoPlayer = _vPlayerObject.GetComponent<VideoPlayer>();
		}

		#region VideoPlayer Setup
		videoPlayer.playOnAwake = false;
		videoPlayer.source = VideoSource.Url;
		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
		videoPlayer.controlledAudioTrackCount = 1;
		videoPlayer.EnableAudioTrack(0, true);
		videoPlayer.SetTargetAudioSource(startTrack, audioSource);
		videoPlayer.url = _url;
		videoPlayer.isLooping = _loop;
		videoPlayer.renderMode = VideoRenderMode.MaterialOverride;
		videoPlayer.SetDirectAudioMute(startTrack, _muted);
		#endregion

		if (_vPlayerObject.GetComponent<Renderer>() != null)
		{
			videoPlayer.targetMaterialRenderer = _vPlayerObject.GetComponent<Renderer>();
		}
		else if (_vPlayerObject.GetComponent<SpriteRenderer>() != null)
		{
			videoPlayer.targetMaterialRenderer = _vPlayerObject.GetComponent<SpriteRenderer>();
		}
		else
		{
			Debug.Log("Missing Render on " + _vPlayerObject);
		}
		videoPlayer.Prepare();

	}
	#endregion
	#region Initialize Audio
	/// <summary>
	/// Initializes Audio Checks if gameObject has an AudioSource if it does it adds it to
	/// the OmiMovie audioSource if not it adds an AudioSource to the gameObject
	/// </summary>
	/// <param name="_vPlayerObject">GameObject to apply AudioSource to</param>
	private void InitializeAudio(GameObject _vPlayerObject)
	{
		if (_vPlayerObject.GetComponent<AudioSource>() == null)
		{
			audioSource = _vPlayerObject.AddComponent<AudioSource>();
		}
		else
		{
			audioSource = _vPlayerObject.GetComponent<AudioSource>();
		}

	}
	#endregion
	#region not used
	/// <summary>
	/// !!!CREATESPRITE IS NOT USED BY OMIMOVIE !!!
	/// </summary>
	/// <param name="gameobj"></param>
	/// <exception cref="NotImplementedException"></exception>
	public override void CreateSprite(GameObject gameobj)
	{
		throw new NotImplementedException();
	}
	/// <summary>
	/// !!!CREATESPRITE IS NOT USED BY OMIMOVIE !!!
	/// </summary>
	/// <returns></returns>
	/// <exception cref="NotImplementedException"></exception>
	public override Sprite CreateSprite()
	{
		throw new NotImplementedException();
	}
	public override Sprite CreateSprite(Rect rect, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType, Vector4 border, bool generateFallbackPhysicsShape)
	{
		throw new NotImplementedException();
	}
	/// <summary>
	/// !!!GETTEXTURE IS NOT USED BY OMIMOVIE !!!
	/// </summary>
	/// <param name="path"></param>
	/// <returns>Will Throws Exception</returns>
	/// <exception cref="NotImplementedException"></exception>
	public override Texture GetTexture(string path)
	{
		throw new NotImplementedException();
	}
	#endregion
}

#endregion



