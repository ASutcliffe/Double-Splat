﻿using UnityEngine;
using System.Collections;
using System;

public class MouseHighlighter : MonoBehaviour, IDevelopmentTool
{

    private DevelopmentToolsSettings settings;
    private GameObject Highlighter;
    private new Renderer renderer;

    public void Initialize(DevelopmentToolsSettings _settings)
    {
        this.settings = _settings;

        Highlighter = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        Highlighter.transform.localScale = new Vector3(1f, 0.1f, 1f);
        Highlighter.GetComponent<CapsuleCollider>().enabled = false;

        renderer = Highlighter.GetComponent<Renderer>();
        Highlighter.transform.parent = this.transform;
        renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        renderer.material.shader = Shader.Find("Transparent/Diffuse");
    }

    // Update is called once per frame
    private void OnGUI()
    {
        if (settings.MouseHighlighterEnabled)
        {
            renderer.enabled = true;
            renderer.material.SetColor("_Color", settings.MouseHighlighterColour);
            Vector3 screenPoint = Input.mousePosition;
            screenPoint.z = 2.0f;
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPoint);
            Highlighter.transform.position = worldPos;
        }
        else
        {
            renderer.enabled = false;
        }
    }
}
