﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DevelopmentToolsManager : MonoBehaviour
{
    public DevelopmentToolsSettings _options;


    // Use this for initialization
    void Start()
    {
        parseSettings();

        Transform[] allChildren = GetComponentsInChildren<Transform>(true);
        foreach (Transform child in allChildren)
        {
            child.gameObject.SetActive(true);
            Component[] components = child.GetComponents<MonoBehaviour>();
            foreach(MonoBehaviour mb in components)
            {
                if(mb is IDevelopmentTool)
                {
                    IDevelopmentTool devTool = (IDevelopmentTool)mb;
                    devTool.Initialize(_options);
                }
            }
        }
        EffectController.Instance.DevelopmentManagerLoadStatus = true;
    }


    void parseSettings()
    {
        _options.FPSCounterEnabled = ParameterManager.Instance.IsShowingFPS;
    }
}
