﻿using UnityEngine;

public interface IDevelopmentTool
{
    void Initialize(DevelopmentToolsSettings settings);
}
