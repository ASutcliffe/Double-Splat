﻿using UnityEngine;
using System.Collections;

public class FPSDisplay : MonoBehaviour, IDevelopmentTool
{ 
    private DevelopmentToolsSettings settings;

    float deltaTime = 0.0f, msec, fps;
    string fpsString;
    GUIStyle style = new GUIStyle();
    Rect fpsDrawLocation;

    public void Initialize(DevelopmentToolsSettings _settings)
    {
        this.settings = _settings;
        fpsDrawLocation = new Rect(0, 0, Screen.width, Screen.height * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = Screen.height * 2 / 100;
        style.normal.textColor = Color.black;

    }

    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
    }



    void OnGUI()
    {
        if (settings.FPSCounterEnabled)
        {
            msec = deltaTime * 1000.0f;
            fps = 1.0f / deltaTime;
            fpsString = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            GUI.Label(fpsDrawLocation, fpsString, style);
        }
    }
}