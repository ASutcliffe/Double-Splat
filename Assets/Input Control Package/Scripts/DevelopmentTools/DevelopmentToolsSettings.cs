﻿using UnityEngine;

[CreateAssetMenu]
public class DevelopmentToolsSettings : ScriptableObject
{
    [Header("Development Tools")]
    public bool FPSCounterEnabled = false;

    [Header("Mouse Controls")]
    public bool MouseHighlighterEnabled = false;
    public Color MouseHighlighterColour = new Color(255, 0, 0, 0.5f);
}
