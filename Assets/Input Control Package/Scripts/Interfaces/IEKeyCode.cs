﻿using UnityEngine;
using System.Collections;
using System;

public class IEKeyCode
{
    public KeyCode code;
    public float pressedTime;

    public IEKeyCode (KeyCode _code, float _time)
    {
        code = _code;
        pressedTime = _time;
    }
}
