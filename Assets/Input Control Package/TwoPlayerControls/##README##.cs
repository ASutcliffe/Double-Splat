/*
 * Drag the interaction Manager, Parameter Loader and render texture plane prefabs to the scene
 * Create an empty gameobject named player controller attatch the "Two Player Input Controller" script
 * Create a two game objects one named player one and the other player two
 * link these players to their slots on the player controller
 * link the render texture RT_ShaderOutput to the slot shader Output
 * link the Render Texture Plane
 * 
 * See Example Scene for working example
 */