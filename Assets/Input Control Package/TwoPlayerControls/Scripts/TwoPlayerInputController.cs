#define DEBUG
///When building product should always #undef DEBUG
#undef DEBUG
#define InteractionDisplay
//Turn On the Display of the Interaction Matrix on the screen
#undef InteractionDisplay
using UnityEngine;
using UnityEngine.UI;

public class TwoPlayerInputController : MonoBehaviour
{
	private int
		idleP1 = 0,
		idleP2 = 0;

	private Vector2
		positionP1,
		positionP2,
		prevPositionP1,
		prevPositionP2,
		PlayAreaDimensions,
		P1NormalisedPosition = Vector2.zero,
		P2NormalisedPosition = Vector2.zero;
#if InteractionDisplay
	public Texture2D IDebugTex
	{
		get { return _iDebugTex; }
		set
		{

			_iDebugTex = value;
			InteractionDebug.sprite = Sprite.Create(_iDebugTex, new Rect(0, 0, 320, 240), new Vector2(0.5f, 0.5f));
		}
	}
	private Texture2D _iDebugTex;
#endif
	public Image InteractionDebug;



	public GameObject
	gameObjectP1,
	gameObjectP2;

	InteractionManager
		interactionManager;

	// Provide reference to Controller instance. Assume only one exists.
	public static TwoPlayerInputController Instance;

	// Stop interaction if device input
	public bool isDeviceInput { set; get; }

	#region Awake
	private void Awake()
	{
#if DEBUG
		LogWriter.Log("TwoPlayerInputController Is Awake");
#endif
		PlayAreaDimensions = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.transform.position.z));

#if InteractionDisplay
		IDebugTex = new Texture2D(320, 240);
		IDebugTex.filterMode = FilterMode.Point;
		InteractionDebug.gameObject.SetActive(true);
#else
		InteractionDebug.gameObject.SetActive(false);
#endif

	}
	#endregion

	#region Start
	void Start()
	{
#if DEBUG
		LogWriter.Log("TwoPlayerInputController Started");
#endif
		// Provide public reference to this object
		Instance = this;

		InteractionManager.OnInteractionReceived += InteractionManager_OnInteractionReceived;
		TouchManager.OnTouch += TouchManager_OnTouch;
	}
#endregion

#region Fixed Update
	/// <summary>
	/// Mouse Input overides Player 1s positioning incase it is needed for 
	/// a player who cant interact with the standard method
	/// </summary>
	private void FixedUpdate()
	{
		// Check if a mouse or device interaction.
		// If mouse or device has input, stop the interaction from overwriting that position (for one frame).
		if (!isDeviceInput)
        {
			// Not mouse or device. Interaction processing. 
			// Update Player1 and Player2 positions.
            PositioningIdleManagment(UIController.Player.Player1, ref positionP1, ref prevPositionP1, ref idleP1);
            PositioningIdleManagment(UIController.Player.Player2, ref positionP2, ref prevPositionP2, ref idleP2);

			// Move Player objects to positions and wait for collision detection.
			MovePlayers();
        }
    }
#endregion

#region Update
	private void Update()
	{

		if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Return))
		{
#if DEBUG
			LogWriter.Log("Quitting App");
#endif
			Application.Quit();
		}
	}
#endregion

#region Interaction Manager On Interacion Recieved
	private void InteractionManager_OnInteractionReceived(int[] interaction, int iWidth, int iHeight)
	{
		gradientPositioning(interaction, 10);
	}
#endregion

#region Gradient Positioning
	/// <summary>
	/// Runs through Every Interaction giving it a position x y value ranging from 0 to 1
	/// player 1 uses x y as x y
	/// player 2 uses z y as x y 
	/// this is just because it originally used rgb when it was done with a shader but also 
	/// is an easy way to store the 2 players positions as y should be the same as it goes 
	/// along a row
	/// 
	/// Finds the the top most point of the bounding box of the interaction by getting the 
	/// max in both x and y and using that to calculate the the bounds and highest point is
	/// used as the output vector 2
	/// </summary>
	/// <param name="interaction">Interaction from interaction manager</param>
	/// <param name="_safeZoneWidth">width in pixels of the zone between the two players wher no interaction is recorded</param>
	void gradientPositioning(int[] interaction, int _safeZoneWidth)
	{
		Vector3 _currentValue = Vector3.zero;
		Vector2
			P1LargestValue = Vector2.zero,
			P2LargestValue = Vector2.zero;
		for (int i = 0; i < interaction.Length; i++)
		{
			int x = (i % 320);
			int y = (240 - 1) - (i / 320);
			if (x < 320 / 2)
			{
#if InteractionDisplay
				IDebugTex.SetPixel(x, y, new Color(0f, 0f, 1f, interaction[i]));
#endif

				if (x > 160 - (_safeZoneWidth / 2))
				{
					_currentValue = Vector3.zero;
				}
				else
				{
					if (interaction[i] > 0)
					{
						_currentValue = new Vector3(x / 160f, y / 240f, 0f);
					}
					else
					{
						_currentValue = Vector3.zero;
					}
				}
				if (_currentValue.x > P1LargestValue.x)
				{
					P1LargestValue.x = _currentValue.x;
				}
				if (_currentValue.y > P1LargestValue.y)
				{
					P1LargestValue.y = _currentValue.y;
				}
			}
			else
			{
#if InteractionDisplay
				IDebugTex.SetPixel(x, y, new Color(1f, 0.5f, 0f, interaction[i]));
#endif
				if (x < 160 + (_safeZoneWidth / 2))
				{
					_currentValue = Vector3.zero;
				}
				else
				{
					if (interaction[i] > 0)
					{
						_currentValue = new Vector3((((x - 160f) / 160f) - 1f) * -1f, y / 240f,0f );
					}
					else
					{
						_currentValue = Vector3.zero;
					}
				}
				if (_currentValue.x > P2LargestValue.x)
				{
					P2LargestValue.x = _currentValue.x;
				}
				if (_currentValue.y > P2LargestValue.y)
				{
					P2LargestValue.y = _currentValue.y;
				}
				P1NormalisedPosition = P1LargestValue;
				P2NormalisedPosition = P2LargestValue;

			}
		}
#if InteractionDisplay
		//IDebugTex.SetPixel((int)(P1NormalisedPosition.x * 160f), (int)(P1NormalisedPosition.y * 240f), new Color(1f, 0f, 0f, 1f));
		for (int x = 0; x < (int)(P1NormalisedPosition.x * 160f); x++)
		{
			IDebugTex.SetPixel(x, (int)(P1NormalisedPosition.y * 240f), new Color(1f, 0f, 0f, 1f));
		}
		for (int y = 0; y < (int)(P1NormalisedPosition.y * 240); y++)
		{
			IDebugTex.SetPixel((int)(P1NormalisedPosition.x * 160f), y, new Color(0f, 1f, 0f, 1f));

		}
		for (int x = 320; x > (int)(320f - (P2NormalisedPosition.x * 160f)); x--)
		{
			IDebugTex.SetPixel(x, (int)(P2NormalisedPosition.y * 240f), new Color(1f, 0f, 0f, 1f));
		}
		for (int y = 0; y < (int)(P2NormalisedPosition.y * 240); y++)
		{
			IDebugTex.SetPixel((int)(320f - (P2NormalisedPosition.x * 160f)), y, new Color(0f, 1f, 0f, 1f));

		}

		IDebugTex.SetPixel((int)(320f-(P2NormalisedPosition.x * 160f)), (int)(P2NormalisedPosition.y * 240f), new Color(0f, 1f, 0f, 1f));
		IDebugTex.Apply();
#endif

	}
#endregion

#region positioning Idle Managment
	/// <summary>
	/// makes up for times when the interaction may not been seen for a frame as well as 
	/// moving the player off screen when their input has hasnt been seen for a set time.
	/// </summary>
	/// <param name="_player">sets the player this will effect</param>
	/// <param name="_position">refrences the positioning value of the object</param>
	/// <param name="_previous">refrences the previous positioning of the object</param>
	/// <param name="_idle">sets how long before player has to be idle before their player is moved from the scene</param>
	void PositioningIdleManagment(UIController.Player _player, ref Vector2 _position, ref Vector2 _previous, ref int _idle)
	{
#if DEBUG
		LogWriter.Log("Positioning " + _player);
#endif
		Vector2 _tmpPosition = _player == UIController.Player.Player1 ? P1NormalisedPosition : P2NormalisedPosition;

		if (0f != _tmpPosition.x && 0f != _tmpPosition.y)
		{
			_previous = _position;
			_position = _tmpPosition;
			_idle = 0;
		}
		else
		{
			_idle++;
			if (_idle < 5)
			{
				_position = _previous;
			}
			else
			{
				_position = new Vector2(0.5f, -1f);
			}
		}
	}
#endregion

#region Player Movement P1 & P2
	/// <summary>
	/// Player Movements convert the normalised position and converts it into world position for the game objects to use.
	/// </summary>
	void PlayerOneMovement()
	{
		gameObjectP1.transform.position = new Vector3((positionP1.x * PlayAreaDimensions.x) - PlayAreaDimensions.x, (positionP1.y * (PlayAreaDimensions.y * 2)) - PlayAreaDimensions.y, 0);
	}

	void PlayerTwoMovement()
	{
		gameObjectP2.transform.position = new Vector3(PlayAreaDimensions.x - (positionP2.x * PlayAreaDimensions.x), (positionP2.y * (PlayAreaDimensions.y * 2)) - PlayAreaDimensions.y, 0);
	}
	#endregion

#region Player Input
	/// <summary>
	/// Update Player 1 position with new position.
	/// </summary>
	/// <param name="newPosition">New position</param>
	public void Player1Input(Vector2 newPosition)
	{
		// Update Player 1 position
		positionP1 = newPosition;
		// Update Player 2 position
		PositioningIdleManagment(UIController.Player.Player2, ref positionP2, ref prevPositionP2, ref idleP2);
		// Move the player objects to the new positions
		MovePlayers();
	}

	/// <summary>
	/// Update Player 2 position
	/// </summary>
	/// <param name="newPosition"></param>
	public void Player2Input(Vector2 newPosition)
    {
		// Update Player 2 position
		positionP2 = newPosition;
		// Update Player 1 position
		PositioningIdleManagment(UIController.Player.Player1, ref positionP1, ref prevPositionP1, ref idleP1);
		// Move the player objects to the new positions
		MovePlayers();
	}
	/// <summary>
	/// Move Player 1 and Player 2 to new positions.
	/// </summary>
	private void MovePlayers()
    {
		PlayerOneMovement();
		PlayerTwoMovement();
	}
	public bool isPlayer1(Vector2 actHere)
    {
		float Player1_2Border = Screen.width / 2;
		//                                     right : left
		//                                   player2 : player1
		return (actHere.x > Player1_2Border) ? false : true;		 
    }

	public void BindDeviceInteractionToDoubleSplat(Vector2 actHere, Vector2 player1Position, Vector2 player2Position)
	{
		// Move player object
		if (  isPlayer1(actHere)  )
		   { Player1Input(player1Position); } 
		else 
		   { Player2Input(player2Position); }
	}

	private void TouchManager_OnTouch(Touch touch, Vector2 player1Position, Vector2 player2Position)
	{
		isTouch = true;

		// Bind the effect at the touch position
		BindDeviceInteractionToDoubleSplat(touch.position, player1Position, player2Position);
		
	}


	#endregion Player Input

	#region Device
	public static bool isTouch = false;
	#endregion Device
}
