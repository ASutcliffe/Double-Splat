
#define GRADEMODE
//#undef GRADEMODE
using System.Linq;
using UnityEngine;

public class ShaderReplacementTest : MonoBehaviour
{
    private Vector2Int interactionDimensions = new Vector2Int(0, 0);
    public Texture2D Output;
    public Texture2D outputP1;
    public Texture2D outputP2;
    public Color player1, player2;
    public int safeZoneWidth = 10;
    InteractionManager interactionManager;
    private void Awake()
    {
        outputP1 = new Texture2D(160, 240);
        outputP2 = new Texture2D(160, 240);
        Output = new Texture2D(320, 240);
        Output.name = "output";
        Output.filterMode = FilterMode.Point;
    }

    // Start is called before the first frame update
    void Start()
    {

        InteractionManager.OnInteractionReceived += InteractionManager_OnInteractionReceived;
        GetComponent<SpriteRenderer>().sprite = Sprite.Create(Output, new Rect(0f, 0f, ParameterManager.Instance.InteractionDimensions.x, ParameterManager.Instance.InteractionDimensions.y), Vector2.zero);
    }

    private void InteractionManager_OnInteractionReceived(int[] interaction, int iWidth, int iHeight)
    {
#if GRADEMODE
        gradientMap(interaction, iWidth, iHeight, safeZoneWidth, ref outputP1, ref outputP2);
        GetComponent<SpriteRenderer>().sprite = Sprite.Create(Output, new Rect(0, 0, ParameterManager.Instance.InteractionDimensions.x, ParameterManager.Instance.InteractionDimensions.y), Vector2.zero);

#endif
    }

    #region Gradient Mode
    /// <summary>
    /// Takes the interaction, the height and the width from the interaction 
    /// manager and converts it into two interaction maps for each players play area
    /// </summary>
    /// <param name="interaction">Taken from InteractionManager_OnInteractionReceived</param>
    /// <param name="iWidth">Taken from InteractionManager_OnInteractionReceived</param>
    /// <param name="iHeight">Taken from InteractionManager_OnInteractionReceived</param>
    /// <param name="_safeZoneWidth">Distance of blacked out zone in the center to stop player interaction cross contaminating</param>
    /// <param name="_outputP1">playAreaP1Tex</param>
    /// <param name="_outputP2">playAreaP2Tex</param>
    void gradientMap(int[] interaction, int iWidth, int iHeight,int _safeZoneWidth,ref Texture2D _outputP1,ref Texture2D _outputP2)
    {
        for (int i = 0; i < interaction.Length; i++)
        {
            int x = (i % iWidth);
            int y = iHeight - (i / iWidth);
            Color pixelColor = Color.black;
            if (x < iWidth / 2)
            {
                if (x > iWidth / 2 - (_safeZoneWidth / 2))
                {
                    pixelColor = Color.black;
                }
                else
                {
                    pixelColor = new Color(x / (iWidth / 2f), (float)y / iHeight, 0f);
                }
                _outputP1.SetPixel(x, y, (interaction[i] == 0) ? Color.black : pixelColor);
            }
            else
            {
                if (x < iWidth / 2 + (_safeZoneWidth / 2))
                {
                    pixelColor = Color.black;
                }
                else
                {
                    pixelColor = new Color(0f, (float)y / iHeight, (((float)(x - (iWidth / 2)) / (iWidth / 2)) - 1f) * -1f);
                }
                _outputP2.SetPixel((_outputP2.width+1)-x, y, (interaction[i] == 0) ? Color.black : pixelColor);
            }
        }
        _outputP1.Apply();
        _outputP2.Apply();
    }
    #endregion
}
