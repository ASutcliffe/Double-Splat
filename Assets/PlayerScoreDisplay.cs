using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScoreDisplay : MonoBehaviour
{
    public TextMeshProUGUI playerScore;
    public UIController.Player player;
    public RectTransform background, inlay, text;
	public Image BackgroundImg, InlayImg;
    private void Awake()
    {
        background.sizeDelta = new Vector2(Screen.width * 0.0703125f, Screen.height * 0.125f);
        inlay.sizeDelta = new Vector2(background.sizeDelta.x, background.sizeDelta.y);
        text.sizeDelta = new Vector2(inlay.sizeDelta.x -5, inlay.sizeDelta.y -5);
        playerScore.fontSize = inlay.sizeDelta.y * 0.556f;
    }
	public void SetStyle()
	{
		BackgroundImg.sprite = ParameterManager.Instance.ScoreBoardBorder.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.ScoreBoardBorder.Dimensions.x, ParameterManager.Instance.ScoreBoardBorder.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			128f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		BackgroundImg.type = Image.Type.Tiled;
		InlayImg.sprite = ParameterManager.Instance.ScoreBoardInlay.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.ScoreBoardInlay.Dimensions.x, ParameterManager.Instance.ScoreBoardInlay.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			128f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		InlayImg.type = Image.Type.Tiled;

	}
}
