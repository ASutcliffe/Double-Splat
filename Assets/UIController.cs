#define DEBUG
///When building product should always #undef DEBUG
#undef DEBUG
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIController : MonoBehaviour
{
	public static UIController Instance = null;
	#region Enumerators
	public enum GameMode { Unlimited, Time, Score, Both }
	public enum Player { Player1, Player2 };
	#endregion
	#region Basic Variables
	#region Public
	public int scorePlayer1
	{
		set
		{
			if (_gamePlaying)
			{
				_scorePlayer1 = value;
				scoreP1.text = _scorePlayer1.ToString();
			}
		}
		get
		{
			return _scorePlayer1;
		}
	}
	public int scorePlayer2
	{
		set
		{
			if (_gamePlaying)
			{
				_scorePlayer2 = value;
				scoreP2.text = _scorePlayer2.ToString();
			}

		}
		get
		{
			return _scorePlayer2;
		}
	}
	public bool Player1Ready
	{
		get { return _player1Ready; }
		set
		{
			_player1Ready = value;
			if (_player1Ready)
			{
				refreshP1.GetComponent<RefreshButton>().Icon.color = new Color(0f, 1f, 0);
			}
			else
			{
				refreshP1.GetComponent<RefreshButton>().Icon.color = new Color(1f, 0f, 0f);

			}
			playersReady = _player1Ready & _player2Ready;
		}
	}
	public bool Player2Ready
	{
		get { return _player2Ready; }
		set
		{
			_player2Ready = value;
			if (_player2Ready)
			{
				refreshP2.GetComponent<RefreshButton>().Icon.color = new Color(0f, 1f, 0f);
			}
			else
			{
				refreshP2.GetComponent<RefreshButton>().Icon.color = new Color(1f, 0f, 0f);

			}
			playersReady = _player2Ready & _player1Ready;
		}
	}
	public float ReloadPositionP1
	{
		get
		{
			return reloadPositionP1;
		}
		set
		{
			reloadPositionP1 = value;
			if (!ParameterManager.Instance.VerticalPlay)
			{
				float _tmpPositionP1 = Mathf.Lerp(cameraWorldSpaceDimensions.y - (reloadButtonSizeP1.x / 2), -cameraWorldSpaceDimensions.y + (reloadButtonSizeP1.x / 2), reloadPositionP1);
				ReloadP1.transform.position = new Vector3(-cameraWorldSpaceDimensions.x + (reloadButtonSizeP1.y / 2), _tmpPositionP1, -1f);
			}
			else
			{
				float _tmpPositionP1 = Mathf.Lerp(-cameraWorldSpaceDimensions.x + (reloadButtonSizeP1.x / 2), 0 - (reloadButtonSizeP1.x / 2) - reloadTimebarOffset, reloadPositionP1);
				ReloadP1.transform.position = new Vector3(_tmpPositionP1, -cameraWorldSpaceDimensions.y + (reloadButtonSizeP1.y / 2), -1f);
			}

		}
	}
	public float ReloadPositionP2
	{
		get
		{
			return reloadPositionP2;

		}
		set
		{
			reloadPositionP2 = value;
			if (!ParameterManager.Instance.VerticalPlay)
			{
				float _tmpPositionP2 = Mathf.Lerp(-cameraWorldSpaceDimensions.y + (reloadButtonSizeP2.x / 2), cameraWorldSpaceDimensions.y - (reloadButtonSizeP2.x / 2), reloadPositionP2);
				ReloadP2.transform.position = new Vector3(cameraWorldSpaceDimensions.x - (reloadButtonSizeP2.y / 2), _tmpPositionP2, -1f);
			}
			else
			{
				float _tmpPositionP2 = Mathf.Lerp(0 + (reloadButtonSizeP2.x / 2) + reloadTimebarOffset, cameraWorldSpaceDimensions.x - (reloadButtonSizeP2.x / 2), reloadPositionP2);
				ReloadP2.transform.position = new Vector3(_tmpPositionP2, -cameraWorldSpaceDimensions.y + (reloadButtonSizeP2.y / 2), -1f);
			}
		}

	}
	public bool GamePlaying
	{
		get
		{
			return _gamePlaying;
		}
		set
		{
			_gamePlaying = value;
		}
	}
	public bool
		playersReady,
		ComboMode;
	#endregion
	#region Private
	private bool
		_gamePlaying,
		_player1Ready,
		_player2Ready;

	private int
		_scorePlayer1 = 0,
		_scorePlayer2 = 0;
	private float
		timerUIMaxLength = 780f,
		timerUILength,
		timerEnd,
		timeLeftNormalised = 1f,
		timeLeft,
		reloadPositionP1 = 0.5f,
		reloadPositionP2 = 0.5f;
	#endregion

	#endregion
	#region Complex Variables
	#region Public
	public Color targetColorP1
	{
		set
		{
			_targetColorP1 = value;
			_targetImageP1.color = new Color(value.r, value.g, value.b, value.a);

		}
		get
		{
			return _targetColorP1;
		}
	}
	public Color targetColorP2
	{
		set
		{
			_targetColorP2 = value;
			_targetImageP2.color = new Color(value.r, value.g, value.b, value.a);
		}
		get
		{
			return _targetColorP2;
		}
	}
	public OmiTexture targetTextureP1
	{
		get
		{
			return _targetTextureP1;
		}
		set
		{
			_targetTextureP1 = value;
			_targetImageP1.sprite = value.CreateSprite();
		}
	}
	public OmiTexture targetTextureP2
	{
		get
		{
			return _targetTextureP2;
		}
		set
		{
			_targetTextureP2 = value;
			_targetImageP2.sprite = value.CreateSprite();
		}
	}

	//TODO: Replace With OmiTexture when adding parameter loader support
	public Texture2D
		ComboTextureP1,
		ComboTextureP2;

	public GameObject
		medalP1,
		medalP2,
		victoryScreen,
		refreshP1,
		refreshP2,
		PF_targetDisplayP1,
		PF_targetDisplayP2,
		PF_scoreDisplayP1,
		PF_scoreDisplayP2,
		PF_TimerBarDisplay,
		PF_ComboP1,
		PF_ComboP2,
		PF_ComboBar,
		endScreenP1,
		endScreenP2,
		ReloadP1,
		ReloadP2,
		forgroundVerticalP1,
		forgroundVerticalP2,
		forgroundHorizontalP1,
		forgroundHorizontalP2,
		comboBarP1,
		comboBarP2;

	public TextMeshProUGUI
		scoreP1,
		scoreP2,
		endScoreP1,
		endScoreP2;
	public GameMode playLimits;

	#endregion
	#region Private
	private Color
		_targetColorP1,
		_targetColorP2;
	private OmiTexture
		_targetTextureP1,
		_targetTextureP2;
	private Image
		_targetImageP1,
		_targetImageP2,
		_timerBarBackground,
		_timerBarInlay,
		_timerBarInformation;
	private GameObject
		targetDisplayP1,
		targetDisplayP2,
		scoreDisplayP1,
		scoreDisplayP2,
		timerBarDisplay;
	public bool showTargetP1
	{
		set
		{
			targetDisplayP1.SetActive(value);
		}
	}

	public bool showTargetP2
	{
		set
		{
			targetDisplayP2.SetActive(value);
		}
	}

	private Vector2
		leftMaxP1 = new Vector2(3.6f, -6.6f),
		rightMaxP1 = new Vector2(-3.6f, -1.9f),
		leftMaxP2 = new Vector2(-3.6f, 1.9f),
		rightMaxP2 = new Vector2(3.6f, 6.6f);
	public Vector2
		reloadButtonSizeP1,
		reloadButtonSizeP2;

	#endregion
	#endregion
	#region Awake
	/// <summary>
	/// Called ar launch if of UI Controller
	/// Disables victory and Refresh and enables gameplaying which lets players gain score.
	/// </summary>
	private void Awake()
	{
		Instance = this;
#if DEBUG
        LogWriter.Log("UI Controller Is Awake");
#endif
		CameraSetup();
		victoryScreen.SetActive(false);
		refreshP1.SetActive(false);
		refreshP2.SetActive(false);
		_gamePlaying = true;
	}
	#endregion
	#region Start
	// Start is called before the first frame update
	void Start()
	{
#if DEBUG
        LogWriter.Log("UI Controller Started");
#endif
		ReloadButtonSetup();
		SetGameMode();
		InstantiateUIComponents();
		GameModeUIInitializations();
		EffectController.Instance.UIControllerLoadStatus = true;
	}

	#region GameMode
	void SetGameMode()
	{
		switch (ParameterManager.Instance.TimeMode, ParameterManager.Instance.ScoreMode)
		{
			case (false, false):
				playLimits = GameMode.Unlimited;
				break;
			case (false, true):
				playLimits = GameMode.Score;
				break;
			case (true, false):
				playLimits = GameMode.Time;
				break;
			case (true, true):
				playLimits = GameMode.Both;
				break;
		}
	}
	#endregion

	#region Reload Button Setup
	void ReloadButtonSetup()
	{
		reloadButtonSizeP1 = reloadButtonSizeP2 = ParameterManager.Instance.ReloadSize;
		reloadPositionP1 = reloadPositionP2 = ParameterManager.Instance.ReloadPosition;
	}
	#endregion

	#region UI Initialization 
	/// <summary>
	/// Loads UI from Prefabs to display and positions UI in either vertical or horizontal positions
	/// </summary>
	void InstantiateUIComponents()
	{
#if DEBUG
        LogWriter.Log("Instantiating UI Components");
#endif
		#region Player 1 Target
#if DEBUG
        LogWriter.Log("Setup for Player 1 Target UI");
#endif
		PlayerTargetUI(PF_targetDisplayP1, ref targetDisplayP1, ref _targetImageP1);
		#endregion
		#region Player 2 Target
#if DEBUG
        LogWriter.Log("Setup for Player 2 Target UI");
#endif
		PlayerTargetUI(PF_targetDisplayP2, ref targetDisplayP2, ref _targetImageP2);
		#endregion
		#region player 1 Score
#if DEBUG
        LogWriter.Log("Setup Player 1 Score UI");
#endif
		PlayerScoreUI(PF_scoreDisplayP1, ref scoreDisplayP1, ref scoreP1);
		#endregion
		#region Player 2 Score
#if DEBUG
        LogWriter.Log("Setup Player 2 Score UI");
#endif
		PlayerScoreUI(PF_scoreDisplayP2, ref scoreDisplayP2, ref scoreP2);
		#endregion
		#region Timer Bar
		TimerBar();
		#endregion
		#region ComboImage
		if (ParameterManager.Instance.ComboMode)
		{
			PlayerComboBar(PF_ComboBar, ref comboBarP1, UIController.Player.Player1);
			PlayerComboBar(PF_ComboBar, ref comboBarP2, UIController.Player.Player2);
		}
		#endregion
		#region End Screens
		EndScreens();
		#endregion
		#region Reload Buttons
		ReloadButtons();
		#endregion
	}
	#region Player Target UI
	void PlayerTargetUI(GameObject _targetDisplayPrefab, ref GameObject _targetDisplay, ref Image _targetImage)
	{
		_targetDisplay = Instantiate(_targetDisplayPrefab, gameObject.transform);
		PlayerTargetDisplay _PTDSctipt = _targetDisplay.GetComponent<PlayerTargetDisplay>();
		_PTDSctipt.SetStyle();
		_targetImage = _PTDSctipt.TargetImage;
		if (ParameterManager.Instance.VerticalPlay)
		{
			RectTransform rectTransform = _targetDisplay.GetComponent<RectTransform>();
			rectTransform.anchorMin = new Vector2(_PTDSctipt.player == UIController.Player.Player1 ? 0f : 0.5f, 1f);
			rectTransform.anchorMax = new Vector2(_PTDSctipt.player == UIController.Player.Player1 ? 0f : 0.5f, 1f);
			rectTransform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
			rectTransform.anchoredPosition = new Vector2(_PTDSctipt.player == UIController.Player.Player1 ? 0 : 15, 0);

		}
	}

	#endregion
	/// <summary>
	/// Sets up the Combo UI Element 
	/// </summary>
	/// <param name="_ComboPrefab">Prefab of the combo Object</param>
	/// <param name="_comboDisplay">Refrence to the game object created from the prefab</param>
	/// <param name="_comboImage">Image to be used for the combo Ui element</param>
	void PlayerComboBar(GameObject _comboBarPrefab, ref GameObject _comboBarDisplay, UIController.Player player)
	{
		_comboBarDisplay = Instantiate(_comboBarPrefab, gameObject.transform);
		ComboBar _CBScript = _comboBarDisplay.GetComponent<ComboBar>();
		_CBScript.SetStyle();
		_CBScript.player = player;
		RectTransform rectTransform = _CBScript.background;
		_CBScript.SetBackground(new Vector2(Screen.width * 0.3076171875f, Screen.height * 0.0325520833333333f), ParameterManager.Instance.VerticalPlay);
		_CBScript.ComboLevel = 0;
	}
	#region Player Score UI
	/// <summary>
	/// Sets up the score ui needs to be run for each players score display
	/// </summary>
	/// <param name="_scoreDisplayPrefab">Prefab of the player score</param>
	/// <param name="_scoreDisplay"> a refrence to store the created gameobject for later use</param>
	/// <param name="_scoreText">refrence to the text display in the score ui</param>
	void PlayerScoreUI(GameObject _scoreDisplayPrefab, ref GameObject _scoreDisplay, ref TextMeshProUGUI _scoreText)
	{
		_scoreDisplay = Instantiate(_scoreDisplayPrefab, gameObject.transform);
		PlayerScoreDisplay _PSDScript = _scoreDisplay.GetComponent<PlayerScoreDisplay>();
		_PSDScript.SetStyle();
		_scoreText = _PSDScript.playerScore;

		if (ParameterManager.Instance.VerticalPlay)
		{
			RectTransform rectTransform = _scoreDisplay.GetComponent<RectTransform>();
			rectTransform.anchorMin = new Vector2(_PSDScript.player == UIController.Player.Player1 ? 0.5f : 1f, 1f);
			rectTransform.anchorMax = new Vector2(_PSDScript.player == UIController.Player.Player1 ? 0.5f : 1f, 1f);
			rectTransform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
			rectTransform.anchoredPosition = new Vector2(_PSDScript.player == UIController.Player.Player1 ? -15 : 0, 0);
		}
	}
	#endregion
	#region Timer Bar
	/// <summary>
	/// Instantiates the timer bar and sets its positioning and dimensions
	/// </summary>
	void TimerBar()
	{
#if DEBUG
        LogWriter.Log("Setup Timer Bar UI");
#endif
		timerBarDisplay = Instantiate(PF_TimerBarDisplay, gameObject.transform);
		TimerBarDisplay _TBDScript = timerBarDisplay.GetComponent<TimerBarDisplay>();
		_TBDScript.SetStyle();
		_timerBarBackground = _TBDScript.TimerBackground;
		_timerBarInlay = _TBDScript.TimerInlay;
		_timerBarInformation = _TBDScript.TimerInformation;
		timerUIMaxLength = Screen.height;
		_timerBarBackground.rectTransform.pivot = new Vector2(0.5f, 1f);
		timerBarDisplay.GetComponent<RectTransform>().sizeDelta = new Vector2(_timerBarBackground.rectTransform.rect.width, timerUIMaxLength);
		_timerBarBackground.rectTransform.sizeDelta = new Vector2(_timerBarBackground.rectTransform.rect.width, timerUIMaxLength);
		_timerBarInlay.rectTransform.sizeDelta = new Vector2(_timerBarInlay.rectTransform.rect.width, timerUIMaxLength);
		_timerBarInformation.rectTransform.sizeDelta = new Vector2(_timerBarInformation.rectTransform.rect.width, timerUIMaxLength);
	}
	#endregion
	#region End Screens
	/// <summary>
	/// Corrects the positioning of the end screen depending on if the game is running in vertical or horizontal
	/// </summary>
	void EndScreens()
	{
#if DEBUG
        LogWriter.Log("Setup End Screen UI");
#endif
		EndScreenDisplay _ESDP1Script = endScreenP1.GetComponent<EndScreenDisplay>();
		EndScreenDisplay _ESDP2Script = endScreenP2.GetComponent<EndScreenDisplay>();
		RefreshButton _RBP1Script = refreshP1.GetComponent<RefreshButton>();
		RefreshButton _RBP2Script = refreshP2.GetComponent<RefreshButton>();
		_ESDP1Script.SetStyle();
		_ESDP2Script.SetStyle();
		_RBP1Script.SetStyle();
		_RBP2Script.SetStyle();
		if (ParameterManager.Instance.VerticalPlay)
		{
			RectTransform rectTransform = endScreenP1.GetComponent<RectTransform>();
			rectTransform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
			rectTransform.anchoredPosition = new Vector2(-340, 100);
			rectTransform = endScreenP2.GetComponent<RectTransform>();
			rectTransform.anchoredPosition = new Vector2(340, 100);
			rectTransform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
			refreshP1.transform.position = new Vector3(-cameraWorldSpaceDimensions.x * 0.75f, -3f, -1.5f);
			refreshP2.transform.position = new Vector3(cameraWorldSpaceDimensions.x * 0.75f, -3f, -1.5f);

		}
	}
	#endregion
	#region Reload Buttons
	/// <summary>
	/// If reload use is set in the parameters this enables the reload button as well as positioning and scaling it to the correct dimensions based on params
	/// each players reload position and scale can be changed independantly.
	/// </summary>
	public float reloadTimebarOffset = 0.2f;
	void ReloadButtons()
	{
		if ((GameController.Instance.modifiers & 32) == 32)
		{
			ReloadP1.SetActive(true);
			ReloadP2.SetActive(true);
			ReloadButton _RBP1Script = ReloadP1.GetComponent<ReloadButton>();
			ReloadButton _RBP2Script = ReloadP2.GetComponent<ReloadButton>();
			_RBP1Script.SetStyle();
			_RBP2Script.SetStyle();
			_RBP1Script.SetDimensions(reloadButtonSizeP1);
			_RBP2Script.SetDimensions(reloadButtonSizeP2);
			_RBP1Script.setArea();
			_RBP2Script.setArea();
			if (!ParameterManager.Instance.VerticalPlay)
			{
				#region P1 Position
				//float _tmpPositionP1 = Mathf.Lerp(leftMaxP1.x, rightMaxP1.x, ReloadPositionP1);
				float _tmpPositionP1 = Mathf.Lerp(cameraWorldSpaceDimensions.y - (reloadButtonSizeP1.x / 2), -cameraWorldSpaceDimensions.y + (reloadButtonSizeP1.x / 2), reloadPositionP1);

				ReloadP1.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, -90f));
				ReloadP1.transform.position = new Vector3(-cameraWorldSpaceDimensions.x + (reloadButtonSizeP1.y / 2), _tmpPositionP1, -1f);
				#endregion
				#region P2 Position
				float _tmpPositionP2 = Mathf.Lerp(-cameraWorldSpaceDimensions.y + ((reloadButtonSizeP2.x - 0.2f) / 2), cameraWorldSpaceDimensions.y - (reloadButtonSizeP2.x / 2), reloadPositionP2);
				ReloadP2.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));
				ReloadP2.transform.position = new Vector3(cameraWorldSpaceDimensions.x - (reloadButtonSizeP2.y / 2), _tmpPositionP2, -1f);
				#endregion
			}
			else
			{
				#region P1 Position
				float _tmpPositionP1 = Mathf.Lerp(-cameraWorldSpaceDimensions.x + (reloadButtonSizeP1.x / 2), 0 - (reloadButtonSizeP1.x / 2) - reloadTimebarOffset, reloadPositionP1);
				ReloadP1.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
				ReloadP1.transform.position = new Vector3(_tmpPositionP1, -cameraWorldSpaceDimensions.y + (reloadButtonSizeP1.y / 2), -1f);
				#endregion
				#region P2 Position
				float _tmpPositionP2 = Mathf.Lerp(0 + (reloadButtonSizeP2.x / 2) + reloadTimebarOffset, cameraWorldSpaceDimensions.x - (reloadButtonSizeP2.x / 2), reloadPositionP2);
				ReloadP2.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
				ReloadP2.transform.position = new Vector3(_tmpPositionP2, -cameraWorldSpaceDimensions.y + (reloadButtonSizeP2.y / 2), -1f);
				#endregion
			}
		}
		else
		{
			ReloadP1.SetActive(false);
			ReloadP2.SetActive(false);
		}
	}
	#endregion
	#endregion
	#region Gamemode UI Initializations
	/// <summary>
	/// Decides which parts of the UI are initialized 
	/// depending on the mode the game is in.
	/// </summary>
	void GameModeUIInitializations()
	{
#if DEBUG
        LogWriter.Log("Game Mode UI Initalizing");
#endif
		ScoreInitialize();
		TimerInitialize();

		//		switch (playLimits)
		//		{
		//			case PlayLimits.Unlimited:
		//#if DEBUG
		//                LogWriter.Log("Initializing Unlimited Mode");
		//#endif
		//				ScoreInitialize();
		//				TimerInitialize();
		//				break;
		//			case PlayLimits.Time:
		//#if DEBUG
		//                LogWriter.Log("Initializing Time Constraint Mode");
		//#endif
		//				TimerInitialize();
		//				ScoreInitialize();
		//				break;
		//			case PlayLimits.Score:
		//#if DEBUG
		//                LogWriter.Log("Initializing Score Constraint Mode");
		//#endif
		//				ScoreInitialize();
		//				TimerInitialize();
		//				break;
		//			case PlayLimits.Both:
		//#if DEBUG
		//                LogWriter.Log("Intializing Scorea And Timer Constraint Mode");
		//#endif
		//				ScoreInitialize();
		//				TimerInitialize();
		//				break;
		//			default:
		//				break;
		//		}
	}
	#region Timer Initialize
	/// <summary>
	/// Initializes the Timer.
	/// </summary>
	void TimerInitialize()
	{
#if DEBUG
        LogWriter.Log("Intitializing Timer");
#endif
		timerEnd = Time.time + ParameterManager.Instance.TimeLimit;
		timeLeft = Mathf.Clamp(timerEnd - Time.time, 0, ParameterManager.Instance.TimeLimit);
		timeLeftNormalised = timeLeft / ParameterManager.Instance.TimeLimit;
	}
	#endregion
	#region Score Intialize
	/// <summary>
	/// Initializes the scores.
	/// </summary>
	void ScoreInitialize()
	{
#if DEBUG
        LogWriter.Log("Intitializing Scores");
#endif
		_gamePlaying = true;
		scorePlayer1 = 0;
		scorePlayer2 = 0;
	}
	#endregion
	#endregion
	#endregion
	#region Update
	// Update is called once per frame
	void Update()
	{
		GameModeLoop();
		if (playersReady)
		{
			Player1Ready = false;
			Player2Ready = false;
			ReloadGame();
		}
#if DEBUG
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Player1Ready = !Player1Ready;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Player2Ready = !Player2Ready;
        }
#endif
	}
	#region Gamemode Loop
	/// <summary>
	/// Changes Loop Dependant on the game mode
	/// </summary>
	void GameModeLoop()
	{
		switch (playLimits)
		{
			case GameMode.Unlimited:
				_timerBarInformation.color = new Color(
					_timerBarInformation.color.r,
					_timerBarInformation.color.b,
					_timerBarInformation.color.g,
					0f);
				break;
			case GameMode.Time:
				if (timeLeftNormalised > 0)
				{
					TimerGraphics();
				}
				else
				{
					podiumUI();
				}
				break;
			case GameMode.Score:
				_timerBarInformation.color = new Color(
					_timerBarInformation.color.r,
					_timerBarInformation.color.b,
					_timerBarInformation.color.g,
					0f);
				if (scorePlayer2 >= ParameterManager.Instance.ScoreLimit | scorePlayer1 >= ParameterManager.Instance.ScoreLimit)
				{
					podiumUI();
				}
				break;
			case GameMode.Both:
				if (timeLeftNormalised > 0)
				{
					if (scorePlayer2 >= ParameterManager.Instance.ScoreLimit | scorePlayer1 >= ParameterManager.Instance.ScoreLimit)
					{
						podiumUI();
					}
					else
					{
						TimerGraphics();
					}
				}
				else
				{
					podiumUI();
				}
				break;
			default:
				break;
		}
	}

	#region Timer Graphics
	/// <summary>
	/// Updates the timer grahpics shrinking the time bar while also changing its colour
	/// </summary>
	void TimerGraphics()
	{

		timeLeft = Mathf.Clamp(timerEnd - Time.time, 0, ParameterManager.Instance.TimeLimit);
		timeLeftNormalised = timeLeft / ParameterManager.Instance.TimeLimit;
		timerUILength = timerUIMaxLength * timeLeftNormalised;

		_timerBarInformation.color = Color.Lerp(ParameterManager.Instance.TimerEndColor, ParameterManager.Instance.TimerStartColor, timeLeftNormalised);
		_timerBarInformation.rectTransform.sizeDelta = new Vector2(_timerBarInformation.rectTransform.rect.width, timerUILength);

	}
	#endregion
	#region Podium UI
	bool CoroutineActivated = false;
	/// <summary>
	/// Stops the score system and displays the score of each player as well as an displays a reward icon for the players
	/// </summary>
	void podiumUI()
	{
		_gamePlaying = false;
		GameController.Instance.ClearObjects(ref GameController.Instance.objectsP1);
		GameController.Instance.ClearObjects(ref GameController.Instance.objectsP2);
		if (ParameterManager.Instance.ComboMode)
		{
			GameController.Instance.combo(Player.Player1, true);
			GameController.Instance.combo(Player.Player2, true);
		}
		endScoreP1.text = scorePlayer1.ToString();
		endScoreP2.text = scorePlayer2.ToString();
		#region Player 1 Wins
		if (scorePlayer1 > scorePlayer2)
		{
			OmiTexture _player1 = ParameterManager.Instance.First;
			medalP1.GetComponent<Image>().sprite = _player1.CreateSprite();
			OmiTexture _player2 = ParameterManager.Instance.Second;
			medalP2.GetComponent<Image>().sprite = _player2.CreateSprite();
		}
		#endregion
		#region Player 2 Wins
		else if (scorePlayer1 < scorePlayer2)
		{
			OmiTexture _player1 = ParameterManager.Instance.Second;
			medalP1.GetComponent<Image>().sprite = _player1.CreateSprite();
			OmiTexture _player2 = ParameterManager.Instance.First;
			medalP2.GetComponent<Image>().sprite = _player2.CreateSprite();
		}
		#endregion
		#region Players Draw
		else if (scorePlayer1 == scorePlayer2)
		{
			OmiTexture _player1 = ParameterManager.Instance.First;
			medalP1.GetComponent<Image>().sprite = _player1.CreateSprite();
			OmiTexture _player2 = ParameterManager.Instance.First;
			medalP2.GetComponent<Image>().sprite = _player2.CreateSprite();
		}
		#endregion
		victoryScreen.SetActive(true);
		if (!CoroutineActivated)
		{
			StartCoroutine(RefreshIconDelay(10f));
		}
	}
	public IEnumerator RefreshIconDelay (float time)
	{
		Debug.Log("TimerStarted");
		CoroutineActivated = true;
		yield return new WaitForSecondsRealtime (time);
		refreshP1.SetActive(true);
		refreshP2.SetActive(true);
		yield break;
	}

	#endregion
	#endregion
	#region Reload Game
	/// <summary>
	/// Sets the game back to its initial state.
	/// </summary>
	void ReloadGame()
	{
		_gamePlaying = true;
		GameController.Instance.GameStart();
		GameModeUIInitializations();
		victoryScreen.SetActive(false);
		refreshP1.SetActive(false);
		refreshP2.SetActive(false);
		CoroutineActivated = false;
	}
	#endregion
	#endregion
	Camera mainCamera;
	public Vector2 cameraWorldSpaceDimensions;
	void CameraSetup()
	{
		mainCamera = Camera.main;
		cameraWorldSpaceDimensions = mainCamera.ViewportToWorldPoint(new Vector2(1, 1));
		Sprite texture;
	}
}
