using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerBarDisplay : MonoBehaviour
{

    public Image
        TimerBackground,
        TimerInlay,
        TimerInformation;
	public void SetStyle()
	{
		TimerBackground.sprite = ParameterManager.Instance.TimeBarBorder.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.TimeBarBorder.Dimensions.x, ParameterManager.Instance.TimeBarBorder.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			128f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		TimerBackground.type = Image.Type.Tiled;
		TimerInlay.sprite = ParameterManager.Instance.TimeBarInlay.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.TimeBarInlay.Dimensions.x, ParameterManager.Instance.TimeBarInlay.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			128f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		TimerInlay.type = Image.Type.Tiled;
		TimerInformation.sprite = ParameterManager.Instance.TimeBarInformation.CreateSprite(
			new Rect(0, 0, ParameterManager.Instance.TimeBarInformation.Dimensions.x, ParameterManager.Instance.TimeBarInformation.Dimensions.y),
			new Vector2(0.5f, 0.5f),
			128f,
			3u,
			SpriteMeshType.FullRect,
			new Vector4(128, 128, 128, 128),
			false);
		TimerInformation.type = Image.Type.Tiled;
	}
}
